package com.interteleco.kuwaitcalendar.activities;

import androidx.appcompat.widget.AppCompatEditText;
import androidx.fragment.app.FragmentActivity;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.interteleco.kuwaitcalendar.R;
import com.interteleco.kuwaitcalendar.dialogs.MapDialog;
import com.interteleco.kuwaitcalendar.server.EventApi;
import com.interteleco.kuwaitcalendar.server.ServiceClient;
import com.interteleco.kuwaitcalendar.server.models.EventData;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EventsMap extends FragmentActivity {

//    private GoogleMap mMap;
//    String title = "";
//    String category = "all";
//    int duration = 1;
//    List<EventData> eventsLocation;
//
//    String tag = "";
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.interactive_map);
//
//        eventsLocation = new ArrayList<EventData>();
//
//        ImageView search = (ImageView) findViewById(R.id.searchBtn);
//        AppCompatEditText txtSearch = (AppCompatEditText) findViewById(R.id.txtSearch);
//
//        txtSearch.addTextChangedListener(new TextWatcher() {
//            @Override
//            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
//
//            }
//
//            @Override
//            public void onTextChanged(CharSequence s, int start, int before, int count) {
//                search.setImageResource(R.drawable.magnify);
//            }
//
//            @Override
//            public void afterTextChanged(Editable s) {
//
//            }
//        });
//
//        search.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                tag = "searh";
//                mMap.clear();
//                getMap(txtSearch.getText().toString(), category, 1);
//
//            }
//        });
//
//
//        //  getMap(title,category,duration);
////  EventData eventData=new EventData();
////     eventData.setTitle_en("test");
////        eventData.setLat(30.039452);
////        eventData.setLongt(31.202534);
////       eventsLocation.add(eventData);
////        EventData event=new EventData();
////        event.setTitle_en("test2");
////        event.setLat(30.060354);
////        event.setLongt(31.345560);
////        eventsLocation.add(event);
//
//        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
//        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
//                .findFragmentById(R.id.map);
//        mapFragment.getMapAsync(this);
//    }
//
//
//    /**
//     * Manipulates the map once available.
//     * This callback is triggered when the map is ready to be used.
//     * This is where we can add markers or lines, add listeners or move the camera. In this case,
//     * we just add a marker near Sydney, Australia.
//     * If Google Play services is not installed on the device, the user will be prompted to install
//     * it inside the SupportMapFragment. This method will only be triggered once the user has
//     * installed Google Play services and returned to the app.
//     */
//    @Override
//    public void onMapReady(GoogleMap googleMap) {
//        mMap = googleMap;
//        getMap(title, category, duration);
//
//
////   for(int i=0;i<eventsLocation.size();i++) {
////    // Add a marker in Sydney and move the camera
////    LatLng location = new LatLng(eventsLocation.get(i).getLat(), eventsLocation.get(i).getLongt());
////    mMap.addMarker(new MarkerOptions().position(location).title(eventsLocation.get(i).getTitle_en()));
////    mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(location,10));
////}
//
//    }
//
//    public void getMap(String title, String category, int duration) {
//
//        EventApi retrofitAPI = ServiceClient.createService(EventApi.class, this);
//        retrofitAPI.getMapEvents(title, category, duration).enqueue(new Callback<List<EventData>>() {
//            @Override
//            public void onResponse(Call<List<EventData>> call, Response<List<EventData>> response) {
//                if (response.code() == 200) {
//
//                    eventsLocation = response.body();
//                    for (int i = 0; i < eventsLocation.size(); i++) {
//                        // Add a marker in Sydney and move the camera
//
//                        LatLng location = new LatLng(eventsLocation.get(i).getLat(), eventsLocation.get(i).getLongt());
//                        Marker marker = mMap.addMarker(new MarkerOptions().position(location).title(eventsLocation.get(i).getTitle_en()));
//                        marker.setTag(i);
//                        // marker.showInfoWindow();
//                        String title = eventsLocation.get(i).getTitle_en();
//
//                        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(location, 8));
//                        mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
//                            @Override
//                            public boolean onMarkerClick(Marker marker) {
//                                int position = (int) (marker.getTag());
//                                MapDialog mapDialog = new MapDialog();
//                                mapDialog.id = eventsLocation.get(position).get_id();
//                                mapDialog.lat = eventsLocation.get(position).getLat();
//                                mapDialog.longt = eventsLocation.get(position).getLongt();
//                                mapDialog.show(getSupportFragmentManager(), "mapDialog");
//
//                                //    Toast.makeText(getApplicationContext(),  ""+ eventsLocation.get(position).getTitle_en()+"", Toast.LENGTH_LONG).show();
//                                return false;
//                            }
//                        });
//
//                    }
//
//                } else {
//                    //    if (swipeRefreshLayout != null)
//                    //    swipeRefreshLayout.setRefreshing(false);
//                    //       progressBar.setVisibility(View.GONE);
//                    Toast.makeText(getApplicationContext(), getString(R.string.serverError), Toast.LENGTH_LONG).show();
//
//                }
//            }
//
//            @Override
//            public void onFailure(Call<List<EventData>> call, Throwable throwable) {
//
//                // if (swipeRefreshLayout != null)
//                //swipeRefreshLayout.setRefreshing(false);
//                //    progressBar.setVisibility(View.GONE);
//                Toast.makeText(getApplicationContext(), getString(R.string.serverError), Toast.LENGTH_LONG).show();
//            }
//        });
//    }


}
