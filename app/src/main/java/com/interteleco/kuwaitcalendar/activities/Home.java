package com.interteleco.kuwaitcalendar.activities;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.viewpager.widget.ViewPager;

import me.relex.circleindicator.CircleIndicator;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


import android.Manifest;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.content.res.Configuration;

import android.graphics.drawable.Drawable;
import android.location.Location;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;

import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewFlipper;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.navigation.NavigationView;
import com.google.firebase.FirebaseApp;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.interteleco.kuwaitcalendar.R;

import com.interteleco.kuwaitcalendar.adapters.SliderAdapter;
import com.interteleco.kuwaitcalendar.dialogs.NoConnection;
import com.interteleco.kuwaitcalendar.fragments.AddEvent;
import com.interteleco.kuwaitcalendar.fragments.AllEventsFragment;
import com.interteleco.kuwaitcalendar.fragments.EditUserProfile;
import com.interteleco.kuwaitcalendar.fragments.EventDetails;
import com.interteleco.kuwaitcalendar.fragments.Feedback;
import com.interteleco.kuwaitcalendar.fragments.Hfragment;
import com.interteleco.kuwaitcalendar.fragments.KuwaitCultureFragment;
import com.interteleco.kuwaitcalendar.fragments.Setting;
import com.interteleco.kuwaitcalendar.fragments.UserProfile;
import com.interteleco.kuwaitcalendar.server.EventApi;
import com.interteleco.kuwaitcalendar.server.ServiceClient;
import com.interteleco.kuwaitcalendar.server.UserApi;
import com.interteleco.kuwaitcalendar.server.models.APIError;
import com.interteleco.kuwaitcalendar.server.models.CategoryData;
import com.interteleco.kuwaitcalendar.server.models.RESPONSE;
import com.interteleco.kuwaitcalendar.utils.Constants;
import com.interteleco.kuwaitcalendar.utils.ErrorUtils;
import com.interteleco.kuwaitcalendar.utils.Fonts;
import com.interteleco.kuwaitcalendar.utils.Global;
import com.interteleco.kuwaitcalendar.utils.NetworkStatusReciever;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import java.util.Objects;
import java.util.Timer;

public class Home extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    AlertDialog alertDialog;
    AlertDialog.Builder builder;
    Toolbar toolbar;
    DrawerLayout drawerLayout;
    NavigationView navigationView;
    ViewPager viewPager;
    SliderAdapter adapter;
    CircleIndicator indicator;
    Handler handler;
    Runnable runnable;
    Timer timer;
    ViewFlipper viewFlipper;
    ArrayList<CategoryData.Category> categories;
    AllEventsFragment allEventsFragment;
    Hfragment home;
    FrameLayout frame;
    private float lastTranslate = 0.0f;
    RelativeLayout noConnection;
    TextView txtNoInternet;
    boolean isRegistered;
    String authorized;
    String fireBaseToken = "";

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        loadLocaleLang();


        setContentView(R.layout.navigation_view);
        if (Global.getInstance(this).getFromSharedPreferneces("mylang").equalsIgnoreCase("en")) {

            getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
            //    getWindow().getDecorView().setTextAlignment(View.TEXT_ALIGNMENT_VIEW_END);
            getWindow().getDecorView().setTextDirection(View.TEXT_DIRECTION_LTR);

        } else {
            getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
            //    getWindow().getDecorView().setTextAlignment(View.TEXT_ALIGNMENT_VIEW_END);
            getWindow().getDecorView().setTextDirection(View.TEXT_DIRECTION_RTL);
        }
        authorized = Global.getInstance(this).getFromSharedPreferneces(Constants.Authorized);
        home = new Hfragment();
        navigationView = (NavigationView) findViewById(R.id.navigation_view);
        ImageView close = (ImageView) navigationView.getHeaderView(0).findViewById(R.id.menuBtn);
        Menu menu = navigationView.getMenu();
        MenuItem profile = menu.findItem(R.id.profile);
        MenuItem myevents = menu.findItem(R.id.myevents);
        MenuItem mhome = menu.findItem(R.id.home_id);
        MenuItem logout = menu.findItem(R.id.logout);
        MenuItem signUp = menu.findItem(R.id.signUp);

        MenuItem add = menu.findItem(R.id.add);
        MenuItem editProfile = menu.findItem(R.id.edit);
        MenuItem accountinfo = menu.findItem(R.id.profileInfo);
        MenuItem feed = menu.findItem(R.id.feedback);

//        myevents.setEnabled(false);
//        profile.setEnabled(false);
//
//        mhome.setEnabled(false);
        if (authorized != null && !authorized.equalsIgnoreCase("")) {
            logout.setTitle(getString(R.string.signout));
            profile.setVisible(true);
            accountinfo.setVisible(true);
            editProfile.setVisible(true);
            signUp.setVisible(false);
            add.setVisible(true);

        } else {
            logout.setTitle(getString(R.string.signin));
            profile.setVisible(false);
            accountinfo.setVisible(false);
            editProfile.setVisible(false);
            add.setVisible(false);

        }

        frame = (FrameLayout) findViewById(R.id.container);

        noConnection = (RelativeLayout) findViewById(R.id.noConnection);

        txtNoInternet = (TextView) findViewById(R.id.txtNOConnection);

        Fonts.set(new TextView[]{txtNoInternet}, this, 0);
        if (Global.getInstance(this).getFromSharedPreferneces("mylang").equalsIgnoreCase("ar"))
            navigationView.setItemTextAppearance(R.style.RobotoTextViewStyle);
        else
            navigationView.setItemTextAppearance(R.style.NavTextViewStyle);
        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);


        categories = new ArrayList<>();
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        setTitle("");
        navigationView.setNavigationItemSelectedListener(this);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.open_drawer, R.string.close_drawer);


        drawerLayout.setDrawerListener(toggle);

        toggle.syncState();

        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
                    drawerLayout.closeDrawer(GravityCompat.START);
                }
            }
        });
        if (NetworkStatusReciever.isConnected(getBaseContext()) == true) {

        } else {
            NoConnection dialog = new NoConnection();
            dialog.show(getSupportFragmentManager(), "NoConnectDialog");
        }


        addFragamentbyTAG(home, "Hfragment");
        getFireBaseToken();
        handleIntent(getIntent());


    }

    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            //     boolean isNetworkAvailable = intent.getBooleanExtra(IS_NETWORK_AVAILABLE, false);
            // String networkStatus = isNetworkAvailable ? "connected" : "disconnected";
            if (isConnected() == false)
                noConnection.setVisibility(View.VISIBLE);
            else
                noConnection.setVisibility(View.GONE);

        }
    };

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);

        setIntent(intent);
        handleIntent(getIntent());
    }

    private void handleIntent(Intent intent) {
        Intent applinkintent = intent;
        String applink = applinkintent.getAction();
        Uri applinkData = applinkintent.getData();

        if (applinkData != null) {
            String id = applinkData.getLastPathSegment();
            //   Toast.makeText(getBaseContext(),  id, Toast.LENGTH_LONG).show();
            EventDetails eventDetails = new EventDetails();
            eventDetails.id = id;
            addFragament(eventDetails);
        }

        if (applinkintent.getExtras() != null) {

            Bundle bundle = applinkintent.getExtras();
            if (bundle != null) {

                String eventId = bundle.getString("gcm.notification.eventId");
                String eventUrl = bundle.getString("gcm.notification.eventURL");

                System.out.println("AP:-----Home:---eventId--: " + eventId);
                System.out.println("AP:-----Home:----eventUrl-: " + eventUrl);
                if (eventId != null && !eventId.equals("null") && !eventId.isEmpty()) {
                    EventDetails eventDetails = new EventDetails();
                    eventDetails.id = eventId;
                    addFragament(eventDetails);
                } else if (eventUrl != null && !eventUrl.equals("null") && !eventUrl.isEmpty()) {
                    if (!eventUrl.isEmpty()) {
                        if (!eventUrl.startsWith("http://") && !eventUrl.startsWith("https://")) {
                            eventUrl = "http://" + eventUrl;
                        }
                        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(eventUrl));
                        startActivity(browserIntent);

                    }
                }
            }
        }

    }

    public boolean isConnected() {

        ConnectivityManager cm = (ConnectivityManager) getApplicationContext()
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netinfo = cm.getActiveNetworkInfo();

        if (netinfo != null && netinfo.isConnectedOrConnecting()) {
            android.net.NetworkInfo wifi = cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
            android.net.NetworkInfo mobile = cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

            if ((mobile != null && mobile.isConnectedOrConnecting()) || (wifi != null && wifi.isConnectedOrConnecting()))
                return true;
            else
                return false;
        } else
            return false;


    }

    public void customizeToolbar(ImageView imageView, Drawable drawable_en, Drawable drawable_ar) {
        if (Global.getInstance(this).getFromSharedPreferneces("mylang").equalsIgnoreCase("en"))

            imageView.setImageDrawable(drawable_en);
        else
            imageView.setImageDrawable(drawable_ar);

    }

    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver();
    }

    private void registerReceiver() {
        // IntentFilter intentFilter = new IntentFilter(ConnectivityManager.EXTRA_CAPTIVE_PORTAL);
        IntentFilter intentFilter = new IntentFilter();

        intentFilter.addAction(ConnectivityManager.CONNECTIVITY_ACTION);
        intentFilter.addAction(WifiManager.SUPPLICANT_STATE_CHANGED_ACTION);
        registerReceiver(broadcastReceiver, intentFilter);
    }

    @Override
    protected void onPause() {

        super.onPause();
        try {
            unregisterReceiver(broadcastReceiver);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {

        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {

            drawerLayout.closeDrawer(GravityCompat.START);

        }

        if (getSupportFragmentManager().getBackStackEntryCount() > 1) {
            getSupportFragmentManager().popBackStack();
        } else {
            this.finish();
        }

    }


    public boolean onNavigationItemSelected(@NonNull MenuItem item) {

        int id = item.getItemId();

        switch (id) {


            case R.id.home_id:
//                if (getSupportFragmentManager().getBackStackEntryCount() > 1) {
//                    getSupportFragmentManager().popBackStack("Hfragment", 0);
//                } else {
//
//
//                }

                break;


            case R.id.recommend_id:
                if (getSupportFragmentManager().findFragmentByTag("Recommended") == null && getSupportFragmentManager().getBackStackEntryCount() == 1) {
                    allEventsFragment = new AllEventsFragment();
                    allEventsFragment.Tag = "recom";
                    allEventsFragment.id = "all";
                    addFragamentbyTAG(allEventsFragment, "Recommended");
                }
                break;


            case R.id.week_id:

                if (getSupportFragmentManager().findFragmentByTag("Week") == null && getSupportFragmentManager().getBackStackEntryCount() == 1) {
                    allEventsFragment = new AllEventsFragment();
                    allEventsFragment.Tag = "week";
                    allEventsFragment.id = "all";
                    addFragamentbyTAG(allEventsFragment, "Week");
                }

                break;
            case R.id.all_id:
                if (getSupportFragmentManager().findFragmentByTag("All") == null && getSupportFragmentManager().getBackStackEntryCount() == 1) {
                    allEventsFragment = new AllEventsFragment();
                    allEventsFragment.Tag = "all";
                    allEventsFragment.id = "all";
                    addFragamentbyTAG(allEventsFragment, "All");
                }
                break;

            case R.id.kw_culture_id:
                if (getSupportFragmentManager().findFragmentByTag("KuwaitCulture") == null && getSupportFragmentManager().getBackStackEntryCount() == 1) {
                    KuwaitCultureFragment kuwaitCultureFragment = new KuwaitCultureFragment();

                    addFragamentbyTAG(kuwaitCultureFragment, "KuwaitCulture");
                }
                break;
            case R.id.map:
                Intent objIntent = new Intent(this, InteractiveMap.class);
                startActivity(objIntent);
                break;


            case R.id.kw_map:
                Intent kwMapIntent = new Intent(this, InteractiveMap.class);
                kwMapIntent.putExtra("EventType", "KuwaitCulture");
                startActivity(kwMapIntent);
                break;


            case R.id.add:
                if (authorized != null && !authorized.equalsIgnoreCase("") && getSupportFragmentManager().getBackStackEntryCount() == 1) {
                    if (getSupportFragmentManager().findFragmentByTag("ADD") == null) {
                        AddEvent addEvent = new AddEvent();
                        addFragamentbyTAG(addEvent, "ADD");
                    }
                } else {

                    Toast.makeText(getBaseContext(), getString(R.string.loginFirst), Toast.LENGTH_LONG).show();

                }
                break;

            case R.id.edit:
                if (getSupportFragmentManager().findFragmentByTag("EDIT") == null && getSupportFragmentManager().getBackStackEntryCount() == 1) {
                    EditUserProfile editUserProfile = new EditUserProfile();
                    editUserProfile.navigationTag = "home";
                    addFragamentbyTAG(editUserProfile, "EDIT");
                }
                break;
            case R.id.profileInfo:
                if (getSupportFragmentManager().findFragmentByTag("PROFILE") == null && getSupportFragmentManager().getBackStackEntryCount() == 1) {
                    UserProfile userProfile = new UserProfile();
                    addFragamentbyTAG(userProfile, "PROFILE");
                }
                break;
            case R.id.feedback:
                if (getSupportFragmentManager().findFragmentByTag("FEED") == null && getSupportFragmentManager().getBackStackEntryCount() == 1) {
                    Feedback feedback = new Feedback();

                    addFragamentbyTAG(feedback, "FEED");
                }
                break;

            case R.id.logout:

                if (authorized != null && !authorized.equalsIgnoreCase("")) {
                    showAlert();

                } else {
                    Intent intent = new Intent(this, MainActivity.class);
                    intent.putExtra("tag", "signin");
                    startActivity(intent);
                }


                break;

            case R.id.signUp:


                Intent intent = new Intent(this, MainActivity.class);
                intent.putExtra("tag", "signUp");
                startActivity(intent);


                break;

            case R.id.setting_id:

                String language = Global.getInstance(this).getFromSharedPreferneces("mylang");

                if (language.equalsIgnoreCase("en")) {
                    Global.getInstance(this).saveInSharedPrefernces("mylang", "ar");

                } else {
                    Global.getInstance(this).saveInSharedPrefernces("mylang", "en");

                }
                navigationView.getMenu().clear();
                navigationView.inflateMenu(R.menu.navigation_menu);
                //    restart(3);
//recreate();
                Intent objintent = new Intent(this, Home.class);
                startActivity(objintent);
                finish();
//
                break;
        }

        drawerLayout.closeDrawer(GravityCompat.START);

        return true;

    }


    public void addFragament(Fragment objBaseFragment) {
        FragmentTransaction objFragmentTransaction = getSupportFragmentManager()
                .beginTransaction();
        objBaseFragment.setRetainInstance(false);
        objFragmentTransaction.add(R.id.container, objBaseFragment, objBaseFragment.getClass().toString());
        objFragmentTransaction.addToBackStack(null);
        objFragmentTransaction.commit();
    }

    public void addFragamentbyTAG(Fragment objBaseFragment, String TAG) {

        FragmentTransaction objFragmentTransaction = getSupportFragmentManager()
                .beginTransaction();
        objBaseFragment.setRetainInstance(true);
        objFragmentTransaction.add(R.id.container, objBaseFragment, TAG);
        objFragmentTransaction.addToBackStack(TAG);
        objFragmentTransaction.commit();

    }

    public void setLocale(String lang) {
        Locale locale = new Locale(lang);
        locale.setDefault(locale);
        Configuration configuration = new Configuration();
        configuration.locale = locale;
        getResources().updateConfiguration(configuration, getResources().getDisplayMetrics());
        Global.getInstance(this).saveInSharedPrefernces("mylang", lang);
    }

    public void loadLocaleLang() {
        String language = Global.getInstance(this).getFromSharedPreferneces("mylang");

        if (language != null)
            setLocale(language);
        else
            setLocale("en");

    }


    public void hideKeyBoard() {
        InputMethodManager inputManager = (InputMethodManager) this.getSystemService(Context.INPUT_METHOD_SERVICE);
        if (this.getCurrentFocus() != null)
            inputManager.hideSoftInputFromWindow(this.getCurrentFocus().getWindowToken(), 0);
    }

    //    public void customizeToolbar(Toolbar toolbar) {
//        if (Global.getInstance(this).getFromSharedPreferneces("mylang").equalsIgnoreCase("en"))
//
//            toolbar.setNavigationIcon(R.drawable.arrow);
//        else
//            toolbar.setNavigationIcon(R.drawable.arrow);
//
//
//    }
    public void restart(int delay) {
        Intent intent = new Intent(getApplicationContext(), Splash.class);

        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                | Intent.FLAG_ACTIVITY_CLEAR_TASK
                | Intent.FLAG_ACTIVITY_NEW_TASK);

        PendingIntent pendingIntent = PendingIntent.getActivity(
                getApplicationContext(), 0, intent, intent.getFlags());

        //Following code will restart your application after 2 seconds
        AlarmManager mgr = (AlarmManager)
                getSystemService(Context.ALARM_SERVICE);
        mgr.set(AlarmManager.RTC, System.currentTimeMillis() + 100,
                pendingIntent);

        //This will finish your activity manually
        finish();

        //This will stop your application and take out from it.
        System.exit(2);
//        PendingIntent intent = PendingIntent.getActivity(this.getBaseContext(), 0, new Intent(getIntent()), getIntent().getFlags());
//        AlarmManager manager = (AlarmManager) this.getSystemService(Context.ALARM_SERVICE);
//        manager.set(AlarmManager.RTC, System.currentTimeMillis() + delay, intent);
//        System.exit(2);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

    }

    private void showAlert() {
        builder = new AlertDialog.Builder(this, R.style.AlertDialogeTheme);
        builder.setMessage(this.getString(R.string.close));
        builder.setPositiveButton(this.getString(R.string.ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Global.getInstance(getApplicationContext()).Logout();
                finishAffinity();
                Intent i = getPackageManager()
                        .getLaunchIntentForPackage(getPackageName());
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);

            }
        });

        builder.setNegativeButton(this.getString(R.string.cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                alertDialog.cancel();
            }
        });
        alertDialog = builder.create();

        alertDialog.show();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 1) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {

                    //shareImage();

                }
            }

        }


    }

    public void askpermissionsToShare() {


        if (Build.VERSION.SDK_INT < 23) {

            if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    Activity#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for Activity#requestPermissions for more details.
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);

            } else {

                //   shareImage();

            }


        } else {

            if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {

                //   shareImage();


            } else {

                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);

            }

        }
    }

//    public void askpermissionsCalendar() {
//
//
//        if (Build.VERSION.SDK_INT < 23) {
//
//            if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_CALENDAR) != PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(this, Manifest.permission.READ_CALENDAR) != PackageManager.PERMISSION_GRANTED) {
//                // TODO: Consider calling
//                //    Activity#requestPermissions
//                // here to request the missing permissions, and then overriding
//                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
//                //                                          int[] grantResults)
//                // to handle the case where the user grants the permission. See the documentation
//                // for Activity#requestPermissions for more details.
//                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_CALENDAR, Manifest.permission.READ_CALENDAR}, 2);
//
//            } else {
//
//
//            }
//
//
//        } else {
//
//            if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_CALENDAR) == PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(this, Manifest.permission.READ_CALENDAR) == PackageManager.PERMISSION_GRANTED) {
//
//
//            } else {
//
//                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_CALENDAR, Manifest.permission.READ_CALENDAR}, 2);
//
//            }
//
//
//        }
//    }
//
//    public void askpermissionsLocation() {
//        if (Build.VERSION.SDK_INT < 23) {
//
//            if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
//                // TODO: Consider calling
//                //    Activity#requestPermissions
//                // here to request the missing permissions, and then overriding
//                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
//                //                                          int[] grantResults)
//                // to handle the case where the user grants the permission. See the documentation
//                // for Activity#requestPermissions for more details.
//                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 3);
//
//
//            } else {
//            }
//
//
//        } else {
//
//            if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
//
//
//            } else {
//
//                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 3);
//
//            }
//
//
//        }
//
//    }

    public void getFireBaseToken() {
        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                    @Override
                    public void onComplete(@NonNull Task<InstanceIdResult> task) {
                        if (!task.isSuccessful()) {
                            Log.w("Home", "getInstanceId failed", task.getException());
                            return;
                        }

                        // Get new Instance ID token
                        String token = task.getResult().getToken();

                        // Log and toast
                        String msg = token;
                        fireBaseToken = token;

                        HashMap<String, Object> body = new HashMap<>();
                        body.put("token", fireBaseToken);

                        sendFcmToken(body);

                        Log.d("Home", msg);
                    }
                });
    }

    private void sendFcmToken(HashMap<String, Object> body) {
        UserApi retrofitAPI = ServiceClient.createService(UserApi.class, getApplicationContext());
        //  String token = Global.getInstance(getContext()).getFromSharedPreferneces(Constants.SH_Token);
        retrofitAPI.sendFcmToken(body).enqueue(new Callback<RESPONSE>() {
            @Override
            public void onResponse(Call<RESPONSE> call, Response<RESPONSE> response) {
                if (response.code() == 200) {
                    RESPONSE result = response.body();

                    //Toast.makeText(getApplicationContext(), result.getMessage(), Toast.LENGTH_LONG).show();
                } else {
                    APIError error = ErrorUtils.parseError(response);
                    Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_LONG).show();

                }
            }

            @Override
            public void onFailure(Call<RESPONSE> call, Throwable throwable) {

                Toast.makeText(getApplicationContext(), getString(R.string.errorServer), Toast.LENGTH_LONG).show();
            }
        });
    }
}
