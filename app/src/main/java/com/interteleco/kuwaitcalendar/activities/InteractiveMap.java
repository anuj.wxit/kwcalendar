package com.interteleco.kuwaitcalendar.activities;

import androidx.appcompat.widget.AppCompatEditText;
import androidx.fragment.app.FragmentActivity;

import android.content.res.Configuration;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.interteleco.kuwaitcalendar.R;
import com.interteleco.kuwaitcalendar.dialogs.MapDialog;
import com.interteleco.kuwaitcalendar.server.EventApi;
import com.interteleco.kuwaitcalendar.server.ServiceClient;
import com.interteleco.kuwaitcalendar.server.models.EventData;
import com.interteleco.kuwaitcalendar.utils.Global;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class InteractiveMap extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    String title = "";
    String category = "all";
    int duration = 1;
    List<EventData> eventsLocation;

    String tag = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        loadLocaleLang();
        setContentView(R.layout.interactive_map);
        if (Global.getInstance(this).getFromSharedPreferneces("mylang").equalsIgnoreCase("en")) {

            getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
            //    getWindow().getDecorView().setTextAlignment(View.TEXT_ALIGNMENT_VIEW_END);
            getWindow().getDecorView().setTextDirection(View.TEXT_DIRECTION_LTR);

        } else {
            getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
            //    getWindow().getDecorView().setTextAlignment(View.TEXT_ALIGNMENT_VIEW_END);
            getWindow().getDecorView().setTextDirection(View.TEXT_DIRECTION_RTL);
        }
        eventsLocation = new ArrayList<EventData>();
        ImageView menu = (ImageView) findViewById(R.id.menuBtn);
        ImageView search = (ImageView) findViewById(R.id.searchBtn);

        AppCompatEditText txtSearch = (AppCompatEditText) findViewById(R.id.txtSearch);
        customizeToolbar(menu, getResources().getDrawable(R.drawable.arrow_leftb), getResources().getDrawable(R.drawable.arrow_right));
        txtSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                search.setImageResource(R.drawable.magnify);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                tag = "searh";
                mMap.clear();
                getMap(txtSearch.getText().toString(), category, 1);

            }
        });
        menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        //  getMap(title,category,duration);
//  EventData eventData=new EventData();
//     eventData.setTitle_en("test");
//        eventData.setLat(30.039452);
//        eventData.setLongt(31.202534);
//       eventsLocation.add(eventData);
//        EventData event=new EventData();
//        event.setTitle_en("test2");
//        event.setLat(30.060354);
//        event.setLongt(31.345560);
//        eventsLocation.add(event);

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;


        getMap(title, category, duration);


//   for(int i=0;i<eventsLocation.size();i++) {
//    // Add a marker in Sydney and move the camera
//    LatLng location = new LatLng(eventsLocation.get(i).getLat(), eventsLocation.get(i).getLongt());
//    mMap.addMarker(new MarkerOptions().position(location).title(eventsLocation.get(i).getTitle_en()));
//    mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(location,10));
//}

    }

    public void getMap(String title, String category, int duration) {

        EventApi retrofitAPI = ServiceClient.createService(EventApi.class, this);
        retrofitAPI.getMapEvents(title, category, duration).enqueue(new Callback<List<EventData>>() {
            @Override
            public void onResponse(Call<List<EventData>> call, Response<List<EventData>> response) {
                if (response.code() == 200) {

                    eventsLocation = response.body();
                    for (int i = 0; i < eventsLocation.size(); i++) {
                        // Add a marker in Sydney and move the camera

                        LatLng location = new LatLng(eventsLocation.get(i).getLat(), eventsLocation.get(i).getLongt());
                        if (Global.getInstance(getApplicationContext()).getFromSharedPreferneces("mylang").equalsIgnoreCase("en"))
                            mMap.addMarker(new MarkerOptions().position(location).title(eventsLocation.get(i).getTitle_en())).setTag(i);
                        else

                            mMap.addMarker(new MarkerOptions().position(location).title(eventsLocation.get(i).getTitle_ar())).setTag(i);
                        // marker.showInfoWindow();

                        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(location, 8));
                        mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
                            @Override
                            public boolean onMarkerClick(Marker marker) {
                                int position = (int) (marker.getTag());
                                MapDialog mapDialog = new MapDialog();
                                if (Global.getInstance(getApplicationContext()).getFromSharedPreferneces("mylang").equalsIgnoreCase("en")) {
                                    String title = eventsLocation.get(position).getTitle_en();
                                    mapDialog.title = title;
                                } else {
                                    String title = eventsLocation.get(position).getTitle_ar();
                                    mapDialog.title = title;
                                }


                                mapDialog.id = eventsLocation.get(position).get_id();
                                mapDialog.lat = eventsLocation.get(position).getLat();
                                mapDialog.longt = eventsLocation.get(position).getLongt();
                                mapDialog.show(getSupportFragmentManager(), "mapDialog");

                                //    Toast.makeText(getApplicationContext(),  ""+ eventsLocation.get(position).getTitle_en()+"", Toast.LENGTH_LONG).show();
                                return false;
                            }
                        });

                    }

                } else {
                    //    if (swipeRefreshLayout != null)
                    //    swipeRefreshLayout.setRefreshing(false);
                    //       progressBar.setVisibility(View.GONE);
                    Toast.makeText(getApplicationContext(), getString(R.string.serverError), Toast.LENGTH_LONG).show();

                }
            }

            @Override
            public void onFailure(Call<List<EventData>> call, Throwable throwable) {

                // if (swipeRefreshLayout != null)
                //swipeRefreshLayout.setRefreshing(false);
                //    progressBar.setVisibility(View.GONE);
                Toast.makeText(getApplicationContext(), getString(R.string.serverError), Toast.LENGTH_LONG).show();
            }
        });
    }

    public void setLocale(String lang) {
        Locale locale = new Locale(lang);
        locale.setDefault(locale);
        Configuration configuration = new Configuration();
        configuration.locale = locale;
        getResources().updateConfiguration(configuration, getResources().getDisplayMetrics());
        Global.getInstance(this).saveInSharedPrefernces("mylang", lang);
    }

    public void loadLocaleLang() {
        String language = Global.getInstance(this).getFromSharedPreferneces("mylang");

        if (language != null)
            setLocale(language);
        else
            setLocale("en");

    }

    public void customizeToolbar(ImageView imageView, Drawable drawable_en, Drawable drawable_ar) {
        if (Global.getInstance(this).getFromSharedPreferneces("mylang").equalsIgnoreCase("en"))

            imageView.setImageDrawable(drawable_en);
        else
            imageView.setImageDrawable(drawable_ar);

    }
}
