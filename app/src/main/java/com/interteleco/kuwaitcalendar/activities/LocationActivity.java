package com.interteleco.kuwaitcalendar.activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import com.google.android.gms.common.api.Status;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.api.net.PlacesClient;
import com.google.android.libraries.places.widget.AutocompleteSupportFragment;
import com.google.android.libraries.places.widget.listener.PlaceSelectionListener;
import com.interteleco.kuwaitcalendar.R;


import java.util.Arrays;
import java.util.List;

public class Location extends AppCompatActivity {
    PlacesClient placesClient;

    List<Place.Field> placeFields = Arrays.asList(Place.Field.ID, Place.Field.NAME, Place.Field.ADDRESS, Place.Field.LAT_LNG);
    AutocompleteSupportFragment places_fragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_location);
        initiatePlaces();
        setPlacesAutoComplete();
    }

    private void setPlacesAutoComplete() {

       /* places_fragment = (AutocompleteSupportFragment) getSupportFragmentManager().findFragmentById(R.id.places_autocomplete_fragment);
        places_fragment.setPlaceFields(placeFields);
        places_fragment.setCountry("KW");
        places_fragment.setOnPlaceSelectedListener(new PlaceSelectionListener() {
            @Override
            public void onPlaceSelected(@NonNull Place place) {
                Toast.makeText(getApplicationContext(), place.getLatLng().toString(), Toast.LENGTH_LONG).show();
                String latlong = place.getLatLng().toString();
                String values = latlong.substring(9, latlong.length());
                Intent location = new Intent();
                location.putExtra("location", place.getLatLng().toString());
                //  location.putExtra("location","LAT/LNG:(29.375859,47.9774052");
                setResult(2, location);

            }

            @Override
            public void onError(@NonNull Status status) {
                Toast.makeText(getApplicationContext(), "place not found", Toast.LENGTH_LONG).show();
            }
        });*/

        places_fragment = (AutocompleteSupportFragment)
                getSupportFragmentManager().findFragmentById(R.id.places_autocomplete_fragment);

// Specify the types of place data to return.
        places_fragment.setPlaceFields(placeFields);
//        places_fragment.setCountry("IN");
// Set up a PlaceSelectionListener to handle the response.
        places_fragment.setOnPlaceSelectedListener(new PlaceSelectionListener() {
            @Override
            public void onPlaceSelected(Place place) {
                // TODO: Get info about the selected place.
//                Toast.makeText(getApplicationContext(), place.getLatLng().toString(), Toast.LENGTH_LONG).show();
                String latlong = place.getLatLng().toString();
                String values = latlong.substring(9, latlong.length());
                Intent location = new Intent();
                location.putExtra("location", place.getLatLng().toString());
                //  location.putExtra("location","LAT/LNG:(29.375859,47.9774052");
                setResult(2, location);
            }

            @Override
            public void onError(Status status) {
                // TODO: Handle the error.
                Toast.makeText(getApplicationContext(), "place not found"+status, Toast.LENGTH_LONG).show();
            }
        });
    }


    private void initiatePlaces() {
        Places.initialize(this, getString(R.string.places_api_key));

        placesClient = Places.createClient(this);
    }

}