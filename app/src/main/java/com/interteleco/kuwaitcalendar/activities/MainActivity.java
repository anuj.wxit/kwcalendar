package com.interteleco.kuwaitcalendar.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GravityCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.Toast;

import com.interteleco.kuwaitcalendar.R;
import com.interteleco.kuwaitcalendar.fragments.UserFragment;
import com.interteleco.kuwaitcalendar.fragments.VerifyEmail;
import com.interteleco.kuwaitcalendar.fragments.VerifyPhone;
import com.interteleco.kuwaitcalendar.utils.Constants;
import com.interteleco.kuwaitcalendar.utils.Global;

import java.util.Locale;


public class MainActivity extends AppCompatActivity {
String tag="";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        loadLocaleLang();
        setContentView(R.layout.activity_main);
        if (Global.getInstance(this).getFromSharedPreferneces("mylang").equalsIgnoreCase("en")) {

            getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
            //    getWindow().getDecorView().setTextAlignment(View.TEXT_ALIGNMENT_VIEW_END);
            getWindow().getDecorView().setTextDirection(View.TEXT_DIRECTION_LTR);

        } else {
            getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
            //    getWindow().getDecorView().setTextAlignment(View.TEXT_ALIGNMENT_VIEW_END);
            getWindow().getDecorView().setTextDirection(View.TEXT_DIRECTION_RTL);
        }

        tag=getIntent().getStringExtra("tag");
        UserFragment userFragment = new UserFragment();


        userFragment.tag=tag;
        addFragament(userFragment);


//  if (tag.equalsIgnoreCase("signUp")){
//      Intent intent = new Intent(Global.getInstance(this).BROADCAST_ACTION);
//      intent.putExtra("action", Constants.SignUP);
//
//     sendBroadcast(intent);
//
//
//    }
   }

    public void hideKeyBoard() {
        InputMethodManager inputManager = (InputMethodManager) this.getSystemService(Context.INPUT_METHOD_SERVICE);
        if (this.getCurrentFocus() != null)
            inputManager.hideSoftInputFromWindow(this.getCurrentFocus().getWindowToken(), 0);
    }

    public void addFragament(Fragment objBaseFragment) {
        FragmentTransaction objFragmentTransaction = getSupportFragmentManager()
                .beginTransaction();
        objBaseFragment.setRetainInstance(false);
        objFragmentTransaction.add(R.id.container, objBaseFragment, objBaseFragment.getClass().toString());
        objFragmentTransaction.addToBackStack(null);
        objFragmentTransaction.commit();
    }

    public void setLocale(String lang) {
        Locale locale = new Locale(lang);
        locale.setDefault(locale);
        Configuration configuration = new Configuration();
        configuration.locale = locale;
       getResources().updateConfiguration(configuration,getResources().getDisplayMetrics());
        Global.getInstance(this).saveInSharedPrefernces("mylang", lang);
    }

    public void loadLocaleLang() {
        String language = Global.getInstance(this).getFromSharedPreferneces("mylang");

        if (language != null)
            setLocale(language);
        else
            setLocale("en");

    }

    public void customizeToolbar(ImageView imageView, Drawable drawable_en, Drawable drawable_ar) {
        if (Global.getInstance(this).getFromSharedPreferneces("mylang").equalsIgnoreCase("en"))

            imageView.setImageDrawable(drawable_en);
        else
            imageView.setImageDrawable(drawable_ar);

    }

    @Override
    public void onBackPressed() {
        if (getSupportFragmentManager().getBackStackEntryCount() > 1) {
        Fragment fragment=getSupportFragmentManager().findFragmentByTag("verify");
            if(fragment instanceof VerifyPhone||fragment instanceof VerifyEmail){

            }else{

                getSupportFragmentManager().popBackStack();

            }

        } else if(getSupportFragmentManager().findFragmentByTag("verify")!=null) {
         //   Toast.makeText(getApplicationContext(),"back", Toast.LENGTH_LONG).show();
        }else{

            this.finish();
        }

    }
    public void addFragamentbyTAG(Fragment objBaseFragment, String TAG) {

        FragmentTransaction objFragmentTransaction = getSupportFragmentManager()
                .beginTransaction();
        objBaseFragment.setRetainInstance(true);
        objFragmentTransaction.add(R.id.container, objBaseFragment, TAG);
        objFragmentTransaction.addToBackStack(TAG);
        objFragmentTransaction.commit();

    }

}
