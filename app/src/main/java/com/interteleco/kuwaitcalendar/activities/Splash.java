package com.interteleco.kuwaitcalendar.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.WindowManager;


import com.interteleco.kuwaitcalendar.R;
import com.interteleco.kuwaitcalendar.utils.Constants;
import com.interteleco.kuwaitcalendar.utils.Global;

import java.util.Timer;
import java.util.TimerTask;

public class Splash extends AppCompatActivity {
    int Delay;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
//                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_spalsh);
        Delay = 3000;

        String authorized = Global.getInstance(this).getFromSharedPreferneces(Constants.Authorized);

        Timer RunSplash = new Timer();
        TimerTask ShowSplash = new TimerTask() {


            @Override
            public void run() {
                finish();
                Intent objIntent = new Intent(Splash.this, Home.class);

                startActivity(objIntent);

//                if (login.equalsIgnoreCase("true")) {
//                    Intent objIntent = new Intent(Splash.this, Home.class);
//                    startActivity(objIntent);
//                } else {
//                    Intent objIntent = new Intent(Splash.this, MainActivity.class);
//                    startActivity(objIntent);
//
//                }

            }

        };

        RunSplash.schedule(ShowSplash, Delay);


    }

}