package com.interteleco.kuwaitcalendar.adapters;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;

import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.interteleco.kuwaitcalendar.R;
import com.interteleco.kuwaitcalendar.activities.Home;
import com.interteleco.kuwaitcalendar.fragments.EventDetails;

import com.interteleco.kuwaitcalendar.server.EventApi;
import com.interteleco.kuwaitcalendar.server.ServiceClient;
import com.interteleco.kuwaitcalendar.server.models.APIError;
import com.interteleco.kuwaitcalendar.server.models.EventData;

import com.interteleco.kuwaitcalendar.server.models.RESPONSE;
import com.interteleco.kuwaitcalendar.utils.Constants;
import com.interteleco.kuwaitcalendar.utils.ErrorUtils;
import com.interteleco.kuwaitcalendar.utils.Fonts;
import com.interteleco.kuwaitcalendar.utils.Global;
import com.interteleco.kuwaitcalendar.utils.ResponseUtil;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AllEventsAdapter extends RecyclerView.Adapter<AllEventsAdapter.EventHolder> {
    ArrayList<EventData> data;
    Context context;
    int week = 0;
    int recom = 1;
    ImageView favourite;
    boolean isLiked;
    int selectedindex;
    String itemId;

    public AllEventsAdapter(Context context, ArrayList<EventData> data) {
        this.context = context;
        this.data = data;
    }


    @NonNull
    @Override
    public AllEventsAdapter.EventHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.all_event_item, parent, false);
        return new AllEventsAdapter.EventHolder(v);


    }


    @Override
    public void onBindViewHolder(@NonNull AllEventsAdapter.EventHolder holder, int position) {
        EventData event = data.get(position);
        String Id = data.get(position).get_id();
        String authorized = Global.getInstance(context).getFromSharedPreferneces(Constants.Authorized);
        itemId = data.get(position).get_id();
        selectedindex = position;
        isLiked = data.get(position).isLikes();
        String name = data.get(position).getTitle_en();
        String url = data.get(position).getImageURL();
       String shareUrl = Constants.SharedURL + "events/" +data.get(position).get_id();
        if (Global.getInstance(context).getFromSharedPreferneces("mylang").equalsIgnoreCase("en"))

            holder.txtDescription.setText(data.get(position).getTitle_en());
        else
            holder.txtDescription.setText(data.get(position).getTitle_ar());
        //img.setImageResource(images[position]);
        if (data.get(position).isLikes() == true) {
            holder.favourite.setImageResource(R.drawable.fav_active);
            holder.favourite.setTag("unlike");
        } else {
            holder.favourite.setImageResource(R.drawable.fav_2);
            holder.favourite.setTag("like");
        }


        Glide.with(context).load(Constants.ImgURL + data.get(position).getImageURL()).placeholder(R.drawable.kwcalendarthum).into(holder.imageEvent);
        Fonts.set(new TextView[]{holder.txtDescription}, context, 1);
        holder.favourite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (authorized != null && !authorized.equalsIgnoreCase("")) {
                    if (holder.favourite.getTag().toString().equalsIgnoreCase("Like"))
                        holder.likeEvent(Id, "like");
                    else
                        holder.likeEvent(Id, "unlike");
                } else {
                    Toast.makeText(context, context.getString(R.string.loginFirst), Toast.LENGTH_LONG).show();

                }
            }

        });
        holder.share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

              //  if (authorized != null && !authorized.equalsIgnoreCase("")) {
                if (ContextCompat.checkSelfPermission(context, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                    holder.shareEvent(Id);
                    holder.shareImage(name, url,shareUrl);
                }else{
                    ((Home)context).askpermissionsToShare();


                }
              //  } else {
              //      Toast.makeText(context, context.getString(R.string.loginFirst), Toast.LENGTH_LONG).show();

             //   }

            }
        });


    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class EventHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.description)
        TextView txtDescription;
        @BindView(R.id.imageEvent)
        ImageView imageEvent;
        @BindView(R.id.imgFav_id)
        ImageView favourite;

        @BindView(R.id.imgshare_id)
        ImageView share;

        EventData eventData;

        public EventHolder(@NonNull View itemView) {
            super(itemView);

            ButterKnife.bind(this, itemView);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    eventData = data.get(getAdapterPosition());
                    EventDetails eventDetails = new EventDetails();
                    eventDetails.id = eventData.get_id();
                    ((Home) context).addFragament(eventDetails);

                }
            });
//
//            favourite.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    likeEvent(itemId);
//                }
//            });
//
//
        }


        public void likeEvent(String ID, String tag) {

            String token = Global.getInstance(context).getFromSharedPreferneces(Constants.SH_Token);

            HashMap<String, Object> body = new HashMap<>();

            body.put("_id", ID);

            // body.put("lang", currentLanguage.toLowerCase());

            // pDialog = showLoadingDialog(getActivity());

            EventApi retrofitAPI = ServiceClient.createService(EventApi.class, context);
            retrofitAPI.likeEvent(token, body).enqueue(new Callback<RESPONSE>() {
                @Override
                public void onResponse(Call<RESPONSE> call, Response<RESPONSE> response) {
                    if (response.code() == 200) {
                        //    hideLoadingDialog(pDialog);
                        RESPONSE result = response.body();
                        // EventData eventData=data.get(selectedindex);
                        if (tag.equalsIgnoreCase("like")) {
                            favourite.setImageResource(R.drawable.fav_active);
                            favourite.setTag("unlike");
                            //     notifyDataSetChanged();

                        } else {

                            favourite.setImageResource(R.drawable.fav_2);
                            favourite.setTag("like");
                            //   notifyDataSetChanged();
                        }
                        Toast.makeText(context, "" + result.getSuccess(), Toast.LENGTH_LONG).show();
                        //  getChildFragmentManager().popBackStack();
                    } else {
                        APIError error = ErrorUtils.parseError(response);
                        Toast.makeText(context, error.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }

                @Override
                public void onFailure(Call<RESPONSE> call, Throwable throwable) {
                    // hideLoadingDialog(pDialog);
                    Toast.makeText(context, context.getString(R.string.errorServer), Toast.LENGTH_LONG).show();
                }
            });


        }

        public void shareEvent(String ID) {

            String token = Global.getInstance(context).getFromSharedPreferneces(Constants.SH_Token);

            HashMap<String, Object> body = new HashMap<>();

            body.put("_id", ID);

            // body.put("lang", currentLanguage.toLowerCase());

            // pDialog = showLoadingDialog(getActivity());

            EventApi retrofitAPI = ServiceClient.createService(EventApi.class, context);
            retrofitAPI.shareEvent(token, body).enqueue(new Callback<RESPONSE>() {
                @Override
                public void onResponse(Call<RESPONSE> call, Response<RESPONSE> response) {
                    if (response.code() == 200) {
                        //    hideLoadingDialog(pDialog);
                        RESPONSE result = response.body();
                        //   String message = response.body().getMessage();
                        //   System.out.print(message);
                        //   data.remove(selectedindex);
                        //  notifyDataSetChanged();


                       // Toast.makeText(context, "" + result.getSuccess(), Toast.LENGTH_LONG).show();
                        //  getChildFragmentManager().popBackStack();
                    } else {
                        //      hideLoadingDialog(pDialog);
                        RESPONSE result = ResponseUtil.parseError(response);
                        Toast.makeText(context, result.getMessage(), Toast.LENGTH_LONG).show();

                    }
                }

                @Override
                public void onFailure(Call<RESPONSE> call, Throwable throwable) {
                    // hideLoadingDialog(pDialog);
                    Toast.makeText(context, context.getString(R.string.errorServer), Toast.LENGTH_LONG).show();
                }
            });


        }

        public void shareImage(String name, String url, String shareUrl) {


            try {


                Glide.with(context)
                        .load(Constants.ImgURL + url)
                        .asBitmap().skipMemoryCache(true).diskCacheStrategy(DiskCacheStrategy.NONE)

                        .into(new SimpleTarget<Bitmap>(250, 250) {
                            @Override
                            public void onResourceReady(Bitmap resource, GlideAnimation glideAnimation) {

                                Intent intent = new Intent(Intent.ACTION_SEND);
                                intent.putExtra(Intent.EXTRA_TEXT, name +" "+"\n"+ shareUrl+" "+"\n"+ context.getString(R.string.downloadGoogle)+"\n"+" https://play.google.com/store/apps/details?id=com.interteleco.kuwaitcalendar "
                                        +"\n"+context.getString(R.string.downloadApple) +"\n"+" https://apps.apple.com/us/app/kuwait-calendar/id1479037618?ls=1  ");
                                String path = MediaStore.Images.Media.insertImage(context.getContentResolver(), resource, "", null);
                                //  Log.i("quoteswahttodo", "is onresoursereddy" + path);

                                Uri screenshotUri = Uri.parse(path);

                                // Log.i("quoteswahttodo", "is onresoursereddy" + screenshotUri);

                                intent.putExtra(Intent.EXTRA_STREAM, screenshotUri);
                                intent.setType("image/*");

                                context.startActivity(Intent.createChooser(intent, "Share image via..."));
                            }

                            @Override
                            public void onLoadFailed(Exception e, Drawable errorDrawable) {
                                // Toast.makeText(getContext(), "Something went wrong", Toast.LENGTH_SHORT).show();

                                super.onLoadFailed(e, errorDrawable);
                            }

                            @Override
                            public void onLoadStarted(Drawable placeholder) {
                                //  Toast.makeText(getContext(), "Starting", Toast.LENGTH_SHORT).show();

                                super.onLoadStarted(placeholder);
                            }
                        });

            } catch (Exception e) {
                e.printStackTrace();
            }
        }


    }

}



