package com.interteleco.kuwaitcalendar.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.interteleco.kuwaitcalendar.R;
import com.interteleco.kuwaitcalendar.fragments.Hfragment;

import com.interteleco.kuwaitcalendar.server.models.CategoryData;
import com.interteleco.kuwaitcalendar.utils.Fonts;
import com.interteleco.kuwaitcalendar.utils.Global;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CategoryAdapter extends RecyclerView.Adapter<CategoryAdapter.Holder> {
    ArrayList<CategoryData.Category> data;
    Context context;
    int selectedIndex =-1;
    public Hfragment hfragment;
    boolean isSelected = false;

    public CategoryAdapter(Context context, ArrayList<CategoryData.Category> data, Hfragment hfragment) {
        this.context = context;
        this.data = data;
        this.hfragment = hfragment;
    }


    @NonNull
    @Override
    public CategoryAdapter.Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.tab_view, parent, false);
        return new CategoryAdapter.Holder(v);
    }
    @Override
    public void onBindViewHolder(@NonNull CategoryAdapter.Holder holder, int position) {
        if (Global.getInstance(context).getFromSharedPreferneces("mylang").equalsIgnoreCase("en"))
        holder.button.setText(data.get(position).getName_en());

        else
            holder.button.setText(data.get(position).getName_ar());
        if(isSelected==false) {
            if (selectedIndex == position || position == 0) {
                holder.button.setTextColor(context.getResources().getColor(R.color.White));
            } else {
                holder.button.setTextColor(context.getResources().getColor(R.color.gray));

            }
        }else{
            if (selectedIndex == position) {
                holder.button.setTextColor(context.getResources().getColor(R.color.White));
            } else {
                holder.button.setTextColor(context.getResources().getColor(R.color.gray));

            }

        }
        holder.button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
              isSelected=true;
                selectedIndex = position;
                //  Toast.makeText(context, data.get(position).get_id(), Toast.LENGTH_LONG).show();
                hfragment.setCategoryId(data.get(position).get_id());

                hfragment.categoryId = data.get(position).get_id();
                hfragment.rpage = 1;
                hfragment.wpage = 1;
                hfragment.LoadCategoryEvents(data.get(position).get_id(), 10, 1);
                notifyDataSetChanged();


//        if(isSelected==false){
//            holder.button.setTextColor(context.getResources().getColor(R.color.White));
//
//            isSelected=true;
//
//        }
//        else{
//
//            holder.button.setTextColor(context.getResources().getColor(R.color.gray));
//            isSelected=false;
//        }
            }
        });
        Fonts.set(new TextView[]{holder.button},context,0);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class Holder extends RecyclerView.ViewHolder {

        @BindView(R.id.tab)
        TextView button;


        public Holder(@NonNull View itemView) {
            super(itemView);

            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
//                    if (getAdapterPosition() == RecyclerView.NO_POSITION) return;
//
//                    // Updating old as well as new positions
//                    notifyItemChanged(selectedIndex);
//                    selectedIndex= getAdapterPosition();
//                    notifyItemChanged(selectedIndex);


                }
            });
        }
    }


}
