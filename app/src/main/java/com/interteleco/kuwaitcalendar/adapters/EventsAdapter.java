package com.interteleco.kuwaitcalendar.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.interteleco.kuwaitcalendar.R;
import com.interteleco.kuwaitcalendar.activities.Home;
import com.interteleco.kuwaitcalendar.fragments.EventDetails;

import com.interteleco.kuwaitcalendar.server.models.EventData;

import com.interteleco.kuwaitcalendar.utils.Constants;
import com.interteleco.kuwaitcalendar.utils.Fonts;
import com.interteleco.kuwaitcalendar.utils.Global;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class EventsAdapter extends RecyclerView.Adapter<EventsAdapter.EventHolder> {

    ArrayList<EventData> data;
    Context context;
    int week = 0;
    int recom = 1;

    public EventsAdapter( Context context, ArrayList<EventData> data) {
        this.context = context;
        this.data = data;
    }


    @NonNull
    @Override
    public EventsAdapter.EventHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == week) {
            View v = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.week_event_item, parent, false);
            return new EventsAdapter.EventHolder(v);

        } else {
            View v = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.recom_event_item, parent, false);
            return new EventsAdapter.EventHolder(v);


        }
    }

    @Override
    public void onBindViewHolder(@NonNull EventsAdapter.EventHolder holder, int position) {
        if (data.size() > 0) {
            if (Global.getInstance(context).getFromSharedPreferneces("mylang").equalsIgnoreCase("en")) {
                holder.txtDescription.setText(data.get(position).getTitle_en());

                // Glide.with(context).load("http://41.41.86.210:3008/events/def.jpg").into(holder.imageEvent);
            } else {
                holder.txtDescription.setText(data.get(position).getTitle_ar());
                //img.setImageResource(images[position]);

                //  Glide.with(context).load("http://41.41.86.210:3008/events/def.jpg").into(holder.imageEvent);
            }

            Glide.with(context).load(Constants.ImgURL + data.get(position).getImageURL()).placeholder(R.drawable.kwcalendarthum).into(holder.imageEvent);

            //    Glide.with(context).load("http://41.41.86.210:3008/events/def.jpg").into(holder.imageEvent);
            Fonts.set(new TextView[]{holder.txtDescription}, context, 1);

        } else {
            holder.noContent.setVisibility(View.VISIBLE);

        }
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public int getItemViewType(int position) {
        if (data.get(position).getType().equalsIgnoreCase("week"))
            return week;
        else
            return recom;

    }

    public class EventHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.description)
        TextView txtDescription;
        @BindView(R.id.imageEvent)
        ImageView imageEvent;
        EventData eventData;
        @BindView(R.id.noContent)
        TextView noContent;

        public EventHolder(@NonNull View itemView) {
            super(itemView);

            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    eventData = data.get(getAdapterPosition());
                    EventDetails eventDetails = new EventDetails();
                    eventDetails.id = eventData.get_id();
                    ((Home) context).addFragament(eventDetails);


                }
            });
        }
    }


}


