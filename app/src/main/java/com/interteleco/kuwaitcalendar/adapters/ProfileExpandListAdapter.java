package com.interteleco.kuwaitcalendar.adapters;

import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;

import com.interteleco.kuwaitcalendar.R;
import com.interteleco.kuwaitcalendar.server.EventApi;
import com.interteleco.kuwaitcalendar.server.ServiceClient;
import com.interteleco.kuwaitcalendar.server.models.APIError;
import com.interteleco.kuwaitcalendar.server.models.EventData;
import com.interteleco.kuwaitcalendar.server.models.ExpandedItem;
import com.interteleco.kuwaitcalendar.server.models.RESPONSE;
import com.interteleco.kuwaitcalendar.utils.Constants;
import com.interteleco.kuwaitcalendar.utils.ErrorUtils;
import com.interteleco.kuwaitcalendar.utils.Fonts;
import com.interteleco.kuwaitcalendar.utils.Global;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProfileExpandListAdapter extends BaseExpandableListAdapter {

    AlertDialog alertDialog;
    AlertDialog.Builder builder;
    private Context _context;
    private List<ExpandedItem> header; // header titles
    // Child data in format of header title, child title
    //int childIndex, groupIndex;
    String itemId;
    private HashMap<ExpandedItem, ArrayList<EventData>> child = new HashMap<>();

    public ProfileExpandListAdapter(Context context, List<ExpandedItem> listDataHeader,
                                    HashMap<ExpandedItem, ArrayList<EventData>> listChildData) {
        this._context = context;
        this.header = listDataHeader;
        this.child = listChildData;
    }

    @Override
    public Object getChild(int groupPosition, int childPosititon) {

        // This will return the child
        return child.get(header.get(groupPosition)).get(
                childPosititon);
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public View getChildView(int groupPosition, final int childPosition,
                             boolean isLastChild, View convertView, ViewGroup parent) {


        if (groupPosition == 0) {
            int groupIndex = groupPosition;
            int childIndex = childPosition;
            LayoutInflater infalInflater = (LayoutInflater) this._context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            EventData eventData = (EventData) getChild(groupPosition, childPosition);
            itemId = eventData.get_id();
            convertView = infalInflater.inflate(R.layout.event_item, parent, false);
            TextView name = (TextView) convertView.findViewById(R.id.name);
            TextView status = (TextView) convertView.findViewById(R.id.status);
            ImageView delete = (ImageView) convertView.findViewById(R.id.delete);
            Fonts.set(new TextView[]{name, status}, _context, 1);
            if (Global.getInstance(_context).getFromSharedPreferneces("mylang").equalsIgnoreCase("en"))
                name.setText(eventData.getTitle_en());
            else
                name.setText(eventData.getTitle_ar());

            if (eventData.getStatus() == 1) {
                delete.setVisibility(View.VISIBLE);
                status.setText(_context.getString(R.string.pending));

                delete.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        showAlert("delete", groupIndex, childIndex);

                        //  deletEvent(eventData.get_id());
                    }
                });


            } else if (eventData.getStatus() == 2) {
                delete.setVisibility(View.GONE);
                status.setText(_context.getString(R.string.accepted));

            } else {
                delete.setVisibility(View.GONE);
                status.setText(_context.getString(R.string.rejected));
            }
        } else if (groupPosition == 1) {
            LayoutInflater infalInflater = (LayoutInflater) this._context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            EventData eventData = (EventData) getChild(groupPosition, childPosition);
            itemId = eventData.get_id();
            int groupIndex = groupPosition;
            int childIndex = childPosition;
            convertView = infalInflater.inflate(R.layout.favourite_item, parent, false);
            TextView name = (TextView) convertView.findViewById(R.id.name);
            TextView date = (TextView) convertView.findViewById(R.id.date);
            TextView venue = (TextView) convertView.findViewById(R.id.venue);
            ImageView delete = (ImageView) convertView.findViewById(R.id.delete);
            Fonts.set(new TextView[]{date, venue, name}, _context, 1);
            if (Global.getInstance(_context).getFromSharedPreferneces("mylang").equalsIgnoreCase("en"))
                name.setText(eventData.getTitle_en());
            else
                name.setText(eventData.getTitle_ar());
            try {
                SimpleDateFormat spf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
                java.util.Date dateStart = spf.parse(eventData.getStartDate());
                spf = new SimpleDateFormat("dd MMM yyyy");
                String strdate = spf.format(dateStart);


                date.setText(strdate);
            } catch (Exception e) {
                e.printStackTrace();
            }

            if (Global.getInstance(_context).getFromSharedPreferneces("mylang").equalsIgnoreCase("en"))
                venue.setText(eventData.getVenueId().getName_en());
            else
                venue.setText(eventData.getVenueId().getName_ar());
            delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // removeFromCalendar(eventData.get_id());
                    showAlert("calendar", groupIndex, childIndex);

                }
            });

        } else {
            LayoutInflater infalInflater = (LayoutInflater) this._context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            EventData eventData = (EventData) getChild(groupPosition, childPosition);
            itemId = eventData.get_id();
            int groupIndex = groupPosition;
            int childIndex = childPosition;
            convertView = infalInflater.inflate(R.layout.favourite_item, parent, false);
            TextView name = (TextView) convertView.findViewById(R.id.name);
            TextView date = (TextView) convertView.findViewById(R.id.date);
            TextView venue = (TextView) convertView.findViewById(R.id.venue);
            ImageView delete = (ImageView) convertView.findViewById(R.id.delete);
            Fonts.set(new TextView[]{date, venue, name}, _context, 1);

            if (Global.getInstance(_context).getFromSharedPreferneces("mylang").equalsIgnoreCase("en"))
                name.setText(eventData.getTitle_en());
            else
                name.setText(eventData.getTitle_ar());
            date.setText(eventData.getStartDate());
            try {
                SimpleDateFormat spf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
                java.util.Date dateStart = spf.parse(eventData.getStartDate());

                spf = new SimpleDateFormat("dd MMM yyyy");
                String strdate = spf.format(dateStart);


                date.setText(strdate);
            } catch (Exception e) {
                e.printStackTrace();
            }

            if (Global.getInstance(_context).getFromSharedPreferneces("mylang").equalsIgnoreCase("en"))
                venue.setText(eventData.getVenueId().getName_en());
            else
                venue.setText(eventData.getVenueId().getName_ar());


            delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showAlert("unlike", groupIndex, childIndex);

                    // unLikeEvent(eventData.get_id());
                }
            });

        }


        //   TextView child_text = (TextView) convertView.findViewById(R.id.child);

        //child_text.setText(childText);
        return convertView;
    }

    @Override
    public int getChildrenCount(int groupPosition) {

        // return children count
        return child.get(header.get(groupPosition)).size();
    }

    @Override
    public Object getGroup(int groupPosition) {

        // Get header position
        return header.get(groupPosition);
    }

    @Override
    public int getGroupCount() {

        // Get header size
        return header.size();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded,
                             View convertView, ViewGroup parent) {

        // Getting header title
        ExpandedItem headerTitle = (ExpandedItem) getGroup(groupPosition);

        // Inflating header layout and setting text
        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.expanded_list, parent, false);

        }

        TextView header_text = (TextView) convertView.findViewById(R.id.title);
        ImageView img = (ImageView) convertView.findViewById(R.id.icon);
        ImageView arrow = (ImageView) convertView.findViewById(R.id.arrow);
        header_text.setText(header.get(groupPosition).getTitle());
        img.setBackgroundResource(header.get(groupPosition).getIcon());
        Fonts.set(new TextView[]{header_text}, _context, 1);

        // If group is expanded then change the text into bold and change the
        // icon
        if (isExpanded) {
            if (child.get(header.get(groupPosition)).size() > 0) {
                arrow.setBackgroundResource(R.drawable.arrow_up);
                convertView.setBackgroundColor(_context.getResources().getColor(R.color.gradientRight));
                header_text.setTextColor(_context.getResources().getColor(R.color.White));

          if(groupPosition==0)
          img.setBackground(_context.getResources().getDrawable(R.drawable.events_menu));
           else if (groupPosition==1)
              img.setBackground(_context.getResources().getDrawable(R.drawable.calender_icon));
           else
              img.setBackground(_context.getResources().getDrawable(R.drawable.love_icon));
            }
            //  header_text.setTypeface(null, Typeface.BOLD);
            //    header_text.setCompoundDrawablesWithIntrinsicBounds(0, 0,
            //    R.drawable.ic_up, 0);
        } else {
            arrow.setBackgroundResource(R.drawable.down_arrow);
            convertView.setBackgroundColor(_context.getResources().getColor(R.color.GRAY1));
            header_text.setTextColor(_context.getResources().getColor(R.color.gray));
            // If group is not expanded then change the text back into normal

            // and change the icon

            //     header_text.setTypeface(null, Typeface.NORMAL);
            // header_text.setCompoundDrawablesWithIntrinsicBounds(0, 0,
            //   R.drawable.ic_down, 0);
        }

        return convertView;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

    public void deletEvent(String ID, int groupIndex, int childIndex) {

        String token = Global.getInstance(_context).getFromSharedPreferneces(Constants.SH_Token);

        HashMap<String, Object> body = new HashMap<>();

        body.put("_id", ID);

        EventApi retrofitAPI = ServiceClient.createService(EventApi.class, _context);
        retrofitAPI.deleteEvent(token, body).enqueue(new Callback<RESPONSE>() {
            @Override
            public void onResponse(Call<RESPONSE> call, Response<RESPONSE> response) {
                if (response.code() == 200) {
                    //    hideLoadingDialog(pDialog);
                    RESPONSE result = response.body();
                    child.get(header.get(groupIndex)).remove(childIndex);
                    Toast.makeText(_context,  result.getMessage(), Toast.LENGTH_LONG).show();
                    notifyDataSetChanged();
                //    Toast.makeText(_context,  _context.getResources().getString(R.string.deletedevent), Toast.LENGTH_LONG).show();
                    //  getChildFragmentManager().popBackStack();
                } else {
                    APIError error = ErrorUtils.parseError(response);
                    Toast.makeText(_context, error.getMessage(), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<RESPONSE> call, Throwable throwable) {
                // hideLoadingDialog(pDialog);
                Toast.makeText(_context, _context.getString(R.string.errorServer), Toast.LENGTH_LONG).show();
            }
        });


    }

    public void unLikeEvent(String ID, int groupIndex, int childIndex) {

        String token = Global.getInstance(_context).getFromSharedPreferneces(Constants.SH_Token);

        HashMap<String, Object> body = new HashMap<>();

        body.put("_id", ID);
//int group=groupIndex;
//int childi =childIndex;
        EventApi retrofitAPI = ServiceClient.createService(EventApi.class, _context);
        retrofitAPI.likeEvent(token, body).enqueue(new Callback<RESPONSE>() {
            @Override
            public void onResponse(Call<RESPONSE> call, Response<RESPONSE> response) {
                if (response.code() == 200) {
                    //    hideLoadingDialog(pDialog);
                    RESPONSE result = response.body();
                    child.get(header.get(groupIndex)).remove(childIndex);

                    notifyDataSetChanged();
                    Toast.makeText(_context,  _context.getResources().getString(R.string.deletedevent), Toast.LENGTH_LONG).show();
                    //  getChildFragmentManager().popBackStack();
                } else {
                    APIError error = ErrorUtils.parseError(response);
                    Toast.makeText(_context, error.getMessage(), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<RESPONSE> call, Throwable throwable) {
                // hideLoadingDialog(pDialog);
                Toast.makeText(_context, _context.getString(R.string.errorServer), Toast.LENGTH_LONG).show();
            }
        });


    }

    public void removeFromCalendar(String ID, int groupIndex, int childIndex) {

        String token = Global.getInstance(_context).getFromSharedPreferneces(Constants.SH_Token);

        HashMap<String, Object> body = new HashMap<>();

        body.put("_id", ID);

        EventApi retrofitAPI = ServiceClient.createService(EventApi.class, _context);
        retrofitAPI.removeCalendar(token, body).enqueue(new Callback<RESPONSE>() {
            @Override
            public void onResponse(Call<RESPONSE> call, Response<RESPONSE> response) {
                if (response.code() == 200) {
                    //    hideLoadingDialog(pDialog);
                    RESPONSE result = response.body();
                    child.get(header.get(groupIndex)).remove(childIndex);

                    notifyDataSetChanged();
                    Toast.makeText(_context,  _context.getResources().getString(R.string.deletedevent), Toast.LENGTH_LONG).show();
                    //  getChildFragmentManager().popBackStack();
                } else {
                    APIError error = ErrorUtils.parseError(response);
                    Toast.makeText(_context, error.getMessage(), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<RESPONSE> call, Throwable throwable) {
                // hideLoadingDialog(pDialog);
                Toast.makeText(_context, _context.getString(R.string.errorServer), Toast.LENGTH_LONG).show();
            }
        });


    }

    private void showAlert(String tag, int groupIndex, int childIndex) {
        builder = new AlertDialog.Builder(_context, R.style.AlertDialogeTheme);
        builder.setMessage(_context.getString(R.string.deleteEvent));
        builder.setPositiveButton(_context.getString(R.string.delete), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                if (tag.equalsIgnoreCase("delete"))
                    deletEvent(itemId, groupIndex, childIndex);
                else if (tag.equalsIgnoreCase("calendar"))
                    removeFromCalendar(itemId, groupIndex, childIndex);
                else
                    unLikeEvent(itemId, groupIndex, childIndex);
            }
        });

        builder.setNegativeButton(_context.getString(R.string.cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                alertDialog.cancel();
            }
        });
        alertDialog = builder.create();

        alertDialog.show();
    }

}
