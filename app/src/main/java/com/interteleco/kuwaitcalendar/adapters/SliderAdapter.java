package com.interteleco.kuwaitcalendar.adapters;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.interteleco.kuwaitcalendar.R;
import com.interteleco.kuwaitcalendar.activities.Home;
import com.interteleco.kuwaitcalendar.fragments.EventDetails;

import com.interteleco.kuwaitcalendar.server.EventApi;
import com.interteleco.kuwaitcalendar.server.ServiceClient;
import com.interteleco.kuwaitcalendar.server.models.APIError;
import com.interteleco.kuwaitcalendar.server.models.EventData;

import com.interteleco.kuwaitcalendar.server.models.RESPONSE;
import com.interteleco.kuwaitcalendar.utils.Constants;
import com.interteleco.kuwaitcalendar.utils.ErrorUtils;
import com.interteleco.kuwaitcalendar.utils.Fonts;
import com.interteleco.kuwaitcalendar.utils.Global;
import com.interteleco.kuwaitcalendar.utils.ResponseUtil;

import androidx.core.content.ContextCompat;
import androidx.viewpager.widget.PagerAdapter;

import java.util.ArrayList;
import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SliderAdapter extends PagerAdapter {
    private Context context;
    LayoutInflater inflater;
    public ArrayList<EventData> data = new ArrayList<>();


    public SliderAdapter(Context context, ArrayList<EventData> data) {
        this.context = context;
        this.data = data;

    }


    @Override
    public int getCount() {

        return data.size();

    }

    @Override
    public boolean isViewFromObject(View view, Object object) {

        return (view == (RelativeLayout) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, final int position) {
        String authorized = Global.getInstance(context).getFromSharedPreferneces(Constants.Authorized);
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View view = inflater.inflate(R.layout.slider, container, false);

        ImageView img = (ImageView) view.findViewById(R.id.imageView_id);
        TextView title = (TextView) view.findViewById(R.id.description);
        ImageView share = (ImageView) view.findViewById(R.id.imgshare_id);
        ImageView like = (ImageView) view.findViewById(R.id.imgFav_id);
        ImageView img1 = (ImageView) view.findViewById(R.id.slide);
//        TextView place = (TextView) view.findViewById(R.id.location);
        ((Home) context).customizeToolbar(img1, context.getResources().getDrawable(R.drawable.nexticon), context.getResources().getDrawable(R.drawable.next));
        String name = data.get(position).getTitle_en();
        String url = data.get(position).getImageURL();
        String shareUrl = Constants.SharedURL + "events/" +data.get(position).get_id();
        if (Global.getInstance(context).getFromSharedPreferneces("mylang").equalsIgnoreCase("en")) {
          //  place.setText(data.get(position).getVenueId().getName_en());
            title.setText(data.get(position).getTitle_en());

        } else {
         //   place.setText(data.get(position).getVenueId().getName_ar());
            title.setText(data.get(position).getTitle_ar());
        }
        //Glide.with(context).load("http://41.41.86.210:3008/events/def.jpg").into(img);

        if (data.get(position).isLikes() == true) {
            like.setImageResource(R.drawable.fav_active);
            like.setTag("unlike");
        } else {
            like.setImageResource(R.drawable.fav_2);
            like.setTag("like");
        }

        Glide.with(context).load(Constants.ImgURL + data.get(position).getImageURL()).into(img);


        img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                EventDetails eventDetails = new EventDetails();
                eventDetails.id = data.get(position).get_id();
                ((Home) context).addFragament(eventDetails);
            }
        });
        like.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (authorized != null && !authorized.equalsIgnoreCase("")) {

                    if (like.getTag().toString().equalsIgnoreCase("like")) {
                        likeEvent(data.get(position).get_id(), "like");
                        like.setImageResource(R.drawable.fav_active);
                        like.setTag("unlike");
                    } else {
                        likeEvent(data.get(position).get_id(), "unlike");
                        like.setImageResource(R.drawable.fav_2);
                        like.setTag("like");
                    }
                } else {
                    Toast.makeText(context, context.getString(R.string.loginFirst), Toast.LENGTH_LONG).show();

                }
            }
        });


        share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                         if (ContextCompat.checkSelfPermission(context, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                             shareEvent(data.get(position).get_id());
                             shareImage(name, url,shareUrl);

                         }else{
                             ((Home)context).askpermissionsToShare();
                         }
           //     } else {

                  //  Toast.makeText(context, context.getString(R.string.loginFirst), Toast.LENGTH_LONG).show();

             //   }


            }
        });


      //  Fonts.set(new TextView[]{place}, context, 0);
        Fonts.set(new TextView[]{title}, context, 1);


        container.addView(view);

        return view;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {

        container.removeView((RelativeLayout) object);

    }

    public void likeEvent(String ID, String tag) {

        String token = Global.getInstance(context).getFromSharedPreferneces(Constants.SH_Token);

        HashMap<String, Object> body = new HashMap<>();

        body.put("_id", ID);

        EventApi retrofitAPI = ServiceClient.createService(EventApi.class, context);
        retrofitAPI.likeEvent(token, body).enqueue(new Callback<RESPONSE>() {
            @Override
            public void onResponse(Call<RESPONSE> call, Response<RESPONSE> response) {
                if (response.code() == 200) {
                    //    hideLoadingDialog(pDialog);
                    RESPONSE result = response.body();

                } else {
                    APIError error = ErrorUtils.parseError(response);
                    Toast.makeText(context, error.getMessage(), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<RESPONSE> call, Throwable throwable) {
                // hideLoadingDialog(pDialog);
                Toast.makeText(context, context.getString(R.string.errorServer), Toast.LENGTH_LONG).show();
            }
        });


    }

    public void shareEvent(String ID) {

        String token = Global.getInstance(context).getFromSharedPreferneces(Constants.SH_Token);

        HashMap<String, Object> body = new HashMap<>();

        body.put("_id", ID);

        // body.put("lang", currentLanguage.toLowerCase());

        // pDialog = showLoadingDialog(getActivity());

        EventApi retrofitAPI = ServiceClient.createService(EventApi.class, context);
        retrofitAPI.shareEvent(token, body).enqueue(new Callback<RESPONSE>() {
            @Override
            public void onResponse(Call<RESPONSE> call, Response<RESPONSE> response) {
                if (response.code() == 200) {
                    //    hideLoadingDialog(pDialog);
                    RESPONSE result = response.body();
                    //   String message = response.body().getMessage();
                    //   System.out.print(message);
                    //   data.remove(selectedindex);
                    //  notifyDataSetChanged();


                    //    Toast.makeText(context, "" + result.getSuccess(), Toast.LENGTH_LONG).show();
                    //  getChildFragmentManager().popBackStack();
                } else {
                    //      hideLoadingDialog(pDialog);
                    RESPONSE result = ResponseUtil.parseError(response);
                    Toast.makeText(context, result.getMessage(), Toast.LENGTH_LONG).show();

                }
            }

            @Override
            public void onFailure(Call<RESPONSE> call, Throwable throwable) {
                // hideLoadingDialog(pDialog);
                Toast.makeText(context, context.getString(R.string.errorServer), Toast.LENGTH_LONG).show();
            }
        });


    }


    public void shareImage(String name, String url,String shareUrl) {


        try {


            Glide.with(context)
                    .load(Constants.ImgURL + url)
                    .asBitmap().skipMemoryCache(true).diskCacheStrategy(DiskCacheStrategy.NONE)

                    .into(new SimpleTarget<Bitmap>(250, 250) {
                        @Override
                        public void onResourceReady(Bitmap resource, GlideAnimation glideAnimation) {

                            Intent intent = new Intent(Intent.ACTION_SEND);
                            intent.putExtra(Intent.EXTRA_TEXT, name + " "+"\n"+ shareUrl+" "+"\n"+ context.getString(R.string.downloadGoogle)+"\n"+" https://play.google.com/store/apps/details?id=com.interteleco.kuwaitcalendar "
                                    +"\n"+ context.getString(R.string.downloadApple)+"\n"+" https://apps.apple.com/us/app/kuwait-calendar/id1479037618?ls=1  ");
                            String path = MediaStore.Images.Media.insertImage(context.getContentResolver(), resource, "", null);
                            //  Log.i("quoteswahttodo", "is onresoursereddy" + path);

                            Uri screenshotUri = Uri.parse(path);

                            // Log.i("quoteswahttodo", "is onresoursereddy" + screenshotUri);

                            intent.putExtra(Intent.EXTRA_STREAM, screenshotUri);
                            intent.setType("image/*");

                            context.startActivity(Intent.createChooser(intent, "Share image via..."));
                        }

                        @Override
                        public void onLoadFailed(Exception e, Drawable errorDrawable) {
                            // Toast.makeText(getContext(), "Something went wrong", Toast.LENGTH_SHORT).show();

                            super.onLoadFailed(e, errorDrawable);
                        }

                        @Override
                        public void onLoadStarted(Drawable placeholder) {
                            //  Toast.makeText(getContext(), "Starting", Toast.LENGTH_SHORT).show();

                            super.onLoadStarted(placeholder);
                        }
                    });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}
