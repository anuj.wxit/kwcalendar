package com.interteleco.kuwaitcalendar.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.interteleco.kuwaitcalendar.R;

import com.interteleco.kuwaitcalendar.server.models.Target;

import java.util.ArrayList;

public class TargetAdapter  extends ArrayAdapter {

    ArrayList<Target> data;
    Context context;
    public  TargetAdapter(Context context, ArrayList<Target>data) {
        super(context, R.layout.list_item, data);
        this.context=context;
        this.data=data;
    }

    @Override
    public int getCount() {

        return data.size();
    }
    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView = inflater.inflate(R.layout.list_item, parent, false);
        TextView txtName = (TextView) convertView.findViewById(R.id.txtItemName);
        Target target = data.get(position);
        txtName.setText(data.get(position).getTitle());
        return convertView;

    }

}
