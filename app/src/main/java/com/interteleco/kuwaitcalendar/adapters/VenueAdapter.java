package com.interteleco.kuwaitcalendar.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.interteleco.kuwaitcalendar.R;

import com.interteleco.kuwaitcalendar.server.models.Venue;
import com.interteleco.kuwaitcalendar.utils.Global;

import java.util.ArrayList;

public class VenueAdapter extends ArrayAdapter {
    ArrayList<Venue> data;
    Context context;

    public VenueAdapter(Context context, ArrayList<Venue> data) {
        super(context, R.layout.list_item, data);
        this.context = context;
        this.data = data;
    }

    @Override
    public int getCount() {

        return data.size();
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView = inflater.inflate(R.layout.list_item, parent, false);
        TextView txtName = (TextView) convertView.findViewById(R.id.txtItemName);
        Venue venue = data.get(position);
        if (Global.getInstance(context).getFromSharedPreferneces("mylang").equalsIgnoreCase("en"))
            txtName.setText(data.get(position).getName_en());
        else
            txtName.setText(data.get(position).getName_ar());
        return convertView;

    }
}
