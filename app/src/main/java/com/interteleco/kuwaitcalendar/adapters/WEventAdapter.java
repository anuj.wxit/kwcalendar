package com.interteleco.kuwaitcalendar.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.interteleco.kuwaitcalendar.R;
import com.interteleco.kuwaitcalendar.activities.Home;
import com.interteleco.kuwaitcalendar.fragments.EventDetails;

import com.interteleco.kuwaitcalendar.server.models.EventData;
import com.interteleco.kuwaitcalendar.utils.Constants;
import com.interteleco.kuwaitcalendar.utils.Fonts;
import com.interteleco.kuwaitcalendar.utils.Global;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;



public class WEventAdapter extends RecyclerView.Adapter<WEventAdapter.EventHolder> {

    ArrayList<EventData> data;
    Context context;
    int week = 0;
    int recom = 1;

    public WEventAdapter(Context context, ArrayList<EventData> data) {
        this.context = context;
        this.data = data;
    }


    @NonNull
    @Override
    public WEventAdapter.EventHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.week_event_item, parent, false);
        return new WEventAdapter.EventHolder(v);

    }

    @Override
    public void onBindViewHolder(@NonNull WEventAdapter.EventHolder holder, int position) {

        if (Global.getInstance(context).getFromSharedPreferneces("mylang").equalsIgnoreCase("en")) {
            holder.txtDescription.setText(data.get(position).getTitle_en());
            Glide.with(context).load(Constants.ImgURL + data.get(position).getImageURL()).into(holder.imageEvent);
        } else if (Global.getInstance(context).getFromSharedPreferneces("mylang").equalsIgnoreCase("ar")) {
            holder.txtDescription.setText(data.get(position).getTitle_ar());
            //img.setImageResource(images[position]);
            Glide.with(context).load(Constants.ImgURL + data.get(position).getImageURL()).into(holder.imageEvent);
            Glide.with(context).load("http://41.41.86.210:3008/events/def.jpg").into(holder.imageEvent);
        }  // Glide.with(context).load(Constants.ImgURL + data.get(position).getImageURL()).into(holder.imageEvent);

        //    Glide.with(context).load("http://41.41.86.210:3008/events/def.jpg").into(holder.imageEvent);
        Fonts.set(new TextView[]{holder.txtDescription}, context, 1);

    }

    @Override
    public int getItemCount() {
        return data.size();
    }


    public class EventHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.description)
        TextView txtDescription;
        @BindView(R.id.imageEvent)
        ImageView imageEvent;
        EventData eventData;

        public EventHolder(@NonNull View itemView) {
            super(itemView);

            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    eventData = data.get(getAdapterPosition());
                    EventDetails eventDetails = new EventDetails();
                    eventDetails.id = eventData.get_id();
                    ((Home) context).addFragament(eventDetails);
                }
            });
        }

    }

}
