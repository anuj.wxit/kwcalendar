package com.interteleco.kuwaitcalendar.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.DialogFragment;

import com.interteleco.kuwaitcalendar.R;
import com.interteleco.kuwaitcalendar.adapters.DropListAdapter;
import com.interteleco.kuwaitcalendar.adapters.TargetAdapter;
import com.interteleco.kuwaitcalendar.adapters.VenueAdapter;
import com.interteleco.kuwaitcalendar.listeners.ListListener;

import com.interteleco.kuwaitcalendar.server.models.CategoryData;
import com.interteleco.kuwaitcalendar.server.models.Target;
import com.interteleco.kuwaitcalendar.server.models.Venue;

import java.util.ArrayList;

public class CategoryList extends DialogFragment {

    public ListListener listener;
    AppCompatActivity activity;
    public DropListAdapter listAdapter;

    public String Tag = "";
    public ArrayList<CategoryData.Category> category;
    public ArrayList<Venue> venues;
    public ArrayList<Target> targets;
    public ArrayList<Target> acountTypes;

    public Dialog onCreateDialog(Bundle savedInstanceState) {
        LayoutInflater inflater = LayoutInflater.from(getActivity());
        targets = new ArrayList<>();
        acountTypes = new ArrayList<>();

        Target target1 = new Target(1, getString(R.string.any));
        Target target2 = new Target(2, getString(R.string.kids));
        Target target3 = new Target(3, getString(R.string.teenagers));
        Target target4 = new Target(4, getString(R.string.adults));
        targets.add(target1);
        targets.add(target2);
        targets.add(target3);
        targets.add(target4);
        Target type1 = new Target(1, getString(R.string.individuals));
        Target type2 = new Target(2, getString(R.string.privatesector));
        Target type3 = new Target(3, getString(R.string.government));
        acountTypes.add(type1);
        acountTypes.add(type2);
        acountTypes.add(type3);
        View view = inflater.inflate(R.layout.dropdownlist, null, false);

        ListView lstData = (ListView) view.findViewById(R.id.lstData);
        if (Tag.equalsIgnoreCase("cat")) {
            DropListAdapter listAdapter = new DropListAdapter(getContext(), category);
            lstData.setAdapter(listAdapter);
        } else if (Tag.equalsIgnoreCase("venue")) {
            VenueAdapter venue = new VenueAdapter(getContext(), venues);
            lstData.setAdapter(venue);
        } else if (Tag.equalsIgnoreCase("account")) {
            TargetAdapter targetAdapter = new TargetAdapter(getContext(), acountTypes);
            lstData.setAdapter(targetAdapter);
        } else {

            TargetAdapter targetAdapter = new TargetAdapter(getContext(), targets);
            lstData.setAdapter(targetAdapter);

        }

        lstData.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                if (Tag.equalsIgnoreCase("cat")) {
                    listener.selectCategory(category.get(position));
                } else if (Tag.equalsIgnoreCase("venue")) {
                    listener.selectVenue(venues.get(position));
                } else if (Tag.equalsIgnoreCase("account")) {
                    listener.selectTarget(acountTypes.get(position));
                } else {

                    listener.selectTarget(targets.get(position));

                }
                getDialog().cancel();
            }
        });
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        AlertDialog objAlertDialog = builder.create();
        objAlertDialog.setView(view, 0, 0, 0, 0);
        return objAlertDialog;
    }


}
