package com.interteleco.kuwaitcalendar.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.text.InputType;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.fragment.app.DialogFragment;

import com.interteleco.kuwaitcalendar.R;
import com.interteleco.kuwaitcalendar.activities.MainActivity;
import com.interteleco.kuwaitcalendar.activities.Splash;
import com.interteleco.kuwaitcalendar.adapters.DropListAdapter;
import com.interteleco.kuwaitcalendar.adapters.TargetAdapter;
import com.interteleco.kuwaitcalendar.adapters.VenueAdapter;
import com.interteleco.kuwaitcalendar.fragments.ForgetPasswordConfirm;
import com.interteleco.kuwaitcalendar.listeners.ListListener;
import com.interteleco.kuwaitcalendar.server.ServiceClient;
import com.interteleco.kuwaitcalendar.server.UserApi;
import com.interteleco.kuwaitcalendar.server.models.APIError;
import com.interteleco.kuwaitcalendar.server.models.CategoryData;
import com.interteleco.kuwaitcalendar.server.models.RESPONSE;
import com.interteleco.kuwaitcalendar.server.models.Target;
import com.interteleco.kuwaitcalendar.server.models.Venue;
import com.interteleco.kuwaitcalendar.utils.Constants;
import com.interteleco.kuwaitcalendar.utils.ErrorUtils;
import com.interteleco.kuwaitcalendar.utils.Fonts;
import com.interteleco.kuwaitcalendar.utils.Global;
import com.interteleco.kuwaitcalendar.utils.Validator;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ChangeField extends DialogFragment {
    AppCompatEditText email;
    TextView errorMessage;
    AppCompatButton sendButton;
    ProgressBar progressBar;
    public String Tag = "";

    public String userToken = "";
    View view;

    public Dialog onCreateDialog(Bundle savedInstanceState) {
        LayoutInflater inflater = LayoutInflater.from(getActivity());
        view = inflater.inflate(R.layout.change_field, null, false);
        progressBar = (ProgressBar) view.findViewById(R.id.progressBar);

        email = (AppCompatEditText) view.findViewById(R.id.edtEmail);
        if (Tag.equalsIgnoreCase("mobile")) {
            email.setHint(getString(R.string.enteryourPhone));
            email.setInputType(InputType.TYPE_CLASS_NUMBER);
            email.setFilters(new InputFilter[]{new InputFilter.LengthFilter(8)});

        } else {
            email.setHint(getString(R.string.enteryourEmail));
        }
        errorMessage = (TextView) view.findViewById(R.id.txtErrorEmail);

        sendButton = (AppCompatButton) view.findViewById(R.id.send);

        setFonts();

        sendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity) getActivity()).hideKeyBoard();
                //    Toast.makeText(getActivity(), "Button", Toast.LENGTH_SHORT).show();
                if (Tag.equalsIgnoreCase("mobile"))
                    editMobile();
                else
                    editEmail();
            }
        });


        email.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                errorMessage.setVisibility(View.GONE);
                //   email.setBackground(getResources().getDrawable(R.drawable.normal_bg));
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });


        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        AlertDialog objAlertDialog = builder.create();
        objAlertDialog.setView(view, 0, 0, 0, 0);
        return objAlertDialog;
    }

    public void editEmail() {
        if (email.getText().length() > 0 && !email.getText().toString().trim().equalsIgnoreCase("")) {
            if (Validator.isValidEMail(email.getText().toString())) {
                ((MainActivity) getActivity()).hideKeyBoard();

                changeEmail();

            } else {
              //  email.requestFocus();
                errorMessage.setVisibility(View.VISIBLE);
                errorMessage.setText(getString(R.string.entervalidEmail));

            }
        } else {
          //  email.requestFocus();
            errorMessage.setVisibility(View.VISIBLE);
            errorMessage.setText(getString(R.string.enterEmail));
        }


    }

    public void editMobile() {
        if (email.getText().length() > 0 && !email.getText().toString().trim().equalsIgnoreCase("")) {
            if (Validator.isMobile(email.getText().toString())) {
                if (email.length()==8) {
                    ((MainActivity) getActivity()).hideKeyBoard();

                    changeMobile();
                }else {
                   // email.requestFocus();
                    errorMessage.setVisibility(View.VISIBLE);
                    errorMessage.setText(getString(R.string.mobilecharacters));}
            } else {
            //    email.requestFocus();
                errorMessage.setVisibility(View.VISIBLE);
                errorMessage.setText(getString(R.string.mobileStart));

            }
        } else {
           // email.requestFocus();
            errorMessage.setText(getString(R.string.entermobileNumber));
            errorMessage.setVisibility(View.VISIBLE);

        }


    }

    private void setFonts() {
        Fonts.set(new EditText[]{email}, getActivity(), 0);
        Fonts.set(new TextView[]{errorMessage}, getActivity(), 0);

        Fonts.set(new AppCompatButton[]{sendButton}, getActivity(), 0);
    }

    private void changeEmail() {
        HashMap<String, Object> body = new HashMap<>();
//        if (Tag.equalsIgnoreCase("home"))
//            userToken = Global.getInstance(getContext()).getFromSharedPreferneces(Constants.SH_Token);
        body.put("newEmail", email.getText().toString());

        String token = Global.getInstance(getContext()).getFromSharedPreferneces(Constants.SH_Token);
        progressBar.setVisibility(View.VISIBLE);
        UserApi retrofitAPI = ServiceClient.createService(UserApi.class, getActivity());
        retrofitAPI.changeEmail(userToken, body).enqueue(new Callback<RESPONSE>() {
            @Override
            public void onResponse(Call<RESPONSE> call, Response<RESPONSE> response) {

                if (response.code() == 200) {
                    progressBar.setVisibility(View.GONE);
                    RESPONSE result = response.body();
                    Intent intent = new Intent(Global.getInstance(getContext()).BROADCAST_ACTION);
                    intent.putExtra("action", Constants.ChangeEmail);
                    intent.putExtra("email", email.getText().toString());
                    getContext().sendBroadcast(intent);
                   // Toast.makeText(getActivity(), getString(R.string.changed), Toast.LENGTH_LONG).show();
                    getDialog().cancel();
                } else {
                    progressBar.setVisibility(View.GONE);
                    APIError error = ErrorUtils.parseError(response);
                    Toast.makeText(getActivity(), error.getMessage(), Toast.LENGTH_LONG).show();

                }


            }

            @Override
            public void onFailure(Call<RESPONSE> call, Throwable throwable) {
                progressBar.setVisibility(View.GONE);
                Toast.makeText(getActivity(), getString(R.string.errorServer), Toast.LENGTH_LONG).show();
            }
        });

    }


    private void changeMobile() {
        HashMap<String, Object> body = new HashMap<>();
//        if (Tag.equalsIgnoreCase("home"))
//            userToken = Global.getInstance(getContext()).getFromSharedPreferneces(Constants.SH_Token);
        body.put("mobileNumber", email.getText().toString());

        String token = Global.getInstance(getContext()).getFromSharedPreferneces(Constants.SH_Token);
        progressBar.setVisibility(View.VISIBLE);
        UserApi retrofitAPI = ServiceClient.createService(UserApi.class, getActivity());
        retrofitAPI.changePhone(userToken, body).enqueue(new Callback<RESPONSE>() {
            @Override
            public void onResponse(Call<RESPONSE> call, Response<RESPONSE> response) {

                if (response.code() == 200) {
                    progressBar.setVisibility(View.GONE);
                    RESPONSE result = response.body();
                    Intent intent = new Intent(Global.getInstance(getContext()).BROADCAST_ACTION);
                    intent.putExtra("action", Constants.ChangePhone);
                    intent.putExtra("mobile", email.getText().toString());
                    getContext().sendBroadcast(intent);
                 //   Toast.makeText(getActivity(), getString(R.string.changed), Toast.LENGTH_LONG).show();
                    getDialog().cancel();
                } else {
                    progressBar.setVisibility(View.GONE);
                    APIError error = ErrorUtils.parseError(response);
                    Toast.makeText(getActivity(), error.getMessage(), Toast.LENGTH_LONG).show();
                }


            }

            @Override
            public void onFailure(Call<RESPONSE> call, Throwable throwable) {
                progressBar.setVisibility(View.GONE);
                Toast.makeText(getActivity(), getString(R.string.errorServer), Toast.LENGTH_LONG).show();
            }
        });

    }

}

