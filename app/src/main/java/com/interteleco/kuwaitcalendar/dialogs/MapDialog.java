package com.interteleco.kuwaitcalendar.dialogs;

import android.Manifest;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.DialogFragment;

import com.interteleco.kuwaitcalendar.R;
import com.interteleco.kuwaitcalendar.activities.ViewDirections;


public class MapDialog extends DialogFragment {

    public String id, title;
    LocationManager locationManager;
    public Double lat, longt;
    LocationListener locationListener;

    public Dialog onCreateDialog(Bundle savedInstanceState) {

        setStyle(androidx.fragment.app.DialogFragment.STYLE_NORMAL, R.style.Theme_AppCompat_Dialog_Alert);
        locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);

        locationListener = new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {

                //   Toast.makeText(getActivity(), location.toString(), Toast.LENGTH_LONG).show();
            }

            @Override
            public void onStatusChanged(String s, int i, Bundle bundle) {

            }

            @Override
            public void onProviderEnabled(String s) {

            }

            @Override
            public void onProviderDisabled(String s) {

            }
        };


        LayoutInflater inflater = LayoutInflater.from(getActivity());

        View view = inflater.inflate(R.layout.map_dialog, null, false);
        TextView show = (TextView) view.findViewById(R.id.show);
        TextView details = (TextView) view.findViewById(R.id.details);
        TextView eventTitle = (TextView) view.findViewById(R.id.title);
        eventTitle.setText(title);

        details.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent objIntent = new Intent(getActivity(), ViewDirections.class);
                objIntent.putExtra("id", id);
                startActivity(objIntent);
            }
        });
        show.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                askDirectionPermission();
            }
        });
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        AlertDialog objAlertDialog = builder.create();
        objAlertDialog.setView(view, 0, 0, 0, 0);
        return objAlertDialog;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

            if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);

                locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locationListener);

                Location lastKnownLocation = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                if (lastKnownLocation != null) {
                    //  Log.e("TAG", "GPS is on");
                    Double latitude = lastKnownLocation.getLatitude();
                    Double longitude = lastKnownLocation.getLongitude();
                    //  centerMapOnLocation(lastKnownLocation, "Your location");
                    viewDirection(latitude, longitude, lat, longt);
                }


            }

        }
    }


    private void viewDirection(Double userat, Double userlong, Double eventlat, Double eventlongt) {
        Intent navigation = new Intent(Intent.ACTION_VIEW,
                Uri.parse("http://maps.google.com/maps?f=d&source=s_d" + "&saddr=" + userat + "," + userlong + "&daddr=" + eventlat + "," + eventlongt));
        navigation.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK & Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
        navigation.setClassName("com.google.android.apps.maps", "com.google.android.maps.MapsActivity");
        startActivity(navigation);
    }

    private void askDirectionPermission() {

        if (Build.VERSION.SDK_INT < 23) {

            if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    Activity#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for Activity#requestPermissions for more details.
                ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 2);

            } else {
                locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locationListener);
                android.location.Location lastKnownLocation = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                //   Toast.makeText(getActivity(), lastKnownLocation.toString(), Toast.LENGTH_LONG).show();
                if (lastKnownLocation != null) {
                    //  Log.e("TAG", "GPS is on");
                    Double latitude = lastKnownLocation.getLatitude();
                    Double longitude = lastKnownLocation.getLongitude();
                    // Toast.makeText(getActivity(), +latitude + longitude + "", Toast.LENGTH_LONG).show();
                    viewDirection(latitude, longitude, lat, longt);
                } else {
                    Toast.makeText(getContext(), "error in location", Toast.LENGTH_LONG).show();
                    locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, locationListener);
                    lastKnownLocation = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                    //   Toast.makeText(getActivity(), lastKnownLocation.toString(), Toast.LENGTH_LONG).show();
                    if (lastKnownLocation != null) {
                        //  Log.e("TAG", "GPS is on");
                        Double latitude = lastKnownLocation.getLatitude();
                        Double longitude = lastKnownLocation.getLongitude();
                        // Toast.makeText(getActivity(), +latitude + longitude + "", Toast.LENGTH_LONG).show();
                        viewDirection(latitude, longitude, lat, longt);
                    }
                }
            }

        } else {

            if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {

                locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locationListener);

                Location lastKnownLocation = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);

                if (lastKnownLocation != null) {
                    //  Log.e("TAG", "GPS is on");
                    Double latitude = lastKnownLocation.getLatitude();
                    Double longitude = lastKnownLocation.getLongitude();
                    //  centerMapOnLocation(lastKnownLocation, "Your location");
                    viewDirection(latitude, longitude, lat, longt);
                }


            } else {

                ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);

            }


        }


    }


}
