package com.interteleco.kuwaitcalendar.dialogs;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.appcompat.widget.AppCompatButton;
import androidx.fragment.app.DialogFragment;

import com.interteleco.kuwaitcalendar.R;

import com.interteleco.kuwaitcalendar.utils.Fonts;
import com.interteleco.kuwaitcalendar.utils.NetworkStatusReciever;

public class NoConnection extends DialogFragment {
    public static String TAG = "";

    AppCompatButton tryagain;
    TextView txtNoInternet;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NORMAL, R.style.FullScreenDialogStyle);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.iconnection, container, false);
        setCancelable(false);
        tryagain = (AppCompatButton) view.findViewById(R.id.tryBtn);
        txtNoInternet = (TextView) view.findViewById(R.id.txtNOConnection);
        Fonts.set(new TextView[]{txtNoInternet}, getContext(), 0);
        tryagain.setOnClickListener(new View.OnClickListener() {


            @Override
            public void onClick(View v) {
                if (NetworkStatusReciever.isConnected(getContext()) == true) {

                    getDialog().cancel();
                    getActivity().finishAffinity();
                    Intent i = getActivity().getPackageManager()
                            .getLaunchIntentForPackage(getActivity().getPackageName());
                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    getActivity().startActivity(i);
                }


            }
        });
        return view;
    }


    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null) {
            int width = ViewGroup.LayoutParams.MATCH_PARENT;
            int height = ViewGroup.LayoutParams.MATCH_PARENT;
            dialog.getWindow().setLayout(width, height);
        }
    }


}
