package com.interteleco.kuwaitcalendar.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.appcompat.widget.AppCompatButton;
import androidx.fragment.app.DialogFragment;

import com.interteleco.kuwaitcalendar.R;
import com.interteleco.kuwaitcalendar.activities.ViewDirections;
import com.interteleco.kuwaitcalendar.utils.Fonts;
import com.interteleco.kuwaitcalendar.utils.NetworkStatusReciever;

public class TermsDialog extends DialogFragment {


    public static String TAG = "";

    AppCompatButton tryagain;
    TextView txtNoInternet;

    public Dialog onCreateDialog(Bundle savedInstanceState) {
        setStyle(androidx.fragment.app.DialogFragment.STYLE_NORMAL, R.style.Theme_AppCompat_Dialog_Alert);

        LayoutInflater inflater = LayoutInflater.from(getActivity());

        View view = inflater.inflate(R.layout.terms, null, false);
        TextView title = (TextView) view.findViewById(R.id.title);
        TextView condition1 = (TextView) view.findViewById(R.id.condition1);
        TextView condition2 = (TextView) view.findViewById(R.id.condition2);
        Fonts.set(new TextView[]{title},getContext(),1);
        Fonts.set(new TextView[]{condition1,condition2},getContext(),0);
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        AlertDialog objAlertDialog = builder.create();
        objAlertDialog.setView(view, 0, 0, 0, 0);
        return objAlertDialog;
    }


}
