package com.interteleco.kuwaitcalendar.fragments;

import android.Manifest;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.loader.content.CursorLoader;

import com.interteleco.kuwaitcalendar.R;
import com.interteleco.kuwaitcalendar.activities.Home;
import com.interteleco.kuwaitcalendar.activities.Location;
import com.interteleco.kuwaitcalendar.listeners.ListListener;

import com.interteleco.kuwaitcalendar.dialogs.CategoryList;
import com.interteleco.kuwaitcalendar.server.EventApi;
import com.interteleco.kuwaitcalendar.server.ServiceClient;
import com.interteleco.kuwaitcalendar.server.models.APIError;
import com.interteleco.kuwaitcalendar.server.models.CategoryData;
import com.interteleco.kuwaitcalendar.server.models.RESPONSE;
import com.interteleco.kuwaitcalendar.server.models.Target;
import com.interteleco.kuwaitcalendar.server.models.Venue;
import com.interteleco.kuwaitcalendar.server.models.VenueResponse;
import com.interteleco.kuwaitcalendar.utils.Constants;
import com.interteleco.kuwaitcalendar.utils.ErrorUtils;
import com.interteleco.kuwaitcalendar.utils.Fonts;
import com.interteleco.kuwaitcalendar.utils.Global;
import com.interteleco.kuwaitcalendar.utils.Validator;

import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.app.Activity.RESULT_OK;

public class AddEvent extends Fragment implements ListListener {

    @BindView(R.id.add_btn)
    AppCompatButton addEvent;

    @BindView(R.id.txtErrorName)
    TextView txtErrorName;

    @BindView(R.id.txtErrorName_ar)
    TextView txtErrorName_ar;


    @BindView(R.id.txtErrordateTo)
    TextView txtErrordateTo;


    @BindView(R.id.txtErrordateFrom)
    TextView txtErrordateFrom;
    @BindView(R.id.txtErrorTimefrom)
    TextView txtErrorTimefrom;


    @BindView(R.id.txtErrorTimeto)
    TextView txtErrorTimeto;
    @BindView(R.id.txtErrorTarget)
    TextView txtErrorTarget;
    @BindView(R.id.txtErrorType)
    TextView txtErrorType;

    @BindView(R.id.txtErrorLocation)
    TextView txtErrorLocation;
    @BindView(R.id.txtErrorVenue)
    TextView txtErrorVenue;
    @BindView(R.id.txtErrorDesc)
    TextView txtErrorDesc;

    @BindView(R.id.txtErrorDesc_ar)
    TextView txtErrorDesc_ar;
    @BindView(R.id.txtErrorImage)
    TextView txtErrorImage;

    @BindView(R.id.txtErrorCheck)
    TextView txtErrorCheck;
//    @BindView(R.id.txtErrorCheck1)
//    TextView txtErrorCheck1;

    @BindView(R.id.edtName)
    EditText edtName;

    @BindView(R.id.edtName_ar)
    EditText edtName_ar;

//    @BindView(R.id.edtOwner)
//    AppCompatEditText edtOwner;

    @BindView(R.id.description)
    AppCompatEditText description;

    @BindView(R.id.description_ar)
    AppCompatEditText description_ar;

    @BindView(R.id.dateFrom)
    AppCompatButton dateFrom;

    @BindView(R.id.dateTo)
    AppCompatButton dateTo;

    @BindView(R.id.timeFrom)
    AppCompatButton timeFrom;

    @BindView(R.id.timeTo)
    AppCompatButton timeTo;

    @BindView(R.id.location)
    AppCompatButton locationBtn;

    @BindView(R.id.target)
    AppCompatButton targetBtn;

    @BindView(R.id.venue)
    AppCompatButton venueBtn;

    @BindView(R.id.type)
    AppCompatButton typeBtn;

    @BindView(R.id.event_img)
    ImageView event_img;

    @BindView(R.id.upload)
    ImageView uploadPoster;

    @BindView(R.id.menuBtn)
    ImageView backBtn;

    @BindView(R.id.toolbar_title)
    TextView toolbar_title;

    @BindView(R.id.check)
    CheckBox check;
//    @BindView(R.id.check1)
//    CheckBox check1;
    Global objGlobal;

    int Pick_Image = 1;
    int Pick_location = 2;
    View view;
    DatePickerDialog pickerDialog;

    Calendar calendar;
    TimePickerDialog timepickerDialog;
    String pickerResult;
    String strdatefrom, strdateto, strtimefrom, strtimeto, locationLatlng;
    int targetGroup = -1;
    String eventType = "";
    String txtVenue = "";
    Double lat;
    Double longt;
    ArrayList<CategoryData.Category> categories;
    ArrayList<Venue> venues;
    Uri imguri,newUri;

    String imagePath, mediaPath, postPath;
    ProgressBar progressBar;
    int yearfrom, monthFrom, dayfrom;
    int hight = 0, width = 0;
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.add_event, container, false);
        ButterKnife.bind(this, view);
        progressBar = (ProgressBar) view.findViewById(R.id.progressBar);

        objGlobal = Global.getInstance(getActivity());
        setFonts();
        venues = new ArrayList<>();
        categories = new ArrayList<>();
        getCategories();
        getVenues();
        ((Home) getActivity()).customizeToolbar(backBtn, getResources().getDrawable(R.drawable.arrow), getResources().getDrawable(R.drawable.arrow_r));

        removeEditTextValidation(edtName, txtErrorName);
        removeEditTextValidation(edtName_ar, txtErrorName_ar);
        removeEditTextValidation(description, txtErrorDesc);
        removeEditTextValidation(description_ar, txtErrorDesc_ar);

        removeValidation(dateFrom, txtErrordateFrom);

        removeValidation(dateTo, txtErrordateTo);

        removeValidation(locationBtn, txtErrorLocation);

        removeValidation(venueBtn, txtErrorVenue);

        removeValidation(targetBtn, txtErrorTarget);

        removeValidation(typeBtn, txtErrorType);

        check.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked)
                    txtErrorCheck.setVisibility(View.GONE);
                else
                    txtErrorCheck.setVisibility(View.VISIBLE);
            }
        });

//        check1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//            @Override
//            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//                if (isChecked)
//                    txtErrorCheck1.setVisibility(View.GONE);
//                else
//                    txtErrorCheck1.setVisibility(View.VISIBLE);
//            }
//        });


        return view;


    }

    private void removeEditTextValidation(EditText view, TextView error) {

        view.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                error.setVisibility(View.GONE);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

    }

    private void removeValidation(Button view, TextView error) {

        view.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                error.setVisibility(View.GONE);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

    }

    private void setFonts() {
        Fonts.set(new TextView[]{toolbar_title}, getActivity(), 1);
        Fonts.set(new TextView[]{txtErrorName, txtErrorName_ar, txtErrordateFrom, txtErrordateTo, txtErrorTimefrom, txtErrorTimeto
                , txtErrorImage, txtErrorLocation, txtErrorTarget, txtErrorType, txtErrorVenue,txtErrorCheck}, getActivity(), 0);
        Fonts.set(new EditText[]{edtName, edtName_ar, description, description_ar}, getActivity(), 0);
        Fonts.set(new Button[]{addEvent, dateFrom, dateTo, timeFrom, timeTo, typeBtn,locationBtn, venueBtn, targetBtn}, getActivity(), 0);
    }

    @OnClick({R.id.dateFrom, R.id.dateTo, R.id.timeFrom, R.id.timeTo, R.id.target, R.id.location, R.id.venue, R.id.upload
            , R.id.add_btn, R.id.type, R.id.menuBtn})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.dateFrom: {

                calendar = Calendar.getInstance();
                int cyear = calendar.get(Calendar.YEAR);
                int cmonth = calendar.get(Calendar.MONTH);
                int cday = calendar.get(Calendar.DAY_OF_MONTH);
                pickerDialog = new DatePickerDialog(getActivity(),
                        android.R.style.Theme_Holo_Light_Dialog, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                        month = month + 1;

                        String date;
                        if (month <10 && dayOfMonth < 10)
                            date = year + "-" + "0" + month + "-" + "0" + dayOfMonth;
                        else if (month < 10 && dayOfMonth > 10)
                            date = year + "-" + "0" + month + "-" + dayOfMonth;
                        else if (month > 10 && dayOfMonth < 10)
                            date = year + "-" + month + "-" + "0" + dayOfMonth;
                        else if (month == 10 && dayOfMonth < 10)
                            date = year + "-" + month + "-" + "0" + dayOfMonth;
                        else
                            date = year + "-" + month + "-" + dayOfMonth;

                        yearfrom = year;
                        monthFrom = month;
                        dayfrom = dayOfMonth;
                        if (year > cyear && month > cmonth + 1 || year >= cyear && month >= cmonth + 1 && dayOfMonth >= cday) {

                            dateFrom.setText(date);
                            strdatefrom = date;
                        } else {
                            Toast.makeText(getActivity(), getString(R.string.currentdate), Toast.LENGTH_LONG).show();
                        }

                    }
                }, cyear, cmonth, cday);
                pickerDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

                pickerDialog.show();
                //Toast.makeText(getActivity(), strdatefrom, Toast.LENGTH_LONG).show();
            }

            break;
            case R.id.dateTo: {

                calendar = Calendar.getInstance();
                int cyear = calendar.get(Calendar.YEAR);
                int cmonth = calendar.get(Calendar.MONTH);
                int cday = calendar.get(Calendar.DAY_OF_MONTH);
                pickerDialog = new DatePickerDialog(getActivity(),
                        android.R.style.Theme_Holo_Light_Dialog, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                        month = month + 1;
                        String date;
                        if (month < 10 && dayOfMonth < 10)
                            date = year + "-" + "0" + month + "-" + "0" + dayOfMonth;
                        else if (month < 10 && dayOfMonth > 10)
                            date = year + "-" + "0" + month + "-" + dayOfMonth;
                        else if (month > 10 && dayOfMonth < 10)
                            date = year + "-" + month + "-" + "0" + dayOfMonth;
                        else if (month == 10 && dayOfMonth < 10)
                            date = year + "-" + month + "-" + "0" + dayOfMonth;
                        else
                            date = year + "-" + month + "-" + dayOfMonth;
                        if (year > cyear && month > cmonth + 1 || year >= cyear && month >= cmonth + 1 && dayOfMonth >= cday) {

                            if (year > yearfrom && month > monthFrom || year >= yearfrom && month >= monthFrom && dayOfMonth >= dayfrom) {
                                if (yearfrom != 0) {
                                    dateTo.setText(date);
                                    strdateto = date;
                                } else {
                                    Toast.makeText(getActivity(), getString(R.string.enterDateFrom), Toast.LENGTH_LONG).show();
                                }
                            }
                        } else {
                            Toast.makeText(getActivity(), getString(R.string.currentdate), Toast.LENGTH_LONG).show();
                        }
                    }
                }, cyear, cmonth, cday);
                pickerDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

                pickerDialog.show();

            }
            break;
            case R.id.timeFrom: {
                calendar = Calendar.getInstance();
                int hour = calendar.get(Calendar.HOUR_OF_DAY);
                int minute = calendar.get(Calendar.MINUTE);

                timepickerDialog = new TimePickerDialog(getActivity(),
                        android.R.style.Theme_DeviceDefault_Dialog, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        String time;
                        if (minute < 10 && hourOfDay < 10)
                            time = "0" + hourOfDay + ":" + "0" + minute;
                        else if (minute < 10 && hourOfDay > 10)
                            time = hourOfDay + ":" + "0" + minute;
                        else if (minute > 10 && hourOfDay < 10)
                            time = "0" + hourOfDay + ":" + minute;
                        else  if (minute < 10 && hourOfDay == 10)
                            time =hourOfDay + ":" + "0" + minute;

                        else

                            time = hourOfDay + ":" + minute;
                        strtimefrom = time;
                        timeFrom.setText(time);

                    }
                }, hour, minute, true);
                //  pickerDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.green()))


                timepickerDialog.show();
            }
            break;
            case R.id.timeTo: {
                calendar = Calendar.getInstance();
                int hour = calendar.get(Calendar.HOUR_OF_DAY);
                int minute = calendar.get(Calendar.MINUTE);

                timepickerDialog = new TimePickerDialog(getActivity(),
                        android.R.style.Theme_DeviceDefault_Dialog, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        String time;
                        if (minute < 10 && hourOfDay < 10)
                            time = "0" + hourOfDay + ":" + "0" + minute;
                        else if (minute < 10 && hourOfDay > 10)
                            time = hourOfDay + ":" + "0" + minute;
                        else if (minute > 10 && hourOfDay < 10)
                            time = "0" + hourOfDay + ":" + minute;
                        else  if (minute < 10 && hourOfDay == 10)
                            time =hourOfDay + ":" + "0" + minute;

                        else

                            time = hourOfDay + ":" + minute;
                        strtimeto = time;
                        timeTo.setText(time);

                    }
                }, hour, minute, true);
                //  pickerDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.green()))


                timepickerDialog.show();
            }
            break;
            case R.id.target: {
                CategoryList list = new CategoryList();
                list.listener = this;
                list.show(getActivity().getSupportFragmentManager(), "list");
            }
            break;
            case R.id.type: {
                CategoryList list = new CategoryList();
                list.Tag = "cat";
                list.listener = this;
                list.category = categories;
                list.show(getActivity().getSupportFragmentManager(), "list");
            }
            break;
            case R.id.venue: {
                CategoryList list = new CategoryList();
                list.Tag = "venue";
                list.listener = this;
                list.venues = venues;
                list.show(getActivity().getSupportFragmentManager(), "list");
            }
            break;
            case R.id.location: {
                Intent intent = new Intent(getActivity(), Location.class);
                startActivityForResult(intent, Pick_location);
//                locationLatlng = "LAT/LNG:(29.375859,47.9774052";
//                String values = locationLatlng.substring(10, locationLatlng.length() - 1);
//                String[] latlong = values.split(",");
//                String latitude = latlong[0];
//                lat = Double.parseDouble(latitude);
//                String longtude = latlong[1];
//                longt = Double.parseDouble(longtude);
//                locationBtn.setText(locationLatlng);
//                Toast.makeText(getActivity(), lat + "/" + longt, Toast.LENGTH_LONG).show();


            }
            break;
            case R.id.upload: {
                //  openGallery();
                askpermissions();
            }
            break;
            case R.id.add_btn: {
                ((Home) getActivity()).hideKeyBoard();
                addEvent();
            }
            break;
            case R.id.menuBtn: {
                getFragmentManager().popBackStack();
            }
            break;

//            case R.id.click: {
//                Uri uri;
//                if (Global.getInstance(getActivity()).getFromSharedPreferneces("mylang").equalsIgnoreCase("en"))
//               uri = Uri.parse("http://kwcalendar.net/.well-known/terms-conditions.html");
//             else
//                 uri = Uri.parse("http://kwcalendar.net/.well-known/terms-conditions-ar.html");
//                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
//                startActivity(intent);
//            }
//            break;
        }

    }


    private void openGallery() {
        //  Intent gallery = new Intent();
        //  gallery.setType("image/*");
//        Intent gallery = new Intent(Intent.ACTION_PICK,
//                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
//        startActivityForResult(gallery, CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE);

        Intent intent = CropImage.activity()

                .getIntent(getContext());

        startActivityForResult(intent, CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE);
    }

    private String pickTime(AppCompatButton button) {

        calendar = Calendar.getInstance();
        int hour = calendar.get(Calendar.HOUR_OF_DAY);
        int minute = calendar.get(Calendar.MINUTE);

        timepickerDialog = new TimePickerDialog(getActivity(),
                android.R.style.Theme_DeviceDefault_Dialog, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                String time;
                if (minute < 10 && hourOfDay < 10)
                    time = "0" + hourOfDay + ":" + "0" + minute;
                else if (minute < 10 && hourOfDay > 10)
                    time = hourOfDay + ":" + "0" + minute;
                else if (minute > 10 && hourOfDay < 10)
                    time = "0" + hourOfDay + ":" + minute;
              else  if (minute < 10 && hourOfDay == 10)
                    time =hourOfDay + ":" + "0" + minute;

                else

                    time = hourOfDay + ":" + minute;
                button.setText(time);
                pickerResult = time;
            }
        }, hour, minute, true);
        //  pickerDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.green()))


        timepickerDialog.show();
        return pickerResult;
    }

    private void addEvent() {

        if (edtName.getText().toString() != null && !edtName.getText().toString().equalsIgnoreCase("")) {

            if (edtName_ar.getText().toString() != null && !edtName_ar.getText().toString().equalsIgnoreCase("")) {

                if (Validator.isEmptyString(strdatefrom)) {

                    if (Validator.isEmptyString(strdateto)) {

//                        if (Validator.isEmptyString(strtimefrom)) {
//
//                            if (Validator.isEmptyString(strtimeto)) {

                            if (Validator.isEmptyString(eventType)) {
                                if (targetGroup != -1) {
                                if (Validator.isEmptyString(locationLatlng)) {

                                    if (Validator.isEmptyString(description.getText().toString())) {

                                        if (Validator.isEmptyString(description_ar.getText().toString())) {
                                            if (Validator.isEmptyString(txtVenue)) {
                                                if (check.isChecked()) {

                                                        if (postPath != null)
                                                            uploadImage();
                                                        else
                                                            txtErrorImage.setVisibility(View.VISIBLE);

                                                } else {
                                                    txtErrorCheck.setVisibility(View.VISIBLE);
                                                }     //   Toast.makeText(getActivity(), "Valid data", Toast.LENGTH_LONG).show();

                                            } else {
                                                txtErrorVenue.setVisibility(View.VISIBLE);
                                                venueBtn.requestFocus();

                                            }

                                        } else {
                                            txtErrorDesc_ar.setVisibility(View.VISIBLE);
                                            description_ar.requestFocus();
                                        }
                                    } else {
                                        txtErrorDesc.setVisibility(View.VISIBLE);
                                        description.requestFocus();
                                    }


                                } else {
                                    txtErrorLocation.setVisibility(View.VISIBLE);
                                    locationBtn.requestFocus();
                                }
                            } else {
                                    txtErrorTarget.setVisibility(View.VISIBLE);
                                    targetBtn.requestFocus();


                            }


                        } else {
                                txtErrorType.setVisibility(View.VISIBLE);
                                typeBtn.requestFocus();
                        }
//                            } else {
//                                txtErrorTimeto.setVisibility(View.VISIBLE);
//
//                            }
//                        } else {
//                            txtErrorTimefrom.setVisibility(View.VISIBLE);
//                        }
                    } else {
                        txtErrordateTo.setVisibility(View.VISIBLE);
                        dateTo.requestFocus();
                    }
                } else {

                    txtErrordateFrom.setVisibility(View.VISIBLE);
                    dateFrom.requestFocus();
                }

            } else {
                txtErrorName_ar.setVisibility(View.VISIBLE);
                edtName_ar.requestFocus();

            }

        } else {

            txtErrorName.setVisibility(View.VISIBLE);
            edtName.requestFocus();
        }

    }

    @Override
    public void selectTarget(Target target) {

        targetBtn.setText(target.getTitle());
        targetGroup = target.getId();
    }

    @Override
    public void selectVenue(Venue venue) {
        if (Global.getInstance(getContext()).getFromSharedPreferneces("mylang").equalsIgnoreCase("en"))
            venueBtn.setText(venue.getName_en());
        else
            venueBtn.setText(venue.getName_ar());
        txtVenue = venue.get_id();
    }

    @Override
    public void selectCategory(CategoryData.Category category) {
        if (Global.getInstance(getContext()).getFromSharedPreferneces("mylang").equalsIgnoreCase("en"))
            typeBtn.setText(category.getName_en());
        else
            typeBtn.setText(category.getName_ar());

        eventType = category.get_id();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            if (data != null) {
                imguri = data.getData();
             ////   CropImage.activity(imguri).setGuidelines(CropImageView.Guidelines.ON).start(getContext(),this);

                    CropImage.ActivityResult result = CropImage.getActivityResult(data);
                    if (resultCode == RESULT_OK) {
                        newUri = result.getUri();
                }

                Bitmap bitmap = null;


                try {
                    bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), newUri);
               //     hight = bitmap.getHeight();
               //     width = bitmap.getWidth();


                } catch (IOException e) {
                    e.printStackTrace();
                }
                event_img.setImageBitmap(bitmap);
                String path = MediaStore.Images.Media.insertImage(getActivity().getContentResolver(),  bitmap, "", null);
            //    Toast.makeText(getContext(), path, Toast.LENGTH_LONG).show();
             newUri = Uri.parse(path);

                //    Toast.makeText(getActivity(), imguri.getPath(), Toast.LENGTH_LONG).show();

                //Toast.makeText(getActivity(), hight + "/" + width, Toast.LENGTH_LONG).show();
             //Uri selectedImage = result.getUri();
                String[] filePathColumn = {MediaStore.Images.Media.DATA};

                Cursor cursor = getActivity().getContentResolver().query(newUri, filePathColumn, null, null, null);
                assert cursor != null;
                cursor.moveToFirst();

                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                mediaPath = cursor.getString(columnIndex);
                // Set the Image in ImageView for Previewing the Media
                //  imageView.setImageBitmap(BitmapFactory.decodeFile(mediaPath));
                cursor.close();
                postPath = mediaPath;
             // Toast.makeText(getActivity(), postPath, Toast.LENGTH_LONG).show();
            }

//        if (requestCode == Crop.REQUEST_PICK && resultCode == RESULT_OK) {
//            beginCrop(data.getData());
//        } else if (requestCode == Crop.REQUEST_CROP) {
//            handleCrop(resultCode, data);

      }
        else if (requestCode == Pick_location) {

            if (data != null) {
                locationLatlng = data.getStringExtra("location");

                String values = locationLatlng.substring(10, locationLatlng.length() - 1);
                String[] latlong = values.split(",");
                String latitude = latlong[0];
                lat = Double.parseDouble(latitude);
                String longtude = latlong[1];
                longt = Double.parseDouble(longtude);
                locationBtn.setText(locationLatlng);
                //Toast.makeText(getActivity(), lat + "/" + longt, Toast.LENGTH_LONG).show();
            }
        }
    }


    private String getRealPathFromURI(Uri contentUri) {
        String[] proj = {MediaStore.Images.Media.DATA};
        CursorLoader loader = new CursorLoader(getContext(), contentUri, proj, null, null, null);
        Cursor cursor = loader.loadInBackground();
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        String result = cursor.getString(column_index);
        cursor.close();
        return result;
    }

    private void uploadImage() {


        //  Toast.makeText(getActivity(), postPath, Toast.LENGTH_LONG).show();
        File file = new File(postPath);
        long lenght = file.length() / 1024;
        //  Toast.makeText(getActivity(), +lenght + "", Toast.LENGTH_LONG).show();
        Map<String, RequestBody> map = new HashMap<>();
        //ile file = new File(postPath);

        // Parsing any Media type file
        RequestBody requestBody = RequestBody.create(MediaType.parse("*/*"), file);
        //  map.put("file\"; filename=\"" + file.getName() + "\"", requestBody);
        map.put("image", requestBody);
        // create RequestBody instance from file
        RequestBody requestFile =
                RequestBody.create(
                        MediaType.parse(getActivity().getContentResolver().getType(newUri)),
                        file
                );

        // MultipartBody.Part is used to send also the actual file name
        MultipartBody.Part body = MultipartBody.Part.createFormData("image", file.getName(), requestFile);
        if (lenght <= 300) {

            EventApi retrofitAPI = ServiceClient.createService(EventApi.class, getActivity());
            retrofitAPI.upload("events", Constants.token, body).enqueue(new Callback<RESPONSE>() {
                @Override
                public void onResponse(Call<RESPONSE> call, Response<RESPONSE> response) {
                    if (response.code() == 200) {
                        //    progressBar.setVisibility(View.GONE);

                        RESPONSE result = response.body();
                        String url = result.getUrl();
                        createEvent(url);
                        //  Toast.makeText(getActivity(), result.getUrl(), Toast.LENGTH_LONG).show();

                    } else {
                        Toast.makeText(getActivity(), getString(R.string.error), Toast.LENGTH_LONG).show();

                    }
                }

                @Override
                public void onFailure(Call<RESPONSE> call, Throwable throwable) {
                    //progressBar.setVisibility(View.GONE);

                    Toast.makeText(getActivity(), getString(R.string.errorServer), Toast.LENGTH_LONG).show();
                }
            });

        } else {

            Toast.makeText(getActivity(), getString(R.string.imageSize), Toast.LENGTH_LONG).show();

        }
    }

    public void createEvent(String url) {

        HashMap<String, Object> body = new HashMap<>();
        body.put("title_ar", edtName.getText().toString().trim());
        body.put("title_en", edtName_ar.getText().toString().trim());
        body.put("startDate", strdatefrom);
        body.put("endDate", strdateto);
        body.put("openTime", strtimefrom);
        body.put("closeTime", strtimeto);
        body.put("long", longt);
        body.put("lat", lat);
        body.put("venueId", txtVenue);
        body.put("categoryId", eventType);
        body.put("typeOfParticipate", 1);
        body.put("attendance", targetGroup);
        body.put("imageURL", url);
        body.put("desc_en", description.getText().toString().trim());
        body.put("desc_ar", description_ar.getText().toString().trim());
        progressBar.setVisibility(View.VISIBLE);
        EventApi retrofitAPI = ServiceClient.createService(EventApi.class, getActivity());
        String token = Global.getInstance(getContext()).getFromSharedPreferneces(Constants.SH_Token);
        retrofitAPI.addEvent(body, token).enqueue(new Callback<RESPONSE>() {
            @Override
            public void onResponse(Call<RESPONSE> call, Response<RESPONSE> response) {
                if (response.code() == 200) {
                    progressBar.setVisibility(View.GONE);

                    RESPONSE result = response.body();

                    Toast.makeText(getActivity(), "EVENT added successfully", Toast.LENGTH_LONG).show();


//                    getActivity().getSupportFragmentManager().popBackStack();
                } else if (response.code() == 201) {
                    progressBar.setVisibility(View.GONE);

                    Toast.makeText(getActivity(), getString(R.string.eventCreated), Toast.LENGTH_LONG).show();
                    UserProfile userProfile = new UserProfile();
                    userProfile.tag = "add";
                    ((Home) getActivity()).addFragament(userProfile);

                } else {
                    progressBar.setVisibility(View.GONE);
                    APIError error = ErrorUtils.parseError(response);
                    Toast.makeText(getActivity(), error.getErrors().get(0).getMessage(), Toast.LENGTH_LONG).show();

                }
            }

            @Override
            public void onFailure(Call<RESPONSE> call, Throwable throwable) {
                progressBar.setVisibility(View.GONE);

                Toast.makeText(getActivity(), getString(R.string.errorServer), Toast.LENGTH_LONG).show();
            }
        });


    }

    public void getCategories() {
        //  progressBar.setVisibility(View.VISIBLE);
        //  HashMap<String, Object> body = new HashMap<>();
        //  String currentLanguage = Global.getInstance(getActivity()).getFromSharedPrefernec(Constants.currentLangauge);

        //   if (isFirstTimeAppear==true)
        //  pDialog = showLoadingDialog(getActivity());
        //  progressBar.setVisibility(View.VISIBLE);

        EventApi retrofitAPI = ServiceClient.createService(EventApi.class, getActivity());
        retrofitAPI.getCategories().enqueue(new Callback<CategoryData>() {
            @Override
            public void onResponse(Call<CategoryData> call, Response<CategoryData> response) {
                if (response.code() == 200) {

                    categories = response.body().getCategories();


                } else {

                }


            }

            @Override
            public void onFailure(Call<CategoryData> call, Throwable throwable) {
                //  hideLoadingDialog(pDialog);
                //   progressBar.setVisibility(View.GONE);
                Toast.makeText(getActivity(), getString(R.string.errorServer), Toast.LENGTH_LONG).show();


            }
        });

    }

    public void getVenues() {
        //  progressBar.setVisibility(View.VISIBLE);
        //  HashMap<String, Object> body = new HashMap<>();
        //  String currentLanguage = Global.getInstance(getActivity()).getFromSharedPrefernec(Constants.currentLangauge);

        //   if (isFirstTimeAppear==true)
        //  pDialog = showLoadingDialog(getActivity());
        //  progressBar.setVisibility(View.VISIBLE);

        EventApi retrofitAPI = ServiceClient.createService(EventApi.class, getActivity());
        retrofitAPI.getVenues().enqueue(new Callback<VenueResponse>() {
            @Override
            public void onResponse(Call<VenueResponse> call, Response<VenueResponse> response) {
                if (response.code() == 200) {
                    // progressBar.setVisibility(View.GONE);
                    // hideLoadingDialog(pDialog);
                    venues = response.body().getVenues();


                } else {
                    //   progressBar.setVisibility(View.GONE);
                    //   RESPONSE result = ResponseUtils.parseError(response);

                    // Toast.makeText(getActivity(), result.getMessage(), Toast.LENGTH_LONG).show();
                }


            }

            @Override
            public void onFailure(Call<VenueResponse> call, Throwable throwable) {
                //  hideLoadingDialog(pDialog);
                //   progressBar.setVisibility(View.GONE);
                Toast.makeText(getContext(), getString(R.string.errorServer), Toast.LENGTH_LONG).show();
            }
        });

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);


        if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

            if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                openGallery();
                //Crop.pickImage(getActivity());
            }
        }
    }

    private void askpermissions() {


        if (Build.VERSION.SDK_INT < 23) {

            if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    Activity#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for Activity#requestPermissions for more details.
                ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, 2);

            } else {

               openGallery();
              //  Crop.pickImage(getActivity());
            }


        } else {

            if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {


                openGallery();
                //Crop.pickImage(getActivity());

            } else {

                ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);

            }

        }
    }

//    private void beginCrop(Uri source) {
//        Uri destination = Uri.fromFile(new File(getActivity().getCacheDir(), "cropped"));
//        Crop.of(source, destination).asSquare().start(getActivity());
//    }

//    private void handleCrop(int resultCode, Intent data) {
//        if (resultCode == RESULT_OK) {
//            Uri uri= Crop.getOutput( data);
//            event_img.setImageURI(Crop.getOutput( data));
//            try {
//                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), uri);
//                String path = MediaStore.Images.Media.insertImage(getActivity().getContentResolver(),  bitmap, "", null);
//                //   Toast.makeText(getApplicationContext(), path, Toast.LENGTH_LONG).show();
//                Uri screenshotUri = Uri.parse(path);
//
//                String[] filePathColumn = {MediaStore.Images.Media.DATA};
//
//                Cursor cursor = getActivity().getContentResolver().query(screenshotUri, filePathColumn, null, null, null);
//                assert cursor != null;
//                cursor.moveToFirst();
//
//                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
//             mediaPath = cursor.getString(columnIndex);
//                // Set the Image in ImageView for Previewing the Media
//                //  imageView.setImageBitmap(BitmapFactory.decodeFile(mediaPath));
//                cursor.close();
//                postPath = mediaPath;
//
//                Toast.makeText(getActivity(), postPath, Toast.LENGTH_LONG).show();
//
//            } catch (IOException e) {
//
//                e.printStackTrace();
//            }
//        } else if (resultCode == Crop.RESULT_ERROR) {
//           // Toast.makeText(getActivity(), Crop.getError(result).getMessage(), Toast.LENGTH_SHORT).show();
//        }
//    }

}