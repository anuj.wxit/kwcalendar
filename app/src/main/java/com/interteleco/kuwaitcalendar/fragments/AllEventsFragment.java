package com.interteleco.kuwaitcalendar.fragments;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.interteleco.kuwaitcalendar.R;
import com.interteleco.kuwaitcalendar.activities.Home;

import com.interteleco.kuwaitcalendar.adapters.AllEventsAdapter;
import com.interteleco.kuwaitcalendar.server.EventApi;
import com.interteleco.kuwaitcalendar.server.ServiceClient;
import com.interteleco.kuwaitcalendar.server.models.EventData;

import com.interteleco.kuwaitcalendar.server.models.WeekEvents;
import com.interteleco.kuwaitcalendar.utils.Constants;
import com.interteleco.kuwaitcalendar.utils.Fonts;
import com.interteleco.kuwaitcalendar.utils.Global;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class AllEventsFragment extends Fragment {
    RecyclerView lstEvents;
    AllEventsAdapter eventsAdapter;
    LinearLayoutManager linearLayoutManager;

    ProgressBar progressBar;
    @BindView(R.id.menuBtn)
    ImageView backBtn;

    @BindView(R.id.toolbar_title)
    TextView toolbar_title;

    @BindView(R.id.swipe_refresh_layout)
    SwipeRefreshLayout swipeRefreshLayout;


    int pastVisiblesItems, visibleItemCount, totalItemCount;
    private boolean isReceiverRegistered;
    private boolean userScrolled = true;
    Global objGlobal;
    ArrayList<EventData> allevents;
    ArrayList<EventData> events;
    public String Tag = "";
    public String id;
    int count;
    int page = 1;
    View view;

    TextView noContent;
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.all_events, container, false);
        ButterKnife.bind(this, view);
        objGlobal = Global.getInstance(getActivity());
        Toolbar toolbar = (Toolbar) view.findViewById(R.id.toolbar);

        TextView title = (TextView) view.findViewById(R.id.toolbar_title);
        progressBar = (ProgressBar) view.findViewById(R.id.progressBar);
        lstEvents = (RecyclerView) view.findViewById(R.id.lstEvents);
        allevents = new ArrayList<>();
        noContent = (TextView) view.findViewById(R.id.noContent);

        events = new ArrayList<>();
        setFonts();
        if (Tag.equalsIgnoreCase("week")) {
            getweekEvents(id, 10, page);
            toolbar_title.setText(getString(R.string.thisweek));
        } else if (Tag.equalsIgnoreCase("all")) {
            toolbar_title.setText(getString(R.string.allevents));
            getAll(10, page);
        } else {
            toolbar_title.setText(getString(R.string.ourrec));
            getRecommended(id, 10, page);

        }
        ((Home) getActivity()).customizeToolbar(backBtn, getResources().getDrawable(R.drawable.arrow), getResources().getDrawable(R.drawable.arrow_r));
        //  ((Home) getActivity()).customizeToolbar(imgShare, getResources().getDrawable(R.drawable.share), getResources().getDrawable(R.drawable.share_active_ar));


        return view;

    }

    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            updateAction(intent);


        }
    };

    private void updateAction(Intent intent) {
        String action = intent.getStringExtra("action");

        if (action.contains(Constants.LikeToggle)) {
            page = 1;

            allevents.clear();
            events.clear();
            setFonts();
            if (Tag.equalsIgnoreCase("week")) {
                getweekEvents(id, 10, page);

            } else if (Tag.equalsIgnoreCase("all")) {

                getAll(10, page);
            } else {
                getRecommended(id, 10, page);

            }

            eventsAdapter.notifyDataSetChanged();


        }

    }

    @Override
    public void onResume() {
        super.onResume();

        IntentFilter intentFilter = new IntentFilter(Global.getInstance(getActivity()).BROADCAST_ACTION);

        getContext().registerReceiver(broadcastReceiver, intentFilter);


    }

    @Override
    public void onDestroy() {

        super.onDestroy();
        getContext().unregisterReceiver(broadcastReceiver);
    }


    @OnClick({R.id.menuBtn})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.menuBtn: {
                getFragmentManager().popBackStack();
            }
            break;
        }

    }

    public void bindData() {
        linearLayoutManager = new LinearLayoutManager(getActivity());
        lstEvents.setHasFixedSize(true);
        lstEvents.setLayoutManager(linearLayoutManager);
        eventsAdapter = new AllEventsAdapter((AppCompatActivity) getActivity(), allevents);
        lstEvents.setAdapter(eventsAdapter);
        lstEvents.addOnScrollListener(new RecyclerView.OnScrollListener() {

            @Override
            public void onScrollStateChanged(RecyclerView recyclerView,
                                             int newState) {

                super.onScrollStateChanged(recyclerView, newState);

                // If scroll state is touch scroll then set userScrolled
                // true
                if (newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL) {
                    userScrolled = true;

                }

            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx,
                                   int dy) {


                if (dy > 0) {
                    visibleItemCount = linearLayoutManager.getChildCount();
                    totalItemCount = linearLayoutManager.getItemCount();
                    pastVisiblesItems = linearLayoutManager
                            .findFirstVisibleItemPosition();

                    // Now check if userScrolled is true and also check if
                    // the item is end then update recycler view and set
                    // userScrolled to false
                    if (userScrolled
                            && (visibleItemCount + pastVisiblesItems) == totalItemCount) {
                        userScrolled = false;

                        //  Log.d("TotalPages", TotalPages + "");
                        if (count > allevents.size()) {
                            page = page + 1;
                            if (Tag.equalsIgnoreCase("week")) {
                                getweekEvents(id, 10, page);

                            } else if (Tag.equalsIgnoreCase("all")) {

                                getAll(10, page);
                            } else {
                                getRecommended(id, 10, page);

                            }

                        }
                    }

                }
            }

        });

        if (swipeRefreshLayout != null) {
            swipeRefreshLayout.setRefreshing(false);
            swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
                @Override
                public void onRefresh() {

                    swipeRefreshLayout.setRefreshing(false);
                    page = 1;
                    allevents.clear();
                    events.clear();
                    setFonts();
                    if (Tag.equalsIgnoreCase("week")) {
                        getweekEvents(id, 10, page);

                    } else if (Tag.equalsIgnoreCase("all")) {

                        getAll(10, page);
                    } else {
                        getRecommended(id, 10, page);

                    }

                    eventsAdapter.notifyDataSetChanged();
                }
            });

        }

    }

    private void setFonts() {
        Fonts.set(new TextView[]{toolbar_title}, getActivity(), 1);

    }

    public void getweekEvents(String category, int size, int page) {
        // body.put("id", "");
        //   if (isFirstTimeAppear==true)
        //  pDialog = showLoadingDialog(getActivity());
        progressBar.setVisibility(View.VISIBLE);
        String token = Global.getInstance(getContext()).getFromSharedPreferneces(Constants.SH_Token);
        EventApi retrofitAPI = ServiceClient.createService(EventApi.class, getActivity());
        retrofitAPI.getWeekEvents(token, category, size, page).enqueue(new Callback<WeekEvents>() {
            @Override
            public void onResponse(Call<WeekEvents> call, Response<WeekEvents> response) {
                if (response.code() == 200) {
                    if (swipeRefreshLayout != null)
                        swipeRefreshLayout.setRefreshing(false);
                    progressBar.setVisibility(View.GONE);

                    events = response.body().getData();
                    allevents.addAll(events);
                    count = response.body().getTotal();

                    if ( allevents.size() < 1)
                        noContent.setVisibility(View.VISIBLE);
                    else
                        noContent.setVisibility(View.GONE);
                    bindData();

                } else {
                    if (swipeRefreshLayout != null)
                        swipeRefreshLayout.setRefreshing(false);
                    progressBar.setVisibility(View.GONE);
                    Toast.makeText(getActivity(), getString(R.string.serverError), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<WeekEvents> call, Throwable throwable) {
                if (swipeRefreshLayout != null)
                    swipeRefreshLayout.setRefreshing(false);
                progressBar.setVisibility(View.GONE);
                Toast.makeText(getActivity(), getString(R.string.serverError), Toast.LENGTH_LONG).show();
            }
        });
    }

    public void getRecommended(String category, int size, int page) {
        String token = Global.getInstance(getContext()).getFromSharedPreferneces(Constants.SH_Token);

        progressBar.setVisibility(View.VISIBLE);

        EventApi retrofitAPI = ServiceClient.createService(EventApi.class, getActivity());
        retrofitAPI.getRecomEvents(token, category, size, page).enqueue(new Callback<WeekEvents>() {
            @Override
            public void onResponse(Call<WeekEvents> call, Response<WeekEvents> response) {
                if (response.code() == 200) {
                    if (swipeRefreshLayout != null)
                        swipeRefreshLayout.setRefreshing(false);
                    progressBar.setVisibility(View.GONE);

                    events = response.body().getData();
                    allevents.addAll(events);

                    count = response.body().getTotal();
                    if ( allevents.size() < 1)
                        noContent.setVisibility(View.VISIBLE);
                    else
                        noContent.setVisibility(View.GONE);
                    bindData();

                } else {
                    if (swipeRefreshLayout != null)
                        swipeRefreshLayout.setRefreshing(false);
                    progressBar.setVisibility(View.GONE);
                    Toast.makeText(getActivity(), getString(R.string.serverError), Toast.LENGTH_LONG).show();

                }
            }

            @Override
            public void onFailure(Call<WeekEvents> call, Throwable throwable) {

                if (swipeRefreshLayout != null)
                    swipeRefreshLayout.setRefreshing(false);
                progressBar.setVisibility(View.GONE);
                Toast.makeText(getActivity(), getString(R.string.serverError), Toast.LENGTH_LONG).show();
            }
        });
    }

    public void getAll(int size, int page) {
        String token = Global.getInstance(getContext()).getFromSharedPreferneces(Constants.SH_Token);
        progressBar.setVisibility(View.VISIBLE);
        EventApi retrofitAPI = ServiceClient.createService(EventApi.class, getActivity());
        retrofitAPI.getAllEvents(token, size, page).enqueue(new Callback<WeekEvents>() {
            @Override
            public void onResponse(Call<WeekEvents> call, Response<WeekEvents> response) {
                if (response.code() == 200) {
                    if (swipeRefreshLayout != null)
                        swipeRefreshLayout.setRefreshing(false);
                    progressBar.setVisibility(View.GONE);

                    events = response.body().getData();
                    allevents.addAll(events);
                    count = response.body().getTotal();
                    if ( allevents.size() < 1)
                        noContent.setVisibility(View.VISIBLE);
                    else
                        noContent.setVisibility(View.GONE);
                    bindData();

                } else {
                    if (swipeRefreshLayout != null)
                        swipeRefreshLayout.setRefreshing(false);
                    progressBar.setVisibility(View.GONE);
                    Toast.makeText(getActivity(), getString(R.string.serverError), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<WeekEvents> call, Throwable throwable) {
                if (swipeRefreshLayout != null)
                    swipeRefreshLayout.setRefreshing(false);
                progressBar.setVisibility(View.GONE);
                Toast.makeText(getActivity(), getString(R.string.serverError), Toast.LENGTH_LONG).show();
            }
        });

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.bind(this, view).unbind();

    }

}
