package com.interteleco.kuwaitcalendar.fragments;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import com.bumptech.glide.Glide;
import com.interteleco.kuwaitcalendar.R;
import com.interteleco.kuwaitcalendar.activities.Home;
import com.interteleco.kuwaitcalendar.activities.MainActivity;
import com.interteleco.kuwaitcalendar.dialogs.CategoryList;
import com.interteleco.kuwaitcalendar.dialogs.ChangeField;
import com.interteleco.kuwaitcalendar.listeners.ListListener;
import com.interteleco.kuwaitcalendar.server.EventApi;
import com.interteleco.kuwaitcalendar.server.ServiceClient;
import com.interteleco.kuwaitcalendar.server.UserApi;
import com.interteleco.kuwaitcalendar.server.models.APIError;
import com.interteleco.kuwaitcalendar.server.models.CategoryData;
import com.interteleco.kuwaitcalendar.server.models.RESPONSE;
import com.interteleco.kuwaitcalendar.server.models.Target;
import com.interteleco.kuwaitcalendar.server.models.User;
import com.interteleco.kuwaitcalendar.server.models.UserModel;
import com.interteleco.kuwaitcalendar.server.models.Venue;
import com.interteleco.kuwaitcalendar.utils.Constants;
import com.interteleco.kuwaitcalendar.utils.ErrorUtils;
import com.interteleco.kuwaitcalendar.utils.Fonts;
import com.interteleco.kuwaitcalendar.utils.Global;
import com.interteleco.kuwaitcalendar.utils.Validator;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EditUserProfile extends Fragment implements ListListener {

    @BindView(R.id.saveChanges)
    AppCompatButton saveChanges;

    @BindView(R.id.txtErrorName)
    TextView txtErrorName;

    @BindView(R.id.txtErrorEmail)
    TextView txtErrorEmail;
    @BindView(R.id.txtErrorPhone)
    TextView txtErrorPhone;

    @BindView(R.id.txtErrorAccType)
    TextView txtErrorAccType;

    @BindView(R.id.txtErrorInterest)
    TextView txtErrorInterest;
    //
    @BindView(R.id.txtverifyEmail)
    TextView txtverifyEmail;
    @BindView(R.id.emailnotVerified)
    TextView emailNotVerified;
    @BindView(R.id.txtverifyPhone)
    TextView txtverifyPhone;

    @BindView(R.id.notVerified)
    TextView phoneNotVerified;
    @BindView(R.id.txtName)
    TextView txtName;
    @BindView(R.id.txtEmail)
    TextView txtEmail;
    @BindView(R.id.txtphone)
    TextView txtphone;
    @BindView(R.id.txtAccount)
    TextView txtAccount;
    @BindView(R.id.txtInterest)
    TextView txtInterest;

    @BindView(R.id.changePassword)
    TextView changePassword;
    @BindView(R.id.edtName)
    EditText edtName;

    @BindView(R.id.edtEmail)
    EditText edtEmail;

    @BindView(R.id.phone)
    AppCompatEditText phone;


    @BindView(R.id.accType)
    AppCompatButton accountType;

    @BindView(R.id.interest)
    AppCompatButton interest;


    @BindView(R.id.selectPic)
    ImageView selectPic;


    @BindView(R.id.avatar)
    CircleImageView avatar;

    //    @BindView(R.id.avatar)
//    ImageView profileImg;
    @BindView(R.id.menuBtn)
    ImageView backBtn;

    @BindView(R.id.toolbar_title)
    TextView toolbar_title;

    Global objGlobal;

    int Pick_Image = 1;

    View view;

    String interestCategory;

    ArrayList<CategoryData.Category> categories;

    Uri imguri;
    String mediaPath, postPath;
    String name, email, intCat, mobile;
    int type;
    Boolean emailStatus, phoneStatus;
    public String Tag = "";
    public String navigationTag = "";
    public String userToken;
    String avatarUrl;
    ProgressBar progressBar;

    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.edit_user, container, false);
        ButterKnife.bind(this, view);
        objGlobal = Global.getInstance(getActivity());

        progressBar = (ProgressBar) view.findViewById(R.id.progressBar);
        if (navigationTag.equalsIgnoreCase("verification"))
            ((MainActivity) getActivity()).customizeToolbar(backBtn, getResources().getDrawable(R.drawable.arrow), getResources().getDrawable(R.drawable.arrow_r));
        else
            ((Home) getActivity()).customizeToolbar(backBtn, getResources().getDrawable(R.drawable.arrow), getResources().getDrawable(R.drawable.arrow_r));

        setFonts();
        categories = new ArrayList<>();
        getProfileInfo();
        getCategories();


        //
        return view;
    }

    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            updateAction(intent);


        }
    };

    private void updateAction(Intent intent) {
        String action = intent.getStringExtra("action");

        if (action.contains(Constants.verifyPhone)) {

            phoneStatus = true;
            txtverifyPhone.setVisibility(View.GONE);
            phoneNotVerified.setVisibility(View.GONE);

        } else if (action.contains(Constants.VerifyEmail)) {
            emailStatus = true;
            txtverifyEmail.setVisibility(View.GONE);
            emailNotVerified.setVisibility(View.GONE);
        } else if (action.contains(Constants.ChangeEmail)) {

            String newEmail = intent.getStringExtra("email");
            edtEmail.setText(newEmail);
        } else if (action.contains(Constants.ChangePhone)) {
            String newEmail = intent.getStringExtra("mobile");
            phone.setText(newEmail);
        }

    }

    @Override
    public void onResume() {
        super.onResume();

        IntentFilter intentFilter = new IntentFilter(Global.getInstance(getActivity()).BROADCAST_ACTION);

        getContext().registerReceiver(broadcastReceiver, intentFilter);


    }

    @Override
    public void onDestroy() {

        super.onDestroy();
        getContext().unregisterReceiver(broadcastReceiver);
    }


    private void setFonts() {
        Fonts.set(new TextView[]{toolbar_title}, getActivity(), 1);
        Fonts.set(new TextView[]{txtErrorName, txtErrorEmail, txtErrorInterest, txtErrorPhone, changePassword, txtName, txtEmail,
                txtphone, txtAccount, txtInterest, emailNotVerified, txtverifyEmail, phoneNotVerified, txtverifyPhone}, getActivity(), 0);
        Fonts.set(new EditText[]{edtName, edtEmail, phone,}, getActivity(), 0);
        Fonts.set(new Button[]{saveChanges}, getActivity(), 0);
    }

    @OnClick({R.id.interest, R.id.selectPic, R.id.saveChanges, R.id.txtverifyEmail, R.id.txtverifyPhone,
            R.id.emailnotVerified, R.id.notVerified, R.id.menuBtn, R.id.changePassword})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.interest: {
                CategoryList list = new CategoryList();
                list.Tag = "cat";
                list.listener = this;
                list.category = categories;
                list.show(getActivity().getSupportFragmentManager(), "list");

            }
            break;
            case R.id.saveChanges: {
                editUser();
            }
            break;
            case R.id.selectPic: {
                askpermissions();
            }
            break;
            case R.id.changePassword: {
                if (navigationTag.equalsIgnoreCase("home")) {
                    ChangePassword changePassword = new ChangePassword();
                    changePassword.userToken = userToken;
                    changePassword.navigationTag = "home";
                    ((Home) getActivity()).addFragament(changePassword);

                } else {
                    ChangePassword changePassword = new ChangePassword();
                    changePassword.userToken = userToken;
                    changePassword.navigationTag = "verification";
                    ((MainActivity) getActivity()).addFragament(changePassword);


                }

            }
            break;
            case R.id.txtverifyEmail: {
                VerifyEmail verifyEmail = new VerifyEmail();
                verifyEmail.tag = "verification";
                verifyEmail.token = userToken;
                verifyEmail.phoneStatus = phoneStatus;
                ((MainActivity) getActivity()).hideKeyBoard();
                ((MainActivity) getActivity()).addFragamentbyTAG(verifyEmail, "verify");
            }
            break;
            case R.id.txtverifyPhone: {
                VerifyPhone verifyPhone = new VerifyPhone();
                verifyPhone.tag = "verification";
                verifyPhone.token = userToken;
                verifyPhone.emailStatus = emailStatus;
                ((MainActivity) getActivity()).hideKeyBoard();

                ((MainActivity) getActivity()).addFragamentbyTAG(verifyPhone, "verify");
            }
            break;

            case R.id.emailnotVerified: {
                ChangeField changeDialog = new ChangeField();
                changeDialog.userToken = userToken;
                changeDialog.Tag = "email";
                changeDialog.show(getActivity().getSupportFragmentManager(), "dialog");
            }
            break;

            case R.id.notVerified: {
                ChangeField changeDialog = new ChangeField();
                changeDialog.userToken = userToken;
                changeDialog.Tag = "mobile";
                changeDialog.show(getActivity().getSupportFragmentManager(), "changedialog");
            }
            break;

            case R.id.menuBtn: {
                getFragmentManager().popBackStack();
            }
            break;
        }

    }

    private void bindUserInfo() {
        edtEmail.setText(email);
        edtName.setText(name);
        phone.setText(mobile);
        interest.setText(intCat);
        if(getContext()!=null&&avatarUrl!=null)
        Glide.with(getContext()).load(Constants.ImgURL + avatarUrl).into(avatar);
        if (type == 1)
            accountType.setText(getString(R.string.individuals));
        else if (type == 2)
            accountType.setText(getString(R.string.privatesector));
        else
            accountType.setText(getString(R.string.government));
        if (!emailStatus) {
            txtverifyEmail.setVisibility(View.VISIBLE);
            emailNotVerified.setVisibility(View.VISIBLE);
        }
        if (phoneStatus == false) {
            phone.setEnabled(false);
            txtverifyPhone.setVisibility(View.VISIBLE);
            phoneNotVerified.setVisibility(View.VISIBLE);
        } else {
            phone.setEnabled(false);

        }
        if (emailStatus == false) {
            txtverifyEmail.setVisibility(View.VISIBLE);
            emailNotVerified.setVisibility(View.VISIBLE);
            edtEmail.setEnabled(false);
        } else {

            edtEmail.setEnabled(false);

        }
    }

    private void openGallery() {
        //  Intent gallery = new Intent();
        //  gallery.setType("image/*");
        Intent gallery = new Intent(Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(gallery, Pick_Image);
    }


    private void editUser() {

        if (edtName.getText().toString() != null && !edtName.getText().toString().equalsIgnoreCase("")) {

            if (Validator.isEmptyString(edtEmail.getText().toString())) {

                if (Validator.isValidEMail(edtEmail.getText().toString())) {
                    //    if (Validator.isEmptyString(phone.getText().toString())) {
                    //     if (Validator.isMobile(phone.getText().toString())) {

                    if (Validator.isEmptyString(interestCategory)) {

                        if (postPath != null)

                            uploadImage();
                        else
                            editUserAccount(avatarUrl);
                        //        Toast.makeText(getActivity(), "Valid data", Toast.LENGTH_LONG).show();
                    } else {
                        txtErrorInterest.setVisibility(View.VISIBLE);
                    }


//                        } else {
//                            phone.requestFocus();
//                            txtErrorPhone.setVisibility(View.VISIBLE);
//                            txtErrorPhone.setText(getString(R.string.mobileStart));
//                        }
//                    } else {
//                        phone.requestFocus();
//                        txtErrorPhone.setVisibility(View.VISIBLE);
//                    }
                } else {
                    edtEmail.requestFocus();
                    txtErrorEmail.setVisibility(View.VISIBLE);
                    txtErrorEmail.setText(getString(R.string.entervalidEmail));
                }
            } else {

                edtEmail.requestFocus();
                txtErrorEmail.setVisibility(View.VISIBLE);

            }

        } else {
            txtErrorName.setVisibility(View.VISIBLE);
            edtName.requestFocus();

        }


    }

    @Override
    public void selectTarget(Target target) {

    }

    @Override
    public void selectVenue(Venue venue) {

    }

    @Override
    public void selectCategory(CategoryData.Category category) {
        if (Global.getInstance(getContext()).getFromSharedPreferneces("mylang").equalsIgnoreCase("en"))
            interest.setText(category.getName_en());
        else
            interest.setText(category.getName_ar());

        interestCategory = category.get_id();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == Pick_Image) {
            if (data != null) {
                imguri = data.getData();


                int hight = 0, width = 0;

                Bitmap bitmap = null;
                try {

                    bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), imguri);
                    hight = bitmap.getHeight();
                    width = bitmap.getWidth();


                } catch (IOException e) {
                    e.printStackTrace();
                }
                avatar.setImageBitmap(bitmap);
                //uploadImage();

                //    Toast.makeText(getActivity(), imguri.getPath(), Toast.LENGTH_LONG).show();

                //Toast.makeText(getActivity(), hight + "/" + width, Toast.LENGTH_LONG).show();
                Uri selectedImage = data.getData();
                String[] filePathColumn = {MediaStore.Images.Media.DATA};

                Cursor cursor = getActivity().getContentResolver().query(selectedImage, filePathColumn, null, null, null);
                assert cursor != null;
                cursor.moveToFirst();

                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                mediaPath = cursor.getString(columnIndex);
                // Set the Image in ImageView for Previewing the Media
                //  imageView.setImageBitmap(BitmapFactory.decodeFile(mediaPath));
                cursor.close();
                postPath = mediaPath;
                //  Toast.makeText(getActivity(), postPath, Toast.LENGTH_LONG).show();
            }
        }

    }

    private void uploadImage() {

        //  Toast.makeText(getActivity(), postPath, Toast.LENGTH_LONG).show();
        File file = new File(postPath);
        long lenght = file.length() / 1024;
       // Toast.makeText(getActivity(), +lenght + "", Toast.LENGTH_LONG).show();
        Map<String, RequestBody> map = new HashMap<>();
        //ile file = new File(postPath);

        // Parsing any Media type file
        RequestBody requestBody = RequestBody.create(MediaType.parse("*/*"), file);
        //  map.put("file\"; filename=\"" + file.getName() + "\"", requestBody);
        map.put("image", requestBody);
        // create RequestBody instance from file
        RequestBody requestFile =
                RequestBody.create(
                        MediaType.parse(getActivity().getContentResolver().getType(imguri)),
                        file
                );

        // MultipartBody.Part is used to send also the actual file name
        MultipartBody.Part body = MultipartBody.Part.createFormData("image", file.getName(), requestFile);

        if (lenght <= 300) {
            UserApi retrofitAPI = ServiceClient.createService(UserApi.class, getActivity());
            retrofitAPI.uploadProfile("users", body).enqueue(new Callback<RESPONSE>() {
                @Override
                public void onResponse(Call<RESPONSE> call, Response<RESPONSE> response) {
                    if (response.code() == 200) {
                        //    progressBar.setVisibility(View.GONE);

                        RESPONSE result = response.body();

                   //     Toast.makeText(getActivity(), result.getUrl(), Toast.LENGTH_LONG).show();
                        String url = result.getUrl();
                        editUserAccount(url);
//                    getActivity().getSupportFragmentManager().popBackStack();
                    } else if (response.code() == 201) {
                        //  progressBar.setVisibility(View.GONE);

                        // RESPONSE result = ResponseUtil.parseError(response);

                        Toast.makeText(getContext(), getString(R.string.error), Toast.LENGTH_LONG).show();
                    } else {
                        Toast.makeText(getContext(), getString(R.string.error), Toast.LENGTH_LONG).show();

                    }
                }

                @Override
                public void onFailure(Call<RESPONSE> call, Throwable throwable) {
                    //progressBar.setVisibility(View.GONE);

                    Toast.makeText(getContext(), getString(R.string.errorServer), Toast.LENGTH_LONG).show();
                }
            });
        } else {
        }
    }

    public void editUserAccount(String Url) {

        HashMap<String, Object> body = new HashMap<>();
        body.put("name", edtName.getText().toString().trim());
        body.put("interestedCategory", interestCategory);
        if (postPath != null)
            body.put("avatar", Url);
        else
            body.put("avatar", avatarUrl);

        if (navigationTag.equalsIgnoreCase("home"))
            userToken = Global.getInstance(getContext()).getFromSharedPreferneces(Constants.SH_Token);
        progressBar.setVisibility(View.VISIBLE);
        UserApi retrofitAPI = ServiceClient.createService(UserApi.class, getActivity());
        retrofitAPI.editProfile(userToken, body).enqueue(new Callback<RESPONSE>() {
            @Override
            public void onResponse(Call<RESPONSE> call, Response<RESPONSE> response) {
                if (response.code() == 200) {
                    progressBar.setVisibility(View.GONE);

                    RESPONSE result = response.body();

                    Toast.makeText(getActivity(), getString(R.string.editSuccess), Toast.LENGTH_LONG).show();


                } else {
                    progressBar.setVisibility(View.GONE);
                    Toast.makeText(getContext(), getString(R.string.error), Toast.LENGTH_LONG).show();
                    APIError error = ErrorUtils.parseError(response);
                    Toast.makeText(getActivity(), error.getErrors().get(0).getMessage(), Toast.LENGTH_LONG).show();

                }
            }

            @Override
            public void onFailure(Call<RESPONSE> call, Throwable throwable) {
                progressBar.setVisibility(View.GONE);

                Toast.makeText(getContext(), getString(R.string.errorServer), Toast.LENGTH_LONG).show();
            }
        });
    }

    public void getCategories() {
        //  progressBar.setVisibility(View.VISIBLE);
        //  HashMap<String, Object> body = new HashMap<>();
        //  String currentLanguage = Global.getInstance(getActivity()).getFromSharedPrefernec(Constants.currentLangauge);

        //   if (isFirstTimeAppear==true)
        //  pDialog = showLoadingDialog(getActivity());
        //  progressBar.setVisibility(View.VISIBLE);

        EventApi retrofitAPI = ServiceClient.createService(EventApi.class, getActivity());
        retrofitAPI.getCategories().enqueue(new Callback<CategoryData>() {
            @Override
            public void onResponse(Call<CategoryData> call, Response<CategoryData> response) {
                if (response.code() == 200) {

                    categories = response.body().getCategories();


                } else {
                    Toast.makeText(getContext(), getString(R.string.error), Toast.LENGTH_LONG).show();
                }


            }

            @Override
            public void onFailure(Call<CategoryData> call, Throwable throwable) {
                //  hideLoadingDialog(pDialog);
                //   progressBar.setVisibility(View.GONE);
                Toast.makeText(getContext(), getString(R.string.errorServer), Toast.LENGTH_LONG).show();


            }
        });

    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);


        if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

            if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                openGallery();
            }
        }
    }

    private void askpermissions() {


        if (Build.VERSION.SDK_INT < 23) {

            if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    Activity#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for Activity#requestPermissions for more details.
                ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, 2);

            } else {


                openGallery();

            }


        } else {

            if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {


                openGallery();


            } else {

                ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);

            }

        }
    }

    private void getProfileInfo() {
        progressBar.setVisibility(View.VISIBLE);
        if (navigationTag.equalsIgnoreCase("home"))
            userToken = Global.getInstance(getContext()).getFromSharedPreferneces(Constants.SH_Token);
        UserApi retrofitAPI = ServiceClient.createService(UserApi.class, getActivity());
        retrofitAPI.getProfileInfo(userToken).enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                if (response.code() == 200) {
                    progressBar.setVisibility(View.GONE);
                    UserModel userModel = response.body().getUser();
                    if (Global.getInstance(getContext()).getFromSharedPreferneces("mylang").equalsIgnoreCase("en"))
                        intCat = userModel.getInterestedCategory().getName_en();
                    else
                        intCat = userModel.getInterestedCategory().getName_ar();

                    interestCategory = userModel.getInterestedCategory().get_id();
                  type= userModel.getAccountType();

                    name = userModel.getName();
                    email = userModel.getEmail();
                    mobile = userModel.getMobileNumber();
                    avatarUrl = userModel.getAvatar();
                    emailStatus = userModel.getEmailActivation();
                    phoneStatus = userModel.getPhoneActivation();
                  if(getContext()!=null)
                    bindUserInfo();
                } else {
                    progressBar.setVisibility(View.GONE);

                    Toast.makeText(getContext(), getString(R.string.error), Toast.LENGTH_LONG).show();
                }


            }

            @Override
            public void onFailure(Call<User> call, Throwable throwable) {
                //  hideLoadingDialog(pDialog);
                progressBar.setVisibility(View.GONE);
                Toast.makeText(getContext(), getString(R.string.errorServer), Toast.LENGTH_LONG).show();

            }
        });

    }


}

