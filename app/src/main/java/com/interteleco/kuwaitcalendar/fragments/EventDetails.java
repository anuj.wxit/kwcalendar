package com.interteleco.kuwaitcalendar.fragments;

import android.Manifest;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.CalendarContract;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatButton;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import com.borjabravo.readmoretextview.ReadMoreTextView;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.interteleco.kuwaitcalendar.R;
import com.interteleco.kuwaitcalendar.activities.Home;
import com.interteleco.kuwaitcalendar.server.EventApi;
import com.interteleco.kuwaitcalendar.server.ServiceClient;
import com.interteleco.kuwaitcalendar.server.models.APIError;
import com.interteleco.kuwaitcalendar.server.models.EventData;
import com.interteleco.kuwaitcalendar.server.models.EventModel;
import com.interteleco.kuwaitcalendar.server.models.RESPONSE;
import com.interteleco.kuwaitcalendar.utils.Constants;
import com.interteleco.kuwaitcalendar.utils.ErrorUtils;
import com.interteleco.kuwaitcalendar.utils.Fonts;
import com.interteleco.kuwaitcalendar.utils.Global;
import com.interteleco.kuwaitcalendar.utils.ResponseUtil;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EventDetails extends Fragment {
    @BindView(R.id.title)
    TextView txtTitle;
    @BindView(R.id.location)
    TextView txtLocation;

    @BindView(R.id.date_label)
    TextView label;

    @BindView(R.id.date)
    TextView txtDate;
    @BindView(R.id.time_label)
    TextView labelTime;

    @BindView(R.id.time)
    TextView txtTime;

    @BindView(R.id.moreText)
    ReadMoreTextView txtDescription;

    @BindView(R.id.viewBtn)
    AppCompatButton ViewDirection;
    @BindView(R.id.imageEvent)
    ImageView imgEvent;
    @BindView(R.id.menuBtn)
    ImageView menu;
    @BindView(R.id.tickets_btn)
    TextView tickets;
    @BindView(R.id.calender_btn)
    LinearLayout calendar;
    @BindView(R.id.txtCalender)
    TextView txtCalendar;

    @BindView(R.id.share)
    ImageView share;
    @BindView(R.id.favourite)
    ImageView favourite;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;

    @BindView(R.id.time_view)
    RelativeLayout timeLayout;

    Global objGlobal;
    String title, Description, startDate, endDate, imgUrl, location, eventStart, eventEnd, startTime, endTime;
    public String id;
    LocationManager locationManager;
    Double lat, longt;
    LocationListener locationListener;
    String authorized;
    boolean isLiked, isCalendered;
    String tag = "";
    String shareUrl;

    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.event_details, container, false);
        ButterKnife.bind(this, view);
        objGlobal = Global.getInstance(getActivity());
        authorized = Global.getInstance(getActivity()).getFromSharedPreferneces(Constants.Authorized);

        if (tag.equalsIgnoreCase("myevent"))
            favourite.setVisibility(View.GONE);
        else
            favourite.setVisibility(View.VISIBLE);
        setFonts();
        getEventData(id);


        locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);

        locationListener = new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {

                //   Toast.makeText(getActivity(), location.toString(), Toast.LENGTH_LONG).show();
            }

            @Override
            public void onStatusChanged(String s, int i, Bundle bundle) {

            }

            @Override
            public void onProviderEnabled(String s) {

            }

            @Override
            public void onProviderDisabled(String s) {

            }
        };


        share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                //  if (authorized != null && !authorized.equalsIgnoreCase("")) {
                shareEvent(id);
                askpermissionsToShare();

                // } else {

                //     Toast.makeText(getActivity(), getString(R.string.loginFirst), Toast.LENGTH_LONG).show();

                //   }


            }
        });
        ((Home) getActivity()).customizeToolbar(menu, getResources().getDrawable(R.drawable.next), getResources().getDrawable(R.drawable.nexticon));

        favourite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (authorized != null && !authorized.equalsIgnoreCase("")) {

                    if (!tag.equalsIgnoreCase("myevent")) {
                        if (favourite.getTag().toString().equalsIgnoreCase("like"))
                            likeEvent(id, "like");
                        else
                            likeEvent(id, "unlike");
                    }
                } else {

                    Toast.makeText(getActivity(), getString(R.string.loginFirst), Toast.LENGTH_LONG).show();

                }


            }
        });


        return view;

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 1) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                    locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);

                    locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locationListener);

                    Location lastKnownLocation = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                    if (lastKnownLocation != null) {
                        //  Log.e("TAG", "GPS is on");
                        Double latitude = lastKnownLocation.getLatitude();
                        Double longitude = lastKnownLocation.getLongitude();
                        //  centerMapOnLocation(lastKnownLocation, "Your location");
                        viewDirection(latitude, longitude, lat, longt);
                    } else {
                        if (lastKnownLocation == null)
                            Toast.makeText(getActivity(), getString(R.string.enablelocation), Toast.LENGTH_LONG).show();
                        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, locationListener);
                        lastKnownLocation = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                        //   Toast.makeText(getActivity(), lastKnownLocation.toString(), Toast.LENGTH_LONG).show();
                        if (lastKnownLocation != null) {
                            //  Log.e("TAG", "GPS is on");
                            Double latitude = lastKnownLocation.getLatitude();
                            Double longitude = lastKnownLocation.getLongitude();
                            // Toast.makeText(getActivity(), +latitude + longitude + "", Toast.LENGTH_LONG).show();
                            viewDirection(latitude, longitude, lat, longt);
                        }


                    }


                }
            }
        } else if (requestCode == 2) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.WRITE_CALENDAR) == PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(getContext(), Manifest.permission.READ_CALENDAR) == PackageManager.PERMISSION_GRANTED) {

                    addToCalendar();

                }
            }
        } else {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(getContext(), Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {

                    shareImage();

                }
            }


        }

    }

    private void viewDirection(Double userat, Double userlong, Double eventlat, Double eventlongt) {
        Intent navigation = new Intent(Intent.ACTION_VIEW,
                Uri.parse("http://maps.google.com/maps?f=d&source=s_d" + "&saddr=" + userat + "," + userlong + "&daddr=" + eventlat + "," + eventlongt));
        navigation.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK & Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
        navigation.setClassName("com.google.android.apps.maps", "com.google.android.maps.MapsActivity");
        startActivity(navigation);
    }

    @OnClick({R.id.share, R.id.menuBtn, R.id.viewBtn, R.id.calender_btn, R.id.tickets_btn})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.menuBtn: {
                getFragmentManager().popBackStack();
            }
            break;
            case R.id.viewBtn: {

                //   Toast.makeText(getActivity(), "view clicked", Toast.LENGTH_LONG).show();
                if (Build.VERSION.SDK_INT < 23) {

                    if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                        // TODO: Consider calling
                        //    Activity#requestPermissions
                        // here to request the missing permissions, and then overriding
                        //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                        //                                          int[] grantResults)
                        // to handle the case where the user grants the permission. See the documentation
                        // for Activity#requestPermissions for more details.
                        ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);

                    } else {
//                        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locationListener);
//                        Location lastKnownLocation = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
//                        //   Toast.makeText(getActivity(), lastKnownLocation.toString(), Toast.LENGTH_LONG).show();
//                        if (lastKnownLocation != null) {
//                            //  Log.e("TAG", "GPS is on");
//                            Double latitude = lastKnownLocation.getLatitude();
//                            Double longitude = lastKnownLocation.getLongitude();
//                            // Toast.makeText(getActivity(), +latitude + longitude + "", Toast.LENGTH_LONG).show();
//                            viewDirection(latitude, longitude, lat, longt);
//                        } else {
//                              Toast.makeText(getActivity(), getString(R.string.enablelocation), Toast.LENGTH_LONG).show();
//                            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, locationListener);
//                            lastKnownLocation = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
//                            //   Toast.makeText(getActivity(), lastKnownLocation.toString(), Toast.LENGTH_LONG).show();
//                            if (lastKnownLocation != null) {
//                                //  Log.e("TAG", "GPS is on");
//                                Double latitude = lastKnownLocation.getLatitude();
//                                Double longitude = lastKnownLocation.getLongitude();
//                                // Toast.makeText(getActivity(), +latitude + longitude + "", Toast.LENGTH_LONG).show();
//                                viewDirection(latitude, longitude, lat, longt);
//                            }
//                        }

                        getLocation();
                    }

                } else {

                    getLocation();

                }

            }
            break;

            case R.id.calender_btn: {

                if (authorized != null && !authorized.equalsIgnoreCase("")) {
                    if (!isCalendered)
                        askpermissions();

                    else
                        Toast.makeText(getActivity(), getString(R.string.inCalendar), Toast.LENGTH_LONG).show();
                } else {

                    Toast.makeText(getActivity(), getString(R.string.loginFirst), Toast.LENGTH_LONG).show();

                }


            }
            break;
        }
    }

    private void getLocation() {
        if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {

            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locationListener);

            Location lastKnownLocation = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);

            if (lastKnownLocation != null) {
                //  Log.e("TAG", "GPS is on");
                Double latitude = lastKnownLocation.getLatitude();
                Double longitude = lastKnownLocation.getLongitude();

                viewDirection(latitude, longitude, lat, longt);

            } else {
                if (lastKnownLocation == null)
                    Toast.makeText(getActivity(), getString(R.string.enablelocation), Toast.LENGTH_LONG).show();
                locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, locationListener);
                lastKnownLocation = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                //   Toast.makeText(getActivity(), lastKnownLocation.toString(), Toast.LENGTH_LONG).show();
                if (lastKnownLocation != null) {
                    //  Log.e("TAG", "GPS is on");
                    Double latitude = lastKnownLocation.getLatitude();
                    Double longitude = lastKnownLocation.getLongitude();
                    // Toast.makeText(getActivity(), +latitude + longitude + "", Toast.LENGTH_LONG).show();
                    viewDirection(latitude, longitude, lat, longt);
                }


            }


        } else {

            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);

        }


    }

    private void share() {
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT, txtDescription.getText());

        sendIntent.putExtra(Intent.EXTRA_TEXT, "https://medium.com/androiddevelopers/sharing-content-between-android-apps-2e6db9d1368b");
        sendIntent.setType("text/plain");

        startActivityForResult(sendIntent, 1);


    }

    public void shareImage() {


        try {


            Glide.with(getContext())
                    .load(Constants.ImgURL + imgUrl)
                    .asBitmap().skipMemoryCache(true).diskCacheStrategy(DiskCacheStrategy.NONE)

                    .into(new SimpleTarget<Bitmap>(250, 250) {
                        @Override
                        public void onResourceReady(Bitmap resource, GlideAnimation glideAnimation) {


                            Intent intent = new Intent(Intent.ACTION_SEND);
                            intent.putExtra(Intent.EXTRA_TEXT, title + "  " + "\n" + shareUrl + " " + "\n" + getString(R.string.downloadGoogle) + "\n" + " https://play.google.com/store/apps/details?id=com.interteleco.kuwaitcalendar "
                                    + "\n" + getString(R.string.downloadApple) + "\n" + " https://apps.apple.com/us/app/kuwait-calendar/id1479037618?ls=1  ");
                            String path = MediaStore.Images.Media.insertImage(getActivity().getContentResolver(), resource, "", null);
                            //  Log.i("quoteswahttodo", "is onresoursereddy" + path);

                            Uri screenshotUri = Uri.parse(path);

                            // Log.i("quoteswahttodo", "is onresoursereddy" + screenshotUri);

                            intent.putExtra(Intent.EXTRA_STREAM, screenshotUri);
                            intent.setType("image/*");
                            startActivity(intent);
                        }

                        @Override
                        public void onLoadFailed(Exception e, Drawable errorDrawable) {
                            // Toast.makeText(getContext(), "Something went wrong", Toast.LENGTH_SHORT).show();

                            super.onLoadFailed(e, errorDrawable);
                        }

                        @Override
                        public void onLoadStarted(Drawable placeholder) {
                            //  Toast.makeText(getContext(), "Starting", Toast.LENGTH_SHORT).show();

                            super.onLoadStarted(placeholder);
                        }
                    });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void bindData() {
        txtTitle.setText(title);
        txtDescription.setText(Description);
        String dateFrom = startDate.substring(0, 10);
        String dateTo = endDate.substring(0, 10);
        if (startTime != null && endTime != null&&!startTime.equalsIgnoreCase("")&&!endTime.equalsIgnoreCase("")) {

            if (Global.getInstance(getActivity()).getFromSharedPreferneces("mylang").equalsIgnoreCase("en"))
                txtTime.setText(startTime +"-" + endTime);
            else
                txtTime.setText(startTime +" "+getString(R.string.to)+" "+ endTime);

        } else {

            timeLayout.setVisibility(View.GONE);

        }
        try {
            SimpleDateFormat spf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
            java.util.Date dateStart = spf.parse(startDate);
            java.util.Date dateEnd = spf.parse(endDate);
            spf = new SimpleDateFormat("dd MMM yyyy");
            startDate = spf.format(dateStart);
            endDate = spf.format(dateEnd);
            if (!dateFrom.equalsIgnoreCase(dateTo))
                txtDate.setText(startDate + "-" + endDate);
            else
                txtDate.setText(startDate);
        } catch (Exception e) {
            e.printStackTrace();
        }
        txtLocation.setText(location);

        if (imgUrl != null && getContext() != null)
            Glide.with(getContext()).load(Constants.ImgURL + imgUrl).placeholder(R.drawable.kwcalendarthum).into(imgEvent);
    }

    private void setFonts() {
        Fonts.set(new TextView[]{txtDescription, txtCalendar, txtLocation, tickets}, getActivity(), 0);
        Fonts.set(new TextView[]{txtTitle, label, labelTime, txtDate}, getActivity(), 1);
        Fonts.set(new AppCompatButton[]{ViewDirection}, getActivity(), 0);
        Fonts.set(new AppCompatButton[]{ViewDirection}, getActivity(), 1);
    }

    public void getEventData(String id) {
        progressBar.setVisibility(View.VISIBLE);
        HashMap<String, Object> body = new HashMap<>();
        //  String currentLanguage = Global.getInstance(getActivity()).getFromSharedPrefernec(Constants.currentLangauge);
        String token = Global.getInstance(getContext()).getFromSharedPreferneces(Constants.SH_Token);
        EventApi retrofitAPI = ServiceClient.createService(EventApi.class, getActivity());
        retrofitAPI.getEventdetails(id, token).enqueue(new Callback<EventModel>() {
            @Override
            public void onResponse(Call<EventModel> call, Response<EventModel> response) {
                if (response.code() == 200) {
                    progressBar.setVisibility(View.GONE);
                    // hideLoadingDialog(pDialog);
                    // count=response.body().getTotalCount();
                    EventModel event = response.body();
                    EventData eventData = response.body().getEvent();
                    lat = eventData.getLat();
                    longt = eventData.getLongt();
                    isCalendered = event.isCalender();
                    if (eventData.isLikes()) {
                        favourite.setImageResource(R.drawable.fav_active);
                        favourite.setTag("unlike");
                    } else {
                        favourite.setImageResource(R.drawable.fav_2);
                        favourite.setTag("like");
                    }
                    if (Global.getInstance(getContext()).getFromSharedPreferneces("mylang").equalsIgnoreCase("en")) {
                        title = eventData.getTitle_en();
                        Description = eventData.getDesc_en().toString();
                        location = eventData.getVenueId().getName_en();

                    } else {
                        title = eventData.getTitle_ar();
                        Description = eventData.getDesc_ar().toString();
                        location = eventData.getVenueId().getName_ar();

                    }

                    imgUrl = eventData.getImageURL();
                    startDate = eventData.getStartDate();
                    endDate = eventData.getEndDate();
                    eventStart = eventData.getStartDate();
                    eventEnd = eventData.getEndDate();
                    startTime = event.getOpen();
                    endTime = event.getClose();
                    shareUrl = Constants.SharedURL + "events/" + eventData.get_id();
                    bindData();

                } else {

                    progressBar.setVisibility(View.GONE);
                    Toast.makeText(getActivity(), getString(R.string.serverError), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<EventModel> call, Throwable throwable) {
                //  hideLoadingDialog(pDialog);
                progressBar.setVisibility(View.GONE);
                Toast.makeText(getActivity(), getString(R.string.serverError), Toast.LENGTH_LONG).show();


            }
        });

    }

    private void addToCalendar() {
        String dateFrom = eventStart.substring(0, 10);
        String dateTo = eventEnd.substring(0, 10);
        ContentResolver contentResolver = getActivity().getContentResolver();
        int startYear = Integer.parseInt(dateFrom.substring(0, 4));
        int startMonth = Integer.parseInt(dateFrom.substring(5, 7));
        int startDay = Integer.parseInt(dateFrom.substring(8, 10));
        int endYear = Integer.parseInt(dateTo.substring(0, 4));
        int endMonth = Integer.parseInt(dateTo.substring(5, 7));
        int endDay = Integer.parseInt(dateTo.substring(8, 10));

        ContentValues values = new ContentValues();
        Calendar beginTime = Calendar.getInstance();
        beginTime.set(startYear, startMonth - 1, startDay, Calendar.HOUR_OF_DAY, Calendar.MINUTE);

        long startMillis = 0;
        long endMillis = 0;

        startMillis = beginTime.getTimeInMillis();
        Calendar endTime = Calendar.getInstance();
        endTime.set(endYear, endMonth - 1, endDay, Calendar.HOUR_OF_DAY, Calendar.MINUTE);
        endMillis = endTime.getTimeInMillis();
        values.put(CalendarContract.Events.TITLE, title);
        values.put(CalendarContract.Events.DESCRIPTION, title);
        values.put(CalendarContract.Events.EVENT_LOCATION, location);
        values.put(CalendarContract.Events.DTSTART, startMillis);
        values.put(CalendarContract.Events.DTEND, endMillis);
        values.put(CalendarContract.Events.EVENT_TIMEZONE, Calendar.getInstance().getTimeZone().getID());
        values.put(CalendarContract.Events.CALENDAR_ID, 1);
        Uri uri = contentResolver.insert(CalendarContract.Events.CONTENT_URI, values);
        addCalendar(id);
        // Toast.makeText(getActivity(), getResources(). getString(R.string.Eventcalendared), Toast.LENGTH_LONG).show();


    }

    private void askpermissions() {


        if (Build.VERSION.SDK_INT < 23) {

            if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_CALENDAR) != PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_CALENDAR) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    Activity#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for Activity#requestPermissions for more details.
                ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.WRITE_CALENDAR, Manifest.permission.READ_CALENDAR}, 2);

            } else {


                addToCalendar();

            }


        } else {

            if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_CALENDAR) == PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_CALENDAR) == PackageManager.PERMISSION_GRANTED) {


                addToCalendar();


            } else {

                ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.WRITE_CALENDAR, Manifest.permission.READ_CALENDAR}, 2);

            }


        }
    }


    public void addCalendar(String ID) {

        String token = Global.getInstance(getActivity()).getFromSharedPreferneces(Constants.SH_Token);

        HashMap<String, Object> body = new HashMap<>();

        body.put("_id", ID);


        EventApi retrofitAPI = ServiceClient.createService(EventApi.class, getActivity());
        retrofitAPI.addCalendar(token, body).enqueue(new Callback<RESPONSE>() {
            @Override
            public void onResponse(Call<RESPONSE> call, Response<RESPONSE> response) {
                if (response.code() == 200) {
                    //    hideLoadingDialog(pDialog);
                    RESPONSE result = response.body();
                    Intent intent = new Intent(Global.getInstance(getContext()).BROADCAST_ACTION);
                    intent.putExtra("action", Constants.Calendar);
                    getContext().sendBroadcast(intent);


                    // Toast.makeText(getActivity(), "" + result.getSuccess(), Toast.LENGTH_LONG).show();
                    //  getChildFragmentManager().popBackStack();
                } else {
                    //      hideLoadingDialog(pDialog);
                    RESPONSE result = ResponseUtil.parseError(response);
                    Toast.makeText(getActivity(), result.getMessage(), Toast.LENGTH_LONG).show();

                }
            }

            @Override
            public void onFailure(Call<RESPONSE> call, Throwable throwable) {
                // hideLoadingDialog(pDialog);
                Toast.makeText(getActivity(), getString(R.string.errorServer), Toast.LENGTH_LONG).show();
            }
        });


    }

    public void likeEvent(String ID, String tag) {

        String token = Global.getInstance(getActivity()).getFromSharedPreferneces(Constants.SH_Token);

        HashMap<String, Object> body = new HashMap<>();

        body.put("_id", ID);

        // body.put("lang", currentLanguage.toLowerCase());

        // pDialog = showLoadingDialog(getActivity());

        EventApi retrofitAPI = ServiceClient.createService(EventApi.class, getActivity());
        retrofitAPI.likeEvent(token, body).enqueue(new Callback<RESPONSE>() {
            @Override
            public void onResponse(Call<RESPONSE> call, Response<RESPONSE> response) {
                if (response.code() == 200) {
                    //    hideLoadingDialog(pDialog);
                    RESPONSE result = response.body();
                    if (tag.equalsIgnoreCase("like")) {
                        favourite.setImageResource(R.drawable.fav_active);
                        favourite.setTag("unlike");
                        //     notifyDataSetChanged();

                    } else {

                        favourite.setImageResource(R.drawable.fav_2);
                        favourite.setTag("like");
                        //   notifyDataSetChanged();
                    }
                    Intent intent = new Intent(Global.getInstance(getContext()).BROADCAST_ACTION);
                    intent.putExtra("action", Constants.LikeToggle);
                    getContext().sendBroadcast(intent);
                    //  getChildFragmentManager().popBackStack();
                } else {
                    APIError error = ErrorUtils.parseError(response);
                    Toast.makeText(getActivity(), error.getMessage(), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<RESPONSE> call, Throwable throwable) {
                // hideLoadingDialog(pDialog);
                Toast.makeText(getActivity(), getString(R.string.errorServer), Toast.LENGTH_LONG).show();
            }
        });

    }

    public void shareEvent(String ID) {

        String token = Global.getInstance(getActivity()).getFromSharedPreferneces(Constants.SH_Token);

        HashMap<String, Object> body = new HashMap<>();

        body.put("_id", ID);

        // body.put("lang", currentLanguage.toLowerCase());

        // pDialog = showLoadingDialog(getActivity());

        EventApi retrofitAPI = ServiceClient.createService(EventApi.class, getActivity());
        retrofitAPI.shareEvent(token, body).enqueue(new Callback<RESPONSE>() {
            @Override
            public void onResponse(Call<RESPONSE> call, Response<RESPONSE> response) {
                if (response.code() == 200) {
                    //    hideLoadingDialog(pDialog);
                    RESPONSE result = response.body();
                    //   String message = response.body().getMessage();
                    //   System.out.print(message);
                    //   data.remove(selectedindex);
                    //  notifyDataSetChanged();


                    //Toast.makeText(getActivity(), "" + result.getSuccess(), Toast.LENGTH_LONG).show();
                    //  getChildFragmentManager().popBackStack();
                } else {
                    //      hideLoadingDialog(pDialog);
                    RESPONSE result = ResponseUtil.parseError(response);
                    Toast.makeText(getActivity(), result.getMessage(), Toast.LENGTH_LONG).show();

                }
            }

            @Override
            public void onFailure(Call<RESPONSE> call, Throwable throwable) {
                // hideLoadingDialog(pDialog);
                Toast.makeText(getActivity(), getActivity().getString(R.string.errorServer), Toast.LENGTH_LONG).show();
            }
        });


    }

    private void askpermissionsToShare() {


        if (Build.VERSION.SDK_INT < 23) {

            if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    Activity#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for Activity#requestPermissions for more details.
                ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, 3);

            } else {

                shareImage();

            }


        } else {

            if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {

                shareImage();


            } else {

                ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, 3);

            }

        }
    }

}
