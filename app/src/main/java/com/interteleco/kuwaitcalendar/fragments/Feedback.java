package com.interteleco.kuwaitcalendar.fragments;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.fragment.app.Fragment;

import com.interteleco.kuwaitcalendar.R;
import com.interteleco.kuwaitcalendar.activities.Home;
import com.interteleco.kuwaitcalendar.activities.MainActivity;
import com.interteleco.kuwaitcalendar.server.ServiceClient;
import com.interteleco.kuwaitcalendar.server.UserApi;
import com.interteleco.kuwaitcalendar.server.models.APIError;
import com.interteleco.kuwaitcalendar.server.models.RESPONSE;
import com.interteleco.kuwaitcalendar.utils.Constants;
import com.interteleco.kuwaitcalendar.utils.ErrorUtils;
import com.interteleco.kuwaitcalendar.utils.Fonts;
import com.interteleco.kuwaitcalendar.utils.Global;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Feedback extends Fragment {


    @BindView(R.id.edtSubject)
    AppCompatEditText subject;
    @BindView(R.id.edtComment)
    AppCompatEditText comment;

    @BindView(R.id.txtErrorSubject)
    TextView txtErrorSubject;
    @BindView(R.id.txtErrorComments)
    TextView txtErrorComments;

    AppCompatButton sendButton;


    ProgressBar progressBar;
    @BindView(R.id.menuBtn)
    ImageView backBtn;

    @BindView(R.id.toolbar_title)
    TextView toolbar_title;
    String authorized;

    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.feedback, container, false);
        ButterKnife.bind(this, view);
        ((Home) getActivity()).customizeToolbar(backBtn, getResources().getDrawable(R.drawable.arrow), getResources().getDrawable(R.drawable.arrow_r));
        progressBar = (ProgressBar) view.findViewById(R.id.progressBar);

        authorized = Global.getInstance(getContext()).getFromSharedPreferneces(Constants.Authorized);
        sendButton = (AppCompatButton) view.findViewById(R.id.send);

        setFonts();
        // Fonts.set(new TextView[]{(TextView) toolbar.getChildAt(0)}, getActivity(), 0);
        sendButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

             //   if (authorized != null && !authorized.equalsIgnoreCase(""))
                    sendYourFeedback();
              //  else
                //    Toast.makeText(getContext(), getContext().getString(R.string.loginFirst), Toast.LENGTH_LONG).show();

            }
        });


        subject.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                txtErrorSubject.setVisibility(View.GONE);
                //   email.setBackground(getResources().getDrawable(R.drawable.normal_bg));
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        comment.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                txtErrorComments.setVisibility(View.GONE);
                //   email.setBackground(getResources().getDrawable(R.drawable.normal_bg));
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });


        return view;

    }

    private void setFonts() {

        Fonts.set(new TextView[]{toolbar_title}, getActivity(), 1);
        Fonts.set(new EditText[]{subject, comment}, getActivity(), 0);
        Fonts.set(new TextView[]{txtErrorComments, txtErrorSubject}, getActivity(), 0);

        Fonts.set(new AppCompatButton[]{sendButton}, getActivity(), 0);
    }

    @OnClick({R.id.menuBtn})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.menuBtn: {
                getFragmentManager().popBackStack();

            }

            break;
        }
    }


    public void sendYourFeedback() {
        if (subject.getText().length() > 0 && !subject.getText().toString().trim().equalsIgnoreCase("")) {
            if (comment.getText().length() > 0 && !comment.getText().toString().trim().equalsIgnoreCase("")) {


                ((Home) getActivity()).hideKeyBoard();
                // String currentLanguage = Global.getInstance(getActivity()).getFromSharedPrefernec(Constants.currentLangauge);

                sendFeedback();


            } else {
                comment.requestFocus();
                txtErrorComments.setVisibility(View.VISIBLE);

                //  email.setBackground(getResources().getDrawable(R.drawable.error_bg));
            }
        } else {
            subject.requestFocus();
            txtErrorSubject.setVisibility(View.VISIBLE);

        }


    }


    private void sendFeedback() {
        HashMap<String, Object> body = new HashMap<>();

        body.put("subject", subject.getText().toString());
        body.put("desc", comment.getText().toString());
        String token = Global.getInstance(getContext()).getFromSharedPreferneces(Constants.SH_Token);
        progressBar.setVisibility(View.VISIBLE);
        UserApi retrofitAPI = ServiceClient.createService(UserApi.class, getActivity());
        retrofitAPI.sendFeedback(token, body).enqueue(new Callback<RESPONSE>() {
            @Override
            public void onResponse(Call<RESPONSE> call, Response<RESPONSE> response) {
                if (response.code() == 201) {
                    progressBar.setVisibility(View.GONE);
                    Toast.makeText(getActivity(), getString(R.string.thank), Toast.LENGTH_LONG).show();
                    getFragmentManager().popBackStack();
                } else {
                    try {

                        APIError error = ErrorUtils.parseError(response);
                        Toast.makeText(getActivity(), error.getErrors().get(0).getMessage(), Toast.LENGTH_LONG).show();
                        progressBar.setVisibility(View.GONE);
                    } catch (Exception e) {
                        e.printStackTrace();

                    }


                }
            }

            @Override
            public void onFailure(Call<RESPONSE> call, Throwable throwable) {
                progressBar.setVisibility(View.GONE);
                Toast.makeText(getActivity(), getString(R.string.errorServer), Toast.LENGTH_LONG).show();
            }
        });
    }


}
