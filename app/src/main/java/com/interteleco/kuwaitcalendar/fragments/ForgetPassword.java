package com.interteleco.kuwaitcalendar.fragments;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;

import com.interteleco.kuwaitcalendar.R;

import com.interteleco.kuwaitcalendar.activities.Home;
import com.interteleco.kuwaitcalendar.activities.MainActivity;
import com.interteleco.kuwaitcalendar.dialogs.CategoryList;
import com.interteleco.kuwaitcalendar.server.ServiceClient;
import com.interteleco.kuwaitcalendar.server.UserApi;
import com.interteleco.kuwaitcalendar.server.models.APIError;
import com.interteleco.kuwaitcalendar.server.models.RESPONSE;
import com.interteleco.kuwaitcalendar.utils.ErrorUtils;
import com.interteleco.kuwaitcalendar.utils.Fonts;

import com.interteleco.kuwaitcalendar.utils.Validator;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ForgetPassword extends Fragment {

    AppCompatEditText email;
    TextView errorMessage;
    AppCompatButton sendButton;

    @BindView(R.id.txt1)
    TextView txt1;
    @BindView(R.id.txt2)
    TextView txt2;
    ProgressBar progressBar;
    @BindView(R.id.menuBtn)
    ImageView backBtn;

    @BindView(R.id.toolbar_title)
    TextView toolbar_title;

    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.forget_pass, container, false);
        ButterKnife.bind(this, view);
        ((MainActivity) getActivity()).customizeToolbar(backBtn, getResources().getDrawable(R.drawable.arrow), getResources().getDrawable(R.drawable.arrow_r));
        progressBar = (ProgressBar) view.findViewById(R.id.progressBar);
        // ChasingDots dots = new ChasingDots();
        //progressBar.setIndeterminateDrawable(dots);
        email = (AppCompatEditText) view.findViewById(R.id.edtEmail);

        errorMessage = (TextView) view.findViewById(R.id.txtErrorEmail);

        sendButton = (AppCompatButton) view.findViewById(R.id.send);

        setFonts();
        // Fonts.set(new TextView[]{(TextView) toolbar.getChildAt(0)}, getActivity(), 0);
        sendButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                ((MainActivity) getActivity()).hideKeyBoard();
                //    Toast.makeText(getActivity(), "Button", Toast.LENGTH_SHORT).show();
                ResetPassword();
            }
        });


        email.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                errorMessage.setVisibility(View.GONE);
                //   email.setBackground(getResources().getDrawable(R.drawable.normal_bg));
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        return view;

    }

    private void setFonts() {

        Fonts.set(new TextView[]{toolbar_title}, getActivity(), 1);
        Fonts.set(new EditText[]{email}, getActivity(), 0);
        Fonts.set(new TextView[]{errorMessage, txt1, txt2}, getActivity(), 0);

        Fonts.set(new AppCompatButton[]{sendButton}, getActivity(), 0);
    }

    @OnClick({R.id.menuBtn})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.menuBtn: {
                getFragmentManager().popBackStack();

            }

            break;
        }
    }


    public void ResetPassword() {
        if (email.getText().length() > 0 && !email.getText().toString().trim().equalsIgnoreCase("")) {
            if (Validator.isValidEMail(email.getText().toString())) {


                ((MainActivity) getActivity()).hideKeyBoard();
                // String currentLanguage = Global.getInstance(getActivity()).getFromSharedPrefernec(Constants.currentLangauge);

                resetPassword();


            } else {
                email.requestFocus();
                errorMessage.setVisibility(View.VISIBLE);
                errorMessage.setText(getString(R.string.entervalidEmail));
                //  email.setBackground(getResources().getDrawable(R.drawable.error_bg));
            }
        } else {
            email.requestFocus();
            errorMessage.setVisibility(View.VISIBLE);
            //  email.setBackground(getResources().getDrawable(R.drawable.error_bg));
        }


    }


    private void resetPassword() {
        HashMap<String, Object> body = new HashMap<>();

        body.put("email", email.getText().toString());
        progressBar.setVisibility(View.VISIBLE);
        UserApi retrofitAPI = ServiceClient.createService(UserApi.class, getActivity());
        retrofitAPI.forgetPassword(body).enqueue(new Callback<RESPONSE>() {
            @Override
            public void onResponse(Call<RESPONSE> call, Response<RESPONSE> response) {
                if (response.code() == 200) {
                    progressBar.setVisibility(View.GONE);
                    RESPONSE result = response.body();
                    result.getMessage();
                    Toast.makeText(getActivity(), result.getMessage(), Toast.LENGTH_LONG).show();
                    ForgetPasswordConfirm forgetPasswordConfirm = new ForgetPasswordConfirm();
                    ((MainActivity) getActivity()).addFragament(forgetPasswordConfirm);

                } else {
                    progressBar.setVisibility(View.GONE);
                    try {

                        APIError error = ErrorUtils.parseError(response);
                        Toast.makeText(getActivity(), error.getMessage(), Toast.LENGTH_LONG).show();
                        progressBar.setVisibility(View.GONE);
                    } catch (Exception e) {
                        e.printStackTrace();

                    }


                }
            }

            @Override
            public void onFailure(Call<RESPONSE> call, Throwable throwable) {
                progressBar.setVisibility(View.GONE);
                Toast.makeText(getActivity(), getString(R.string.errorServer), Toast.LENGTH_LONG).show();
            }
        });
    }


}
