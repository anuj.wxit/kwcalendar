package com.interteleco.kuwaitcalendar.fragments;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;

import com.interteleco.kuwaitcalendar.R;
import com.interteleco.kuwaitcalendar.activities.Home;

import com.interteleco.kuwaitcalendar.activities.MainActivity;
import com.interteleco.kuwaitcalendar.server.ServiceClient;
import com.interteleco.kuwaitcalendar.server.UserApi;
import com.interteleco.kuwaitcalendar.server.models.APIError;
import com.interteleco.kuwaitcalendar.server.models.RESPONSE;
import com.interteleco.kuwaitcalendar.utils.ErrorUtils;
import com.interteleco.kuwaitcalendar.utils.Fonts;
import com.interteleco.kuwaitcalendar.utils.Validator;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ForgetPasswordConfirm extends Fragment {


    AppCompatEditText email, newpass, confirmPass;

    AppCompatButton sendButton;

    @BindView(R.id.txtErrorPass)
    TextView txtErrorPass;

    @BindView(R.id.txtErrorconfirm)
    TextView txtErrorconfirm;

    @BindView(R.id.txtErrorEmail)
    TextView txtErrorEmail;
    @BindView(R.id.menuBtn)
    ImageView backBtn;

    @BindView(R.id.toolbar_title)
    TextView toolbar_title;
    ProgressBar progressBar;

    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.confirm_forget, container, false);
        ButterKnife.bind(this, view);
        progressBar = (ProgressBar) view.findViewById(R.id.progressBar);


        email = (AppCompatEditText) view.findViewById(R.id.edtEmail);
        newpass = (AppCompatEditText) view.findViewById(R.id.password);
        confirmPass = (AppCompatEditText) view.findViewById(R.id.confirmPassword);

        sendButton = (AppCompatButton) view.findViewById(R.id.save);
        ((MainActivity) getActivity()).customizeToolbar(backBtn, getResources().getDrawable(R.drawable.arrow), getResources().getDrawable(R.drawable.arrow_r));
        //  Fonts.set(new TextView[]{(TextView) toolbar.getChildAt(0)}, getActivity(), 0);
        setFonts();

        sendButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                //  ((HomeActivity) getActivity()).hideKeyBoard();
                ChangePassword();
            }
        });


        newpass.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                txtErrorPass.setVisibility(View.GONE);

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        confirmPass.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                txtErrorconfirm.setVisibility(View.GONE);

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        email.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                txtErrorEmail.setVisibility(View.GONE);
                //   confirmPass.setBackground(getResources().getDrawable(R.drawable.normal_bg));
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        return view;

    }

    @OnClick({R.id.menuBtn})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.menuBtn: {
                getFragmentManager().popBackStack();

            }

            break;
        }
    }


//(Global.getInstance(getActivity()).getFromSharedPrefernec(Constants.SH_PASSWORD)

    private void setFonts() {
        Fonts.set(new TextView[]{toolbar_title}, getActivity(), 1);
        Fonts.set(new EditText[]{confirmPass, newpass, email}, getActivity(), 0);
        Fonts.set(new TextView[]{txtErrorPass, txtErrorEmail, txtErrorconfirm}, getActivity(), 0);
        Fonts.set(new AppCompatButton[]{sendButton}, getActivity(), 0);
    }

    public void ChangePassword() {

        if (email.getText().length() > 0 && !email.getText().toString().trim().equalsIgnoreCase("")) {
            if (newpass.getText().length() > 0 && !newpass.getText().toString().trim().equalsIgnoreCase("")) {
                if (Validator.ValidatePassword(newpass.getText().toString())) {
                    if (confirmPass.getText().length() > 0 && !confirmPass.getText().toString().equalsIgnoreCase("")) {
                        if (confirmPass.getText().toString().equalsIgnoreCase(newpass.getText().toString())) {
                            ((MainActivity) getActivity()).hideKeyBoard();
                            //  String currentLanguage = Global.getInstance(getActivity()).getFromSharedPrefernec(Constants.currentLangauge);

                            changePassword();
                        } else {
                            confirmPass.requestFocus();
                            txtErrorconfirm.setVisibility(View.VISIBLE);
                            txtErrorconfirm.setText(getString(R.string.passwordsNotMatched));

                        }
                    } else {
                        confirmPass.requestFocus();
                        txtErrorconfirm.setVisibility(View.VISIBLE);

                    }

                } else {
                    newpass.requestFocus();
                    txtErrorPass.setText(getString(R.string.passwordCharacters));
                    txtErrorPass.setVisibility(View.VISIBLE);


                }
            } else {
                newpass.requestFocus();
                //   txtErrorPass.setText(getString(R.string.passwordCharacters));
                txtErrorPass.setVisibility(View.VISIBLE);

            }


        } else {
            email.requestFocus();
            txtErrorEmail.setVisibility(View.VISIBLE);
            txtErrorEmail.setText(getString(R.string.enterCode));

        }
    }


    private void changePassword() {
        HashMap<String, Object> body = new HashMap<>();
        //  body.put("lang", currentLanguage.toLowerCase());
        body.put("code", email.getText().toString());
        body.put("password", newpass.getText().toString());
        body.put("confirmPassword", confirmPass.getText().toString());

        progressBar.setVisibility(View.VISIBLE);
        UserApi retrofitAPI = ServiceClient.createService(UserApi.class, getActivity());
        retrofitAPI.resetPassword(body).enqueue(new Callback<RESPONSE>() {
            @Override
            public void onResponse(Call<RESPONSE> call, Response<RESPONSE> response) {

                if (response.code() == 200) {
                    progressBar.setVisibility(View.GONE);
                    RESPONSE result = response.body();

                    Toast.makeText(getActivity(), result.getMessage(), Toast.LENGTH_LONG).show();
                    getFragmentManager().popBackStack();
                    getFragmentManager().popBackStack();

                } else {
                    progressBar.setVisibility(View.GONE);
                    APIError error = ErrorUtils.parseError(response);
                    Toast.makeText(getActivity(), error.getMessage(), Toast.LENGTH_LONG).show();
                }


            }

            @Override
            public void onFailure(Call<RESPONSE> call, Throwable throwable) {

                progressBar.setVisibility(View.GONE);
                Toast.makeText(getActivity(), getString(R.string.errorServer), Toast.LENGTH_LONG).show();
            }
        });


    }

}
