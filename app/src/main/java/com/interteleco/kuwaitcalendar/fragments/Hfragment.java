package com.interteleco.kuwaitcalendar.fragments;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.AbsListView;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.viewpager.widget.ViewPager;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseApp;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.interteleco.kuwaitcalendar.R;
import com.interteleco.kuwaitcalendar.activities.Home;

import com.interteleco.kuwaitcalendar.activities.MainActivity;
import com.interteleco.kuwaitcalendar.adapters.CategoryAdapter;
import com.interteleco.kuwaitcalendar.adapters.EventsAdapter;
import com.interteleco.kuwaitcalendar.adapters.SliderAdapter;
import com.interteleco.kuwaitcalendar.server.EventApi;
import com.interteleco.kuwaitcalendar.server.ServiceClient;
import com.interteleco.kuwaitcalendar.server.UserApi;
import com.interteleco.kuwaitcalendar.server.models.CategoryData;
import com.interteleco.kuwaitcalendar.server.models.EventData;
;
import com.interteleco.kuwaitcalendar.server.models.EventsResponse;
import com.interteleco.kuwaitcalendar.server.models.RESPONSE;
import com.interteleco.kuwaitcalendar.server.models.ResponseData;
import com.interteleco.kuwaitcalendar.server.models.WeekEvents;
import com.interteleco.kuwaitcalendar.utils.Constants;
import com.interteleco.kuwaitcalendar.utils.Fonts;
import com.interteleco.kuwaitcalendar.utils.Global;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Timer;
import java.util.TimerTask;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import me.relex.circleindicator.CircleIndicator;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Hfragment extends Fragment {
    @BindView(R.id.title_recom)
    TextView txtRecom;
    @BindView(R.id.title)
    TextView txtWeek;
    @BindView(R.id.viewbutton)
    TextView txtView;


    @BindView(R.id.viewRbutton)
    TextView viewRbutton;

    @BindView(R.id.addEvent)
    TextView addEvent;

    ProgressBar progressBar;
    @BindView(R.id.swipe_refresh_layout)
    SwipeRefreshLayout swipeRefreshLayout;

    EventsAdapter eventsAdapter;
    public ArrayList<EventData> sliderEvents, Events, weekEvents, recEvents;

    public ArrayList<CategoryData.Category> categories = new ArrayList<>();
    public String categoryId = "all";
    public int rpage = 1;
    public int wpage = 1;

    private boolean isReceiverRegistered;
    int pastVisiblesItems, visibleItemCount, totalItemCount;
    int pastVisiblesItemsW, visibleItemCountW, totalItemCountW;
    LinearLayoutManager linearLayoutManager, linearLayoutManager_r, linearLayoutManager_w;

    RecyclerView lstWEvents, lstREvents, tabs;
    ViewPager viewPager;
    SliderAdapter adapter;
    CircleIndicator indicator;
    Handler handler;
    Runnable runnable;
    Timer timer;
    //ViewFlipper viewFlipper;
    int i, rCount, wCount;
    TextView noSContent, noWcontent, noRContent;
    private boolean userScrolledW = true;
    private boolean userScrolledR = true;
    CategoryAdapter categoryAdapter;
    View view;
    ArrayList<AppCompatButton> butons = new ArrayList<>();
    DrawerLayout drawer;
    String authorized;
    AlertDialog alertDialog;
    AlertDialog.Builder builder;

    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        //FirebaseApp.initializeApp(getContext());
        view = inflater.inflate(R.layout.home, container, false);
        ButterKnife.bind(this, view);
      authorized = Global.getInstance(getContext()).getFromSharedPreferneces(Constants.Authorized);

            addEvent.setVisibility(View.VISIBLE);

        sliderEvents = new ArrayList<>();
        Events = new ArrayList<>();
        recEvents = new ArrayList<>();
        weekEvents = new ArrayList<>();
        AppCompatActivity activity = (AppCompatActivity) getActivity();
        drawer = (DrawerLayout) activity.findViewById(R.id.drawer_layout);

        lstWEvents = (RecyclerView) view.findViewById(R.id.lstWeekevents);
        lstREvents = (RecyclerView) view.findViewById(R.id.lstRecEvents);
        tabs = (RecyclerView) view.findViewById(R.id.tabs);
        Toolbar toolbar = (Toolbar) view.findViewById(R.id.toolbar);
        noRContent = (TextView) view.findViewById(R.id.noRContent);
        noWcontent = (TextView) view.findViewById(R.id.noContent);
        noSContent = (TextView) view.findViewById(R.id.noSContent);
        progressBar = (ProgressBar) view.findViewById(R.id.progressBar);
        viewPager = (ViewPager) view.findViewById(R.id.viewPager_id);
        indicator = (CircleIndicator) view.findViewById(R.id.circleIndicator_id);
        linearLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        linearLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        tabs.setHasFixedSize(true);
        tabs.setLayoutManager(linearLayoutManager);

        setFonts();


        // LinearLayout gallery = (LinearLayout) view.findViewById(R.id.gallery);
        // LayoutInflater layoutInflater = LayoutInflater.from(getActivity());//
//        for (i = 0; i < categories.size(); i++) {
//            View mView = layoutInflater.inflate(R.layout.tab_view, gallery, false);
//            AppCompatButton button = (AppCompatButton) mView.findViewById(R.id.tab);
//             button.setText(categories.get(i).getName_en());
//             btnIndex=i;
//            if (i!= 0) {
//                categoryId = categories.get(i).get_id();
//            }
//            butons.add(button);
//            gallery.addView(mView);
//        }
        // String authorized = Global.getInstance(getActivity()).getFromSharedPreferneces(Constants.Authorized);
        if (authorized != null && !authorized.equalsIgnoreCase("")) {
            String devicetoken = Global.getInstance(getActivity()).getFromSharedPreferneces(Constants.SH_DeviceToken);
            if (devicetoken == null) {
                FirebaseInstanceId.getInstance().getInstanceId()
                        .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                            @Override
                            public void onComplete(@NonNull Task<InstanceIdResult> task) {
                                if (!task.isSuccessful()) {
                                    //   Log.w(TAG, "getInstanceId failed", task.getException());
                                    return;
                                }

                                // Get new Instance ID token
                                String token = task.getResult().getToken();
                                Global.getInstance(getActivity()).saveInSharedPrefernces(Constants.SH_DeviceToken, token);
                                sendTokenToServer(token);
                                // Log and toast
                                // String msg = getString(R.string.msg_token_fmt, token);
                                //  Log.d(TAG, msg);
                                //  Toast.makeText(getActivity(),token, Toast.LENGTH_SHORT).show();
                            }
                        });
            } else {
                FirebaseInstanceId.getInstance().getInstanceId()
                        .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                            @Override
                            public void onComplete(@NonNull Task<InstanceIdResult> task) {
                                if (!task.isSuccessful()) {
                                    //   Log.w(TAG, "getInstanceId failed", task.getException());
                                    return;
                                }

                                // Get new Instance ID token
                                String token = task.getResult().getToken();
                                if (token.equalsIgnoreCase(Global.getInstance(getActivity()).getFromSharedPreferneces(Constants.SH_DeviceToken))) {

                                } else {

                                    sendNewTokenToServer(token, Global.getInstance(getActivity()).getFromSharedPreferneces(Constants.SH_DeviceToken));


                                }


                                // Log and toast
                                // String msg = getString(R.string.msg_token_fmt, token);
                                //  Log.d(TAG, msg);
                                //  Toast.makeText(getActivity(),token, Toast.LENGTH_SHORT).show();
                            }
                        });


            }
        }
        activity.setSupportActionBar(toolbar);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(getActivity(), drawer, toolbar, R.string.open_drawer, R.string.close_drawer) {
            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
//                //  if (drawerView.getId() == R.id.right_drawer) slideOffset = -1;
//                super.onDrawerSlide(drawerView, slideOffset);
//                FrameLayout frame = (FrameLayout) activity.findViewById(R.id.container);
//                NavigationView navigationView = (NavigationView) activity.findViewById(R.id.navigation_view);
//                if (drawer.isDrawerOpen(GravityCompat.END)) {
//                 slideOffset=-1;
//                }
//                if (Global.getInstance(getActivity()).getFromSharedPrefernec("mylang").equalsIgnoreCase("en"))
//                    frame.setTranslationX(drawerView.getWidth() * slideOffset);
//                else {
//                   // navigationView.setTranslationX(drawerView.getWidth() * (1 - slideOffset));
//                    frame.setTranslationX(drawerView.getWidth() * -1);
//
//                    if (drawer.isDrawerOpen(GravityCompat.END))
//                   drawer.closeDrawer(GravityCompat.END);
//                    super.onDrawerSlide(drawerView, slideOffset);
//
//
//                }
            }

        };


        drawer.setDrawerListener(toggle);
        toggle.syncState();


        getCategories();

        int Permission_All = 1;

        String[] Permissions = {Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.WRITE_CALENDAR,Manifest.permission.READ_CALENDAR};
        if(!hasPermissions(getContext(), Permissions)){
            ActivityCompat.requestPermissions(getActivity(), Permissions, Permission_All);
        }


//        ((Home)getActivity()).askpermissionsToShare();
//
//        ((Home)getActivity()).askpermissionsCalendar();
//        ((Home)getActivity()).askpermissionsLocation();
       return view;
    }

    public String setCategoryId(String id) {
        categoryId = id;
        return categoryId;
    }

    @OnClick({R.id.viewRbutton, R.id.viewbutton, R.id.addEvent})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.viewbutton: {
                AllEventsFragment allEventsFragment = new AllEventsFragment();
                allEventsFragment.Tag = "week";
                allEventsFragment.id = categoryId;
                ((Home) getActivity()).addFragament(allEventsFragment);

            }
            break;
            case R.id.viewRbutton: {
                AllEventsFragment allEventsFragment = new AllEventsFragment();
                allEventsFragment.Tag = "recomend";
                allEventsFragment.id = categoryId;
                ((Home) getActivity()).addFragament(allEventsFragment);

            }
            break;
            case R.id.addEvent: {

                if (authorized != null && !authorized.equalsIgnoreCase("")) {
                    AddEvent addEvent = new AddEvent();

                    ((Home) getActivity()).addFragament(addEvent);
                }else{
                //    Toast.makeText(getActivity(),getString(R.string.loginFirst), Toast.LENGTH_LONG).show();
               showAlert();
                }
            }
            break;
        }
    }


    private void setFonts() {
        Fonts.set(new TextView[]{viewRbutton, txtView}, getActivity(), 0);
        Fonts.set(new TextView[]{txtRecom, txtWeek, noRContent, noSContent, noWcontent}, getActivity(), 1);
    }

    private void loadCategories() {
        categoryAdapter = new CategoryAdapter(getContext(), categories, this);
        categoryAdapter.hfragment = this;
        tabs.setAdapter(categoryAdapter);


    }

    public void bindList() {
        linearLayoutManager_w = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        lstWEvents.setHasFixedSize(true);
        lstWEvents.setLayoutManager(linearLayoutManager_w);
        eventsAdapter = new EventsAdapter(getActivity(), weekEvents);
        lstWEvents.setAdapter(eventsAdapter);
        linearLayoutManager_r = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        lstREvents.setHasFixedSize(true);
        lstREvents.setLayoutManager(linearLayoutManager_r);
        eventsAdapter = new EventsAdapter(getActivity(), recEvents);
        lstREvents.setAdapter(eventsAdapter);
        adapter = new SliderAdapter(getContext(), sliderEvents);
        viewPager.setAdapter(adapter);
        indicator.setViewPager(viewPager);
        // indicator.setBackgroundColor(getResources().getColor(R.color.gray));
        handler = new Handler();
        runnable = new Runnable() {
            @Override
            public void run() {

                int i = viewPager.getCurrentItem();

                if (i == adapter.data.size() - 1) {

                    i = 0;
                    viewPager.setCurrentItem(i, true);

                } else {

                    i++;
                    viewPager.setCurrentItem(i, true);
                }
            }
        };
        if (timer != null) {
            timer.cancel();
            timer = new Timer();

        } else {
            timer = new Timer();
        }
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                handler.post(runnable);
            }
        }, 5000, 5000);
        lstREvents.addOnScrollListener(new RecyclerView.OnScrollListener() {

            @Override
            public void onScrollStateChanged(RecyclerView recyclerView,
                                             int newState) {

                super.onScrollStateChanged(recyclerView, newState);

                // If scroll state is touch scroll then set userScrolled
                // true
                if (newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL) {
                    userScrolledR = true;

                }

            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx,
                                   int dy) {


                if (dx > 0) {
                    visibleItemCount = linearLayoutManager_r.getChildCount();
                    totalItemCount = linearLayoutManager_r.getItemCount();
                    pastVisiblesItems = linearLayoutManager_r
                            .findFirstVisibleItemPosition();

                    // Now check if userScrolled is true and also check if
                    // the item is end then update recycler view and set
                    // userScrolled to false
                    if (userScrolledR
                            && (visibleItemCount + pastVisiblesItems) == totalItemCount) {
                        userScrolledR = false;

                        //  Log.d("TotalPages", TotalPages + "");
                        if (rCount > recEvents.size()) {
                            rpage = rpage + 1;
                            getRecommended(categoryId, 10, rpage);
                            //      Toast.makeText(getActivity(), "recomend" + rpage, Toast.LENGTH_LONG).show();
                        }


                    }

                }
            }

        });
        lstWEvents.addOnScrollListener(new RecyclerView.OnScrollListener() {

            @Override
            public void onScrollStateChanged(RecyclerView recyclerView,
                                             int newState) {

                super.onScrollStateChanged(recyclerView, newState);

                // If scroll state is touch scroll then set userScrolled
                // true
                if (newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL) {
                    userScrolledW = true;

                }

            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx,
                                   int dy) {


                if (dx > 0) {
                    visibleItemCountW = linearLayoutManager_w.getChildCount();
                    totalItemCountW = linearLayoutManager_w.getItemCount();
                    pastVisiblesItemsW = linearLayoutManager_w
                            .findFirstVisibleItemPosition();

                    // Now check if userScrolled is true and also check if
                    // the item is end then update recycler view and set
                    // userScrolled to false
                    if (userScrolledW
                            && (visibleItemCountW + pastVisiblesItemsW) == totalItemCountW) {
                        userScrolledW = false;

                        //  Log.d("TotalPages", TotalPages + "");
                        if (wCount > weekEvents.size()) {
                            wpage = wpage + 1;
                            getWeekEvents(categoryId, 10, wpage);
                            // Toast.makeText(getActivity(), "wewk" + wpage, Toast.LENGTH_SHORT).show();
                        }


                    }

                }
            }

        });

//   int []  images={R.drawable.logo,R.drawable.logo,R.drawable.logo};
//    for (int i=0;i<images.length;i++){
//        flipImage(images[i]);
//
//    }
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                swipeRefreshLayout.setRefreshing(false);
                //    eventsAdapter.notifyDataSetChanged();

                if (categories.size() < 1) {
                    //categories.clear();
                    getCategories();


                }
                LoadCategoryEvents(categoryId, 10, 1);
                // eventsAdapter.notifyDataSetChanged();
            }
        });
    }


    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            updateAction(intent);


        }
    };

    private void updateAction(Intent intent) {
        String action = intent.getStringExtra("action");

        if (action.contains(Constants.LikeToggle)) {
            LoadCategoryEvents(categoryId, 10, 1);
        }

    }

    @Override
    public void onResume() {
        super.onResume();

        IntentFilter intentFilter = new IntentFilter(Global.getInstance(getActivity()).BROADCAST_ACTION);

        getContext().registerReceiver(broadcastReceiver, intentFilter);


    }

    @Override
    public void onDestroy() {

        super.onDestroy();
        getContext().unregisterReceiver(broadcastReceiver);
    }


    public void LoadEvents(String category, int size, int page) {
//        HashMap<String, Object> body = new HashMap<>();
        //  if (isFirstTimeAppear)
        //   pDialog = showLoadingDialog(getActivity());

        // String currentLanguage = Global.getInstance(getActivity()).getFromSharedPrefernec(Constants.currentLangauge);

        progressBar.setVisibility(View.VISIBLE);
        String token = Global.getInstance(getContext()).getFromSharedPreferneces(Constants.SH_Token);
        EventApi retrofitAPI = ServiceClient.createService(EventApi.class, getActivity());
        retrofitAPI.getEvents(token, category, size, page).enqueue(new Callback<EventsResponse>() {
            @Override
            public void onResponse(Call<EventsResponse> call, Response<EventsResponse> response) {
                if (response.code() == 200) {
                    if (swipeRefreshLayout != null)
                        swipeRefreshLayout.setRefreshing(false);
                    progressBar.setVisibility(View.GONE);

                    EventsResponse eventsResponse = response.body();
                    ResponseData responseData = eventsResponse.getData();
                    sliderEvents = responseData.getSlider();
                    weekEvents = responseData.getThisWeek().getData();
                    wCount = responseData.getThisWeek().getTotal();
                    rCount = responseData.getRecommended().getTotal();
                    for (int i = 0; i < weekEvents.size(); i++) {
                        weekEvents.get(i).setType("week");
                    }
                    recEvents = responseData.getRecommended().getData();
                    for (int i = 0; i < recEvents.size(); i++) {
                        recEvents.get(i).setType("recom");
                    }
                    if (sliderEvents.size() < 1)
                        noSContent.setVisibility(View.VISIBLE);
                    else
                        noSContent.setVisibility(View.GONE);
                    if (weekEvents.size() < 1) {
                        noWcontent.setVisibility(View.VISIBLE);
                        txtView.setVisibility(View.GONE);
                    } else {
                        noWcontent.setVisibility(View.GONE);
                        txtView.setVisibility(View.VISIBLE);
                    }
                    if (recEvents.size() < 1) {
                        noRContent.setVisibility(View.VISIBLE);
                        viewRbutton.setVisibility(View.GONE);
                    } else {
                        noRContent.setVisibility(View.GONE);
                        viewRbutton.setVisibility(View.VISIBLE);
                    }
                    bindList();
                    eventsAdapter.notifyDataSetChanged();
                } else {
                    progressBar.setVisibility(View.GONE);
                    if (swipeRefreshLayout != null)
                        swipeRefreshLayout.setRefreshing(false);
//                    RESPONSE result = ResponseUtils.parseError(response);
                    Toast.makeText(getActivity(), getString(R.string.serverError), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<EventsResponse> call, Throwable throwable) {
                progressBar.setVisibility(View.GONE);
                if (swipeRefreshLayout != null)
                    swipeRefreshLayout.setRefreshing(false);
                Toast.makeText(getActivity(), getString(R.string.serverError), Toast.LENGTH_LONG).show();
            }
        });


    }

    public void LoadCategoryEvents(String category, int size, int page) {
        progressBar.setVisibility(View.VISIBLE);

        EventApi retrofitAPI = ServiceClient.createService(EventApi.class, getActivity());
        String token = Global.getInstance(getContext()).getFromSharedPreferneces(Constants.SH_Token);
        retrofitAPI.getEvents(token, category, size, page).enqueue(new Callback<EventsResponse>() {
            @Override
            public void onResponse(Call<EventsResponse> call, Response<EventsResponse> response) {
                if (response.code() == 200) {
                    //     if (swipeRefreshLayout != null)
                    // //    swipeRefreshLayout.setRefreshing(false);
                    progressBar.setVisibility(View.GONE);

                    EventsResponse eventsResponse = response.body();
                    ResponseData responseData = eventsResponse.getData();
                    sliderEvents.clear();
                    weekEvents.clear();
                    recEvents.clear();
                    sliderEvents = responseData.getSlider();
                    weekEvents = responseData.getThisWeek().getData();
                    for (int i = 0; i < weekEvents.size(); i++) {
                        weekEvents.get(i).setType("week");
                    }
                    recEvents = responseData.getRecommended().getData();
                    for (int i = 0; i < recEvents.size(); i++) {
                        recEvents.get(i).setType("recom");
                    }


                    if (sliderEvents.size() < 1)
                        noSContent.setVisibility(View.VISIBLE);
                    else
                        noSContent.setVisibility(View.GONE);
                    if (weekEvents.size() < 1) {
                        noWcontent.setVisibility(View.VISIBLE);
                        txtView.setVisibility(View.GONE);
                    } else {
                        noWcontent.setVisibility(View.GONE);
                        txtView.setVisibility(View.VISIBLE);
                    }
                    if (recEvents.size() < 1) {
                        noRContent.setVisibility(View.VISIBLE);
                        viewRbutton.setVisibility(View.GONE);
                    } else {
                        noRContent.setVisibility(View.GONE);
                        viewRbutton.setVisibility(View.VISIBLE);
                    }

                    bindList();
                    eventsAdapter.notifyDataSetChanged();
                } else {
                    progressBar.setVisibility(View.GONE);

                    Toast.makeText(getActivity(), getString(R.string.serverError), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<EventsResponse> call, Throwable throwable) {
                progressBar.setVisibility(View.GONE);

                Toast.makeText(getActivity(), getString(R.string.serverError), Toast.LENGTH_LONG).show();
            }
        });


    }

    public void getWeekEvents(String category, int size, int page) {
        //  progressBar.setVisibility(View.VISIBLE);
        // HashMap<String, Object> body = new HashMap<>();
        //  String currentLanguage = Global.getInstance(getActivity()).getFromSharedPrefernec(Constants.currentLangauge);

        // body.put("id", "");
        //   if (isFirstTimeAppear==true)
        //  pDialog = showLoadingDialog(getActivity());
        //  progressBar.setVisibility(View.VISIBLE);
        String token = Global.getInstance(getContext()).getFromSharedPreferneces(Constants.SH_Token);
        EventApi retrofitAPI = ServiceClient.createService(EventApi.class, getActivity());
        retrofitAPI.getWeekEvents(token, category, size, page).enqueue(new Callback<WeekEvents>() {
            @Override
            public void onResponse(Call<WeekEvents> call, Response<WeekEvents> response) {
                if (response.code() == 200) {
                    //     if (swipeRefreshLayout != null)
                    // //    swipeRefreshLayout.setRefreshing(false);
                    //progressBar.setVisibility(View.GONE);
                    ArrayList<EventData> events = new ArrayList<>();
                    events = response.body().getData();
                    weekEvents.addAll(events);
                    for (int i = 0; i < weekEvents.size(); i++) {
                        weekEvents.get(i).setType("week");
                    }
                    eventsAdapter.notifyDataSetChanged();

                } else {
//                    progressBar.setVisibility(View.GONE);

//                    RESPONSE result = ResponseUtils.parseError(response);
//                    Toast.makeText(getActivity(), result.getMessage(), Toast.LENGTH_LONG).show();

                }
            }

            @Override
            public void onFailure(Call<WeekEvents> call, Throwable throwable) {
                //  progressBar.setVisibility(View.GONE);

                Toast.makeText(getActivity(), "Error", Toast.LENGTH_LONG).show();
            }
        });
    }

    public void getRecommended(String category, int size, int page) {
        String token = Global.getInstance(getContext()).getFromSharedPreferneces(Constants.SH_Token);
        EventApi retrofitAPI = ServiceClient.createService(EventApi.class, getActivity());
        retrofitAPI.getRecomEvents(token, category, size, page).enqueue(new Callback<WeekEvents>() {
            @Override
            public void onResponse(Call<WeekEvents> call, Response<WeekEvents> response) {
                if (response.code() == 200) {
                    //     if (swipeRefreshLayout != null)
                    // //    swipeRefreshLayout.setRefreshing(false);
                    //progressBar.setVisibility(View.GONE);
                    ArrayList<EventData> events = new ArrayList<>();
                    events = response.body().getData();
                    recEvents.addAll(events);


                    for (int i = 0; i < recEvents.size(); i++) {
                        recEvents.get(i).setType("recom");
                    }

                    eventsAdapter.notifyDataSetChanged();

                } else {
//                    progressBar.setVisibility(View.GONE);

//                    RESPONSE result = ResponseUtils.parseError(response);
//                    Toast.makeText(getActivity(), result.getMessage(), Toast.LENGTH_LONG).show();

                }
            }

            @Override
            public void onFailure(Call<WeekEvents> call, Throwable throwable) {
                //  progressBar.setVisibility(View.GONE);

                Toast.makeText(getActivity(), "Error", Toast.LENGTH_LONG).show();
            }
        });
    }

    private void sendTokenToServer(String deviceToken) {
        HashMap<String, Object> body = new HashMap<>();

        String userToken = Global.getInstance(getContext()).getFromSharedPreferneces(Constants.SH_Token);
        // String deviceToken = Global.getInstance(getContext()).getFromSharedPreferneces(Constants.SH_DeviceToken);

        body.put("deviceType", "android");
        body.put("deviceToken", deviceToken);
        String token = Global.getInstance(getContext()).getFromSharedPreferneces(Constants.SH_Token);
        progressBar.setVisibility(View.VISIBLE);
        UserApi retrofitAPI = ServiceClient.createService(UserApi.class, getActivity());
        retrofitAPI.addToken(userToken, body).enqueue(new Callback<RESPONSE>() {
            @Override
            public void onResponse(Call<RESPONSE> call, Response<RESPONSE> response) {

                if (response.code() == 201) {
                    progressBar.setVisibility(View.GONE);
                    RESPONSE result = response.body();

                    ;

                } else {
                    progressBar.setVisibility(View.GONE);
                    //   RESPONSE result = ResponseUtils.parseError(response);
                    // Toast.makeText(getActivity(), result.getMessage(), Toast.LENGTH_LONG).show();
                    Toast.makeText(getActivity(), getString(R.string.error), Toast.LENGTH_LONG).show();
                }


            }

            @Override
            public void onFailure(Call<RESPONSE> call, Throwable throwable) {
                progressBar.setVisibility(View.GONE);
                Toast.makeText(getActivity(), getString(R.string.errorServer), Toast.LENGTH_LONG).show();
            }
        });

    }

    private void sendNewTokenToServer(String deviceToken, String newToken) {
        HashMap<String, Object> body = new HashMap<>();

        String userToken = Global.getInstance(getContext()).getFromSharedPreferneces(Constants.SH_Token);
        // String deviceToken = Global.getInstance(getContext()).getFromSharedPreferneces(Constants.SH_DeviceToken);

        body.put("oldDeviceToken", deviceToken);
        body.put("deviceToken", newToken);
        String token = Global.getInstance(getContext()).getFromSharedPreferneces(Constants.SH_Token);
        progressBar.setVisibility(View.VISIBLE);
        UserApi retrofitAPI = ServiceClient.createService(UserApi.class, getActivity());
        retrofitAPI.updateToken(userToken, body).enqueue(new Callback<RESPONSE>() {
            @Override
            public void onResponse(Call<RESPONSE> call, Response<RESPONSE> response) {

                if (response.code() == 201) {
                    progressBar.setVisibility(View.GONE);
                    RESPONSE result = response.body();

                    // Toast.makeText(getActivity(), getString(R.string.changed), Toast.LENGTH_LONG).show();

                } else {
                    progressBar.setVisibility(View.GONE);
                    //   RESPONSE result = ResponseUtils.parseError(response);
                    Toast.makeText(getActivity(), getString(R.string.error), Toast.LENGTH_LONG).show();

                }


            }

            @Override
            public void onFailure(Call<RESPONSE> call, Throwable throwable) {
                progressBar.setVisibility(View.GONE);
                Toast.makeText(getActivity(), getString(R.string.errorServer), Toast.LENGTH_LONG).show();
            }
        });

    }

//    public void flipImage(int image) {
//        ImageView imageView = new ImageView(getContext());
//        viewFlipper.addView(imageView);
//        viewFlipper.setAutoStart(true);
//        viewFlipper.setFlipInterval(3000);
//        viewFlipper.setInAnimation(getContext(), android.R.anim.slide_in_left);
//        viewFlipper.setOutAnimation(getContext(), android.R.anim.slide_out_right);
//    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.bind(this, view).unbind();
//         timer.cancel();
    }

    public void getCategories() {
        //  progressBar.setVisibility(View.VISIBLE);
        //  HashMap<String, Object> body = new HashMap<>();
        //  String currentLanguage = Global.getInstance(getActivity()).getFromSharedPrefernec(Constants.currentLangauge);

        //   if (isFirstTimeAppear==true)
        //  pDialog = showLoadingDialog(getActivity());
        //  progressBar.setVisibility(View.VISIBLE);

        EventApi retrofitAPI = ServiceClient.createService(EventApi.class, getActivity());
        retrofitAPI.getCategories().enqueue(new Callback<CategoryData>() {
            @Override
            public void onResponse(Call<CategoryData> call, Response<CategoryData> response) {
                if (response.code() == 200) {
                    if (swipeRefreshLayout != null)
                        swipeRefreshLayout.setRefreshing(false);
                    categories = response.body().getCategories();
                    CategoryData.Category category = new CategoryData.Category();
                    category.setName_en("All");
                    category.setName_ar("الكل");
                    category.set_id("all");
                    categories.add(0, category);

                    loadCategories();
                    LoadEvents(categories.get(0).get_id(), 10, 1);
                } else {
                    if (swipeRefreshLayout != null)
                        swipeRefreshLayout.setRefreshing(false);
                    // Toast.makeText(getActivity(), result.getMessage(), Toast.LENGTH_LONG).show();
                }


            }

            @Override
            public void onFailure(Call<CategoryData> call, Throwable throwable) {
                if (swipeRefreshLayout != null)
                    swipeRefreshLayout.setRefreshing(false);

                //  hideLoadingDialog(pDialog);
                //   progressBar.setVisibility(View.GONE);
                Toast.makeText(getActivity(), getString(R.string.errorServer), Toast.LENGTH_LONG).show();


            }
        });

    }
    public static boolean hasPermissions(Context context, String... permissions){

        if(Build.VERSION.SDK_INT>=Build.VERSION_CODES.M && context!=null && permissions!=null){
            for(String permission: permissions){
                if(ActivityCompat.checkSelfPermission(context, permission)!=PackageManager.PERMISSION_GRANTED){
                    return  false;
                }
            }
        }
        return true;
    }

    private void showAlert() {
        builder = new AlertDialog.Builder(getContext(), R.style.AlertDialogeTheme);
        builder.setMessage(this.getString(R.string.loginFirst));
        builder.setPositiveButton(this.getString(R.string.ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                alertDialog.cancel();
            }
        });

        builder.setNegativeButton(this.getString(R.string.Signup), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(getActivity(), MainActivity.class);
                intent.putExtra("tag", "signUp");
                startActivity(intent);
            }
        });
        alertDialog = builder.create();

        alertDialog.show();
    }
}


