package com.interteleco.kuwaitcalendar.fragments;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.fragment.app.Fragment;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.Task;

import com.interteleco.kuwaitcalendar.R;

import com.interteleco.kuwaitcalendar.activities.Home;
import com.interteleco.kuwaitcalendar.activities.MainActivity;

import com.interteleco.kuwaitcalendar.server.ServiceClient;
import com.interteleco.kuwaitcalendar.server.UserApi;
import com.interteleco.kuwaitcalendar.server.models.APIError;

import com.interteleco.kuwaitcalendar.server.models.UserModel;
import com.interteleco.kuwaitcalendar.utils.Constants;
import com.interteleco.kuwaitcalendar.utils.ErrorUtils;
import com.interteleco.kuwaitcalendar.utils.Global;
import com.interteleco.kuwaitcalendar.utils.Validator;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Login extends Fragment {

    @BindView(R.id.loginBtn)
    AppCompatButton loginBtn;


    @BindView(R.id.txtErrorEmail)
    TextView txtErrorEmail;


    @BindView(R.id.txtErrorPass)
    TextView txtErrorPass;


    @BindView(R.id.edtEmail)
    EditText edtEmail;


    @BindView(R.id.password)
    AppCompatEditText password;

    Global objGlobal;
    int RC_SIGN_IN;
    LoginButton loginButton;
    CallbackManager callbackManager;
    View view;
    AccessToken token = null;
    GoogleSignInClient mGoogleSignInClient;
    GoogleSignInAccount account;
    ProgressBar progressBar;
    AlertDialog alertDialog;
    AlertDialog.Builder builder;
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.login, container, false);
        FacebookSdk.sdkInitialize(getContext());
        ButterKnife.bind(this, view);
        progressBar = (ProgressBar) view.findViewById(R.id.progressBar);

        objGlobal = Global.getInstance(getActivity());
        callbackManager = CallbackManager.Factory.create();
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail().requestIdToken(Constants.ClientID)
                .build();
        mGoogleSignInClient = GoogleSignIn.getClient(getActivity(), gso);
        //   GoogleSignInAccount   account = GoogleSignIn.getLastSignedInAccount(getActivity());
        //updateUI(account);

        //
        edtEmail.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                txtErrorEmail.setVisibility(View.GONE);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        password.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                txtErrorPass.setVisibility(View.GONE);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        return view;
    }

    private void loginwithFacebook() {

        loginButton = new LoginButton(getActivity());
        loginButton.setFragment(this);
        loginButton.setReadPermissions("email");
        //     LoginManager.getInstance().logInWithReadPermissions(this,
        //  Arrays.asList("email"));
        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                loginResult.getAccessToken().getUserId();

                token = loginResult.getAccessToken();

                if (token != null) {

                    getFacebookUserInfo();
                }


            }

            @Override
            public void onCancel() {

            }

            @Override
            public void onError(FacebookException error) {
                Toast.makeText(getActivity(), "Error", Toast.LENGTH_LONG).show();
            }
        });

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        callbackManager.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);

        // Result returned from launching the Intent from GoogleSignInClient.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            // The Task returned from this call is always completed, no need to attach
            // a listener.
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            handleSignInResult(task);
        }


    }

    private void getFacebookUserInfo() {


        GraphRequest request = GraphRequest.newMeRequest(
                token,
                new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(
                            JSONObject object,
                            GraphResponse response) {
                        try {
                            String email = object.getString("email");
                            String id = object.getString("id");
                           // Toast.makeText(getActivity(), id + email, Toast.LENGTH_LONG).show();
                            loginWithSocialAccount("faceId", id, email);
                            LoginManager.getInstance().logOut();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        //JSONObject objRespone = new JSONObject(response.toString().trim());

                        //JSONObject jsonData=objRespone.getJSONObject("graphObject");


//                        Gson gson = new Gson();
//                        FacebookUser objFacebookUser = gson.fromJson(object.toString(), FacebookUser.class);
//                        String img = String.format(
//                                "http://graph.facebook.com/%s/picture?type=large",
//                                objFacebookUser.getId());
//                        ExboothUser objUser = CreateUser(objFacebookUser.getName(), objFacebookUser.getEmail(),
//                                objFacebookUser.getGender(), img);
//                        Register objRegister = (Register) Fragment.instantiate(getActivity(),
//                                Register.class.getName(), null);
//                        objRegister.objUser = objUser;
//                        pbWelcome.setVisibility(View.INVISIBLE);
//                        pbWelcome.setVisibility(View.INVISIBLE);
//                        ((Main) getActivity()).addFrgament(objRegister);

                        // Application code
                    }
                });
        Bundle parameters = new Bundle();
        parameters.putString("fields", "id,name,link,gender,birthday,email");
        request.setParameters(parameters);
        request.executeAsync();
    }

    @Override
    public void onStart() {
        super.onStart();
//        GoogleSignInAccount acct = GoogleSignIn.getLastSignedInAccount(getActivity());
//        if (acct != null) {
//
//            String personEmail = acct.getEmail();
//            String personId = acct.getId();
//            loginWithSocialAccount("googleId",personId, personEmail);
//        }

    }

    private void signIn() {
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    @OnClick({R.id.loginBtn, R.id.mail, R.id.facebook, R.id.forgotPass})
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.loginBtn: {

                validateLoginData();

//                CategoryList list = new CategoryList();
//                list.listener = this;
//                list.show(getActivity().getSupportFragmentManager(), "list");
//                CategoryList list = new CategoryList();
//                list.Tag = "cat";
//                list.listener = this;
//                list.category = categories;
//                list.show(getActivity().getSupportFragmentManager(), "list");

            }
            break;

            case R.id.facebook: {

                loginwithFacebook();
                loginButton.performClick();
            }
            break;
            case R.id.mail: {
                signIn();
            }
            break;
            case R.id.forgotPass: {
                ForgetPassword forgetPassword = new ForgetPassword();

                ((MainActivity) getActivity()).addFragament(forgetPassword);
            }
            break;
        }

    }


    private void handleSignInResult(Task<GoogleSignInAccount> completedTask) {
        try {
            GoogleSignInAccount account = completedTask.getResult(ApiException.class);
            String email = account.getEmail();
            String personId = account.getId();
            loginWithSocialAccount("googleId", personId, email);


            // Toast.makeText(getActivity(),personId+ email, Toast.LENGTH_LONG).show();
            // Signed in successfully, show authenticated UI.
            //updateUI(account);
        } catch (ApiException e) {

            e.printStackTrace();
            // The ApiException status code indicates the detailed failure reason.
            // Please refer to the GoogleSignInStatusCodes class reference for more information.
            //  Log.w(TAG, "signInResult:failed code=" + e.getStatusCode());
            //updateUI(null);
        }
    }

    private void validateLoginData() {


        if (Validator.isEmptyString(edtEmail.getText().toString())) {

            if (Validator.isValidEMail(edtEmail.getText().toString())) {

                if (Validator.isEmptyString(password.getText().toString())) {
                    if (Validator.ValidatePassword(password.getText().toString())) {

                        //  Toast.makeText(getActivity(), "Valid data", Toast.LENGTH_LONG).show();
                        ((MainActivity) getActivity()).hideKeyBoard();
                        loginAccount();

                    } else {
                        txtErrorPass.setVisibility(View.VISIBLE);
                    }


                } else {
                    password.requestFocus();
                    txtErrorPass.setVisibility(View.VISIBLE);
                    txtErrorPass.setText(getString(R.string.passwordCharacters));
                }


            } else {
                edtEmail.requestFocus();
                txtErrorEmail.setVisibility(View.VISIBLE);
                txtErrorEmail.setText(getString(R.string.entervalidEmail));
            }
        } else {
            edtEmail.requestFocus();
            txtErrorEmail.setVisibility(View.VISIBLE);

        }


    }


    public void loginAccount() {
        progressBar.setVisibility(View.VISIBLE);
        HashMap<String, Object> body = new HashMap<>();

        body.put("email", edtEmail.getText().toString().trim());
        body.put("password", password.getText().toString());

        UserApi retrofitAPI = ServiceClient.createService(UserApi.class, getActivity());
        retrofitAPI.login(body).enqueue(new Callback<UserModel>() {
            @Override
            public void onResponse(Call<UserModel> call, Response<UserModel> response) {
                if (response.code() == 200) {
                    progressBar.setVisibility(View.GONE);
                    UserModel user = response.body();
                    Global.getInstance(getActivity()).saveInSharedPrefernces(Constants.SH_Email, user.getEmail());

                    Global.getInstance(getActivity()).saveInSharedPrefernces(Constants.SH_statusPhone, user.getPhoneActivation().toString());
                    Global.getInstance(getActivity()).saveInSharedPrefernces(Constants.SH_stausEmail, user.getEmailActivation().toString());

                    if (user.getEmailActivation() == true && user.getPhoneActivation() == true) {
                        Global.getInstance(getActivity()).saveInSharedPrefernces(Constants.SH_Token, "bearer " + user.getBearer());
                        Global.getInstance(getActivity()).saveInSharedPrefernces(Constants.Authorized, "true");
                        Intent objIntent = new Intent(getActivity(), Home.class);
                        startActivity(objIntent);
                        getActivity().finishAffinity();
                    } else {
                        showAlert( user.getBearer());
//                        EditUserProfile editUserProfile = new EditUserProfile();
//                        editUserProfile.userToken = "bearer " + user.getBearer();
//                        editUserProfile.navigationTag = "verification";
//                        ((MainActivity) getActivity()).addFragament(editUserProfile);
                        //Toast.makeText(getActivity(), getString(R.string.verifyMessage), Toast.LENGTH_LONG).show();
                    }


                } else {
                    progressBar.setVisibility(View.GONE);
                    APIError error = ErrorUtils.parseError(response);
                    Toast.makeText(getActivity(), error.getMessage(), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<UserModel> call, Throwable throwable) {
                progressBar.setVisibility(View.GONE);

                Toast.makeText(getActivity(), getString(R.string.errorServer), Toast.LENGTH_LONG).show();
            }
        });


    }

    public void loginWithSocialAccount(String idType, String id
            , String email) {

        HashMap<String, Object> body = new HashMap<>();

        body.put(idType, id);
        body.put("email", email);

        UserApi retrofitAPI = ServiceClient.createService(UserApi.class, getActivity());
        retrofitAPI.loginSocial(body).enqueue(new Callback<UserModel>() {
            @Override
            public void onResponse(Call<UserModel> call, Response<UserModel> response) {
                if (response.code() == 200) {
                    //    progressBar.setVisibility(View.GONE);

                    UserModel user = response.body();
                    Global.getInstance(getActivity()).saveInSharedPrefernces(Constants.SH_Email, user.getEmail());

                    Global.getInstance(getActivity()).saveInSharedPrefernces(Constants.SH_statusPhone, user.getPhoneActivation().toString());
                    Global.getInstance(getActivity()).saveInSharedPrefernces(Constants.SH_stausEmail, user.getEmailActivation().toString());

                    if (user.getEmailActivation() == true && user.getPhoneActivation() == true) {
                        Global.getInstance(getActivity()).saveInSharedPrefernces(Constants.SH_Token, "bearer " + user.getBearer());
                        Global.getInstance(getActivity()).saveInSharedPrefernces(Constants.Authorized, "true");
                        Intent objIntent = new Intent(getActivity(), Home.class);
                        startActivity(objIntent);
                        getActivity().finishAffinity();
                    } else {
                     showAlert(user.getBearer());
                       // Toast.makeText(getActivity(), getString(R.string.verifyMessage), Toast.LENGTH_LONG).show();
                    }


                    //      Toast.makeText(getActivity(), "Successful Login", Toast.LENGTH_LONG).show();


//                    getActivity().getSupportFragmentManager().popBackStack();
                } else if (response.code() == 400) {
                    //  progressBar.setVisibility(View.GONE);

                    // RESPONSE result = ResponseUtil.parseError(response);

                    Toast.makeText(getActivity(), getString(R.string.error), Toast.LENGTH_LONG).show();

                } else if (response.code() == 401) {
                    // Toast.makeText(getActivity(), getString(R.string.error), Toast.LENGTH_LONG).show();
                    Intent intent = new Intent(Global.getInstance(getContext()).BROADCAST_ACTION);
                    intent.putExtra("action", Constants.SocialRegister);
                    intent.putExtra("socialId", id);
                    intent.putExtra("tag", "social");
                    intent.putExtra("idType", idType);
                    intent.putExtra("email", email);
                    getContext().sendBroadcast(intent);
                }
            }

            @Override
            public void onFailure(Call<UserModel> call, Throwable throwable) {
                progressBar.setVisibility(View.GONE);

                Toast.makeText(getActivity(), getString(R.string.errorServer), Toast.LENGTH_LONG).show();
            }
        });


    }
    private void showAlert(String token) {
        builder = new AlertDialog.Builder(getContext(), R.style.AlertDialogeTheme);
        builder.setMessage(this.getString(R.string.verifyMessage));
        builder.setPositiveButton(this.getString(R.string.ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                EditUserProfile editUserProfile = new EditUserProfile();
                editUserProfile.userToken = "bearer " + token;
                editUserProfile.navigationTag = "verification";
                ((MainActivity) getActivity()).addFragament(editUserProfile);

            }
        });


        alertDialog = builder.create();

        alertDialog.show();
    }
}


