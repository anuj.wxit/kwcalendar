package com.interteleco.kuwaitcalendar.fragments;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import com.interteleco.kuwaitcalendar.R;
import com.interteleco.kuwaitcalendar.activities.Home;
import com.interteleco.kuwaitcalendar.activities.MainActivity;
import com.interteleco.kuwaitcalendar.dialogs.CategoryList;
import com.interteleco.kuwaitcalendar.dialogs.NoConnection;
import com.interteleco.kuwaitcalendar.dialogs.TermsDialog;
import com.interteleco.kuwaitcalendar.listeners.ListListener;

import com.interteleco.kuwaitcalendar.server.EventApi;
import com.interteleco.kuwaitcalendar.server.ServiceClient;
import com.interteleco.kuwaitcalendar.server.UserApi;
import com.interteleco.kuwaitcalendar.server.models.APIError;
import com.interteleco.kuwaitcalendar.server.models.CategoryData;

import com.interteleco.kuwaitcalendar.server.models.RESPONSE;
import com.interteleco.kuwaitcalendar.server.models.Target;
import com.interteleco.kuwaitcalendar.server.models.UserModel;
import com.interteleco.kuwaitcalendar.server.models.Venue;
import com.interteleco.kuwaitcalendar.utils.Constants;
import com.interteleco.kuwaitcalendar.utils.ErrorUtils;
import com.interteleco.kuwaitcalendar.utils.Fonts;
import com.interteleco.kuwaitcalendar.utils.Global;
import com.interteleco.kuwaitcalendar.utils.Validator;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Register extends Fragment implements ListListener {

    @BindView(R.id.register)
    AppCompatButton register;

    @BindView(R.id.txtErrorName)
    TextView txtErrorName;

    @BindView(R.id.txtErrorEmail)
    TextView txtErrorEmail;
    @BindView(R.id.txtErrorPhone)
    TextView txtErrorPhone;
    @BindView(R.id.txtErrorconfirm)
    TextView txtErrorconfirm;

    @BindView(R.id.txtErrorPass)
    TextView txtErrorPass;


    @BindView(R.id.txtErrorAccType)
    TextView txtErrorAccType;

    @BindView(R.id.txtErrorInterest)
    TextView txtErrorInterest;
    @BindView(R.id.haveAccount)
    TextView txthaveAccount;
    @BindView(R.id.login)
    TextView login;

    @BindView(R.id.edtName)
    EditText edtName;

    @BindView(R.id.edtEmail)
    EditText edtEmail;

    @BindView(R.id.phone)
    AppCompatEditText phone;

    @BindView(R.id.password)
    AppCompatEditText password;

    @BindView(R.id.confirmPassword)
    AppCompatEditText confirmPassword;

    @BindView(R.id.accType)
    AppCompatButton accountType;

    @BindView(R.id.interest)
    AppCompatButton interest;


    @BindView(R.id.selectPic)
    ImageView selectPic;
    @BindView(R.id.avatar)
    ImageView profileImg;
    @BindView(R.id.click)
    TextView terms;

    @BindView(R.id.check)
    CheckBox check;
    @BindView(R.id.txtErrorCheck)
    TextView txtErrorCheck;

    Global objGlobal;

    int Pick_Image = 1;
    int Pick_location = 2;
    View view;


    String interestCategory;
    int accounttype = 1;

    ArrayList<CategoryData.Category> categories;

    Uri imguri;
    String postPath = "";
    String mediaPath;
    long imageSize;
    String tag = "";
    String socialId, SocialEmail, idType;
    ProgressBar progressBar;

    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.register, container, false);
        ButterKnife.bind(this, view);

        progressBar = (ProgressBar) view.findViewById(R.id.progressBar);
        objGlobal = Global.getInstance(getActivity());
        setFonts();
        categories = new ArrayList<>();
        getCategories();
        //


        edtName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                txtErrorName.setVisibility(View.GONE
                );
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        edtEmail.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                txtErrorEmail.setVisibility(View.GONE);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        password.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                txtErrorPass.setVisibility(View.GONE);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        confirmPassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                txtErrorconfirm.setVisibility(View.GONE);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        phone.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                txtErrorPhone.setVisibility(View.GONE);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        check.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked)
                    txtErrorCheck.setVisibility(View.GONE);
                else
                    txtErrorCheck.setVisibility(View.VISIBLE);
            }
        });


        return view;

    }

    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            updateAction(intent);


        }
    };

    private void setFonts() {

        Fonts.set(new TextView[]{txthaveAccount, txtErrorName, txtErrorEmail, txtErrorPass, txtErrorconfirm, txtErrorAccType, txtErrorInterest
                , txtErrorPhone}, getActivity(), 0);
        Fonts.set(new EditText[]{edtName, edtEmail, phone, password, confirmPassword}, getActivity(), 0);
        Fonts.set(new Button[]{register}, getActivity(), 0);
    }

    private void updateAction(Intent intent) {
        String action = intent.getStringExtra("action");

        if (action.contains(Constants.SocialRegister)) {

            tag = intent.getStringExtra("tag");
            socialId = intent.getStringExtra("socialId");
            SocialEmail = intent.getStringExtra("email");
            idType = intent.getStringExtra("idType");
            edtEmail.setText(SocialEmail);
            edtEmail.setEnabled(false);

        }

    }

    @Override
    public void onResume() {
        super.onResume();

        IntentFilter intentFilter = new IntentFilter(Global.getInstance(getActivity()).BROADCAST_ACTION);

        getContext().registerReceiver(broadcastReceiver, intentFilter);


    }

    @Override
    public void onDestroy() {

        super.onDestroy();
        getContext().unregisterReceiver(broadcastReceiver);
    }

    @OnClick({R.id.accType, R.id.interest, R.id.register, R.id.selectPic, R.id.login, R.id.click})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.accType: {
                CategoryList list = new CategoryList();
                list.Tag = "account";
                list.listener = this;

                list.show(getActivity().getSupportFragmentManager(), "list");

            }

            break;
            case R.id.interest: {
                CategoryList list = new CategoryList();
                list.Tag = "cat";
                list.listener = this;
                list.category = categories;
                list.show(getActivity().getSupportFragmentManager(), "list");

            }
            break;
            case R.id.register: {
                registerUser();
            }
            break;
            case R.id.selectPic: {
                askpermissions();
            }
            break;
            case R.id.login: {
                Intent intent = new Intent(Global.getInstance(getContext()).BROADCAST_ACTION);
                intent.putExtra("action", Constants.Login);

                getContext().sendBroadcast(intent);

            }
            break;
            case R.id.click: {
//                TermsDialog dialog = new TermsDialog();
//                dialog.show(getActivity().getSupportFragmentManager(), "Dialog");
                Uri uri;
                if (Global.getInstance(getActivity()).getFromSharedPreferneces("mylang").equalsIgnoreCase("en"))
               uri = Uri.parse("http://kwcalendar.net/.well-known/terms-conditions.html");
             else
                 uri = Uri.parse("http://kwcalendar.net/.well-known/terms-conditions-ar.html");
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                startActivity(intent);
            }
            break;
        }

    }


    private void openGallery() {
        //  Intent gallery = new Intent();
        //  gallery.setType("image/*");
        Intent gallery = new Intent(Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(gallery, Pick_Image);
    }


    private void registerUser() {

        if (edtName.getText().toString() != null && !edtName.getText().toString().equalsIgnoreCase("")) {

            if (Validator.isEmptyString(edtEmail.getText().toString())) {

                if (Validator.isValidEMail(edtEmail.getText().toString())) {
                    if (Validator.isEmptyString(phone.getText().toString())) {
                        if (phone.length() == 8) {
                            if (Validator.isMobile(phone.getText().toString())) {
                                if (Validator.isEmptyString(password.getText().toString())) {
                                    if (Validator.ValidatePassword(password.getText().toString())) {

                                        if (Validator.ValidatePasswordMatch(password.getText().toString(), confirmPassword.getText().toString())) {

                                            if (accounttype != -1) {

                                                if (Validator.isEmptyString(interestCategory)) {

                                                    if (check.isChecked()) {
                                                        ((MainActivity) getActivity()).hideKeyBoard();


                                                        if (postPath != null && !postPath.equalsIgnoreCase(""))
                                                            uploadImage();
                                                        else
                                                            registerNewAccountWithoutAvatar();
                                                    } else {
                                                        txtErrorCheck.setVisibility(View.VISIBLE);

                                                    }

                                                } else {
                                                    txtErrorInterest.setVisibility(View.VISIBLE);
                                                }
                                            } else {
                                                txtErrorAccType.setVisibility(View.VISIBLE);
                                            }
                                        } else {


                                            confirmPassword.requestFocus();
                                            txtErrorconfirm.setVisibility(View.VISIBLE);
                                            txtErrorconfirm.setText(getString(R.string.passwordsNotMatched));

                                        }

                                    } else {
                                        password.requestFocus();
                                        txtErrorPass.setVisibility(View.VISIBLE);
                                        txtErrorPass.setText(getString(R.string.passwordCharacters));
                                    }


                                } else {
                                    password.requestFocus();
                                    txtErrorPass.setVisibility(View.VISIBLE);

                                }
                            } else {
                                phone.requestFocus();
                                txtErrorPhone.setVisibility(View.VISIBLE);
                                txtErrorPhone.setText(getString(R.string.mobileStart));
                            }


                        } else {
                            phone.requestFocus();
                            txtErrorPhone.setVisibility(View.VISIBLE);
                            txtErrorPhone.setText(getString(R.string.mobilecharacters));
                        }
                    } else {
                        phone.requestFocus();
                        txtErrorPhone.setVisibility(View.VISIBLE);
                    }
                } else {
                    edtEmail.requestFocus();
                    txtErrorEmail.setVisibility(View.VISIBLE);
                    txtErrorEmail.setText(getString(R.string.entervalidEmail));
                }
            } else {

                txtErrorEmail.setVisibility(View.VISIBLE);
                edtEmail.requestFocus();
            }

        } else {
            txtErrorName.setVisibility(View.VISIBLE);
            edtName.requestFocus();

        }


    }

    @Override
    public void selectTarget(Target target) {
        accounttype = target.getId();
        accountType.setText(target.getTitle());
    }

    @Override
    public void selectVenue(Venue venue) {
        // venueBtn.setText(venue.getName_en());
        //  txtVenue = venue.get_id();
    }

    @Override
    public void selectCategory(CategoryData.Category category) {
        ///  typeBtn.setText(category.getName_en());
        if (Global.getInstance(getContext()).getFromSharedPreferneces("mylang").equalsIgnoreCase("en"))
            interest.setText(category.getName_en());
        else
            interest.setText(category.getName_ar());

        interestCategory = category.get_id();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == Pick_Image) {
            if (data != null) {
                imguri = data.getData();


                int hight = 0, width = 0;

                Bitmap bitmap = null;
                try {
                    bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), imguri);
                    hight = bitmap.getHeight();
                    width = bitmap.getWidth();


                } catch (IOException e) {
                    e.printStackTrace();
                }
                profileImg.setImageBitmap(bitmap);
                //uploadImage();

                //    Toast.makeText(getActivity(), imguri.getPath(), Toast.LENGTH_LONG).show();

                //Toast.makeText(getActivity(), hight + "/" + width, Toast.LENGTH_LONG).show();
                Uri selectedImage = data.getData();
                String[] filePathColumn = {MediaStore.Images.Media.DATA};

                Cursor cursor = getActivity().getContentResolver().query(selectedImage, filePathColumn, null, null, null);
                assert cursor != null;
                cursor.moveToFirst();

                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                mediaPath = cursor.getString(columnIndex);
                // Set the Image in ImageView for Previewing the Media
                //  imageView.setImageBitmap(BitmapFactory.decodeFile(mediaPath));
                cursor.close();
                postPath = mediaPath;
                //  Toast.makeText(getActivity(), postPath, Toast.LENGTH_LONG).show();
            }
        }
    }

    private void uploadImage() {


        //  Toast.makeText(getActivity(), postPath, Toast.LENGTH_LONG).show();
        File file = new File(postPath);
        imageSize = file.length() / 1024;
        // Toast.makeText(getActivity(), +lenght + "", Toast.LENGTH_LONG).show();
        Map<String, RequestBody> map = new HashMap<>();
        //ile file = new File(postPath);

        // Parsing any Media type file
        RequestBody requestBody = RequestBody.create(MediaType.parse("*/*"), file);
        //  map.put("file\"; filename=\"" + file.getName() + "\"", requestBody);
        map.put("image", requestBody);
        // create RequestBody instance from file
        RequestBody requestFile =
                RequestBody.create(
                        MediaType.parse(getActivity().getContentResolver().getType(imguri)),
                        file
                );
        // MultipartBody.Part is used to send also the actual file name
        MultipartBody.Part body = MultipartBody.Part.createFormData("image", file.getName(), requestFile);

        if (imageSize <= 300) {
            EventApi retrofitAPI = ServiceClient.createService(EventApi.class, getActivity());
            retrofitAPI.upload("users", Constants.token, body).enqueue(new Callback<RESPONSE>() {
                @Override
                public void onResponse(Call<RESPONSE> call, Response<RESPONSE> response) {
                    if (response.code() == 200) {
                        //    progressBar.setVisibility(View.GONE);

                        RESPONSE result = response.body();
                        String url = result.getUrl();

                        registerNewAccount(url);

                        //    Toast.makeText(getActivity(), result.getUrl(), Toast.LENGTH_LONG).show();


//                    getActivity().getSupportFragmentManager().popBackStack();
                    } else if (response.code() == 201) {
                        //  progressBar.setVisibility(View.GONE);

                        // RESPONSE result = ResponseUtil.parseError(response);

                        Toast.makeText(getActivity(), getString(R.string.error), Toast.LENGTH_LONG).show();

                    } else {
                        Toast.makeText(getActivity(), getString(R.string.error), Toast.LENGTH_LONG).show();

                    }
                }

                @Override
                public void onFailure(Call<RESPONSE> call, Throwable throwable) {
                    //progressBar.setVisibility(View.GONE);

                    Toast.makeText(getActivity(), getString(R.string.errorServer), Toast.LENGTH_LONG).show();
                }
            });
        } else {
            Toast.makeText(getActivity(), "Image Size is too large", Toast.LENGTH_LONG).show();

        }
    }

    public void registerNewAccount(String Url) {

        HashMap<String, Object> body = new HashMap<>();
        body.put("name", edtName.getText().toString().trim());
        body.put("email", edtEmail.getText().toString().trim());
        body.put("password", password.getText().toString());
        body.put("confirmPassword", confirmPassword.getText().toString());
        body.put("mobileNumber", phone.getText().toString());
        body.put("accountType", accounttype);
        if (tag.equalsIgnoreCase("social")) {
            if (idType.equalsIgnoreCase("faceId"))
                body.put("faceId", socialId);
            else
                body.put("googleId", socialId);
        }
        body.put("interestedCategory", interestCategory);

        body.put("imageURL", Url);
        progressBar.setVisibility(View.VISIBLE);
        UserApi retrofitAPI = ServiceClient.createService(UserApi.class, getActivity());
        retrofitAPI.register(body).enqueue(new Callback<UserModel>() {
            @Override
            public void onResponse(Call<UserModel> call, Response<UserModel> response) {
                if (response.code() == 200) {
                    progressBar.setVisibility(View.GONE);

                    UserModel user = response.body();
                    Global.getInstance(getActivity()).saveInSharedPrefernces(Constants.SH_Email, user.getEmail());
                    Global.getInstance(getActivity()).saveInSharedPrefernces(Constants.SH_statusPhone, user.getPhoneActivation().toString());
                    Global.getInstance(getActivity()).saveInSharedPrefernces(Constants.SH_stausEmail, user.getEmailActivation().toString());

                    //   Global.getInstance(getActivity()).saveInSharedPrefernces(Constants.SH_Token, "bearer " + user.getBearer());
                    // Global.getInstance(getActivity()).saveInSharedPrefernces(Constants.Authorized, "true");


                    VerifyEmail verifyEmail = new VerifyEmail();
                    verifyEmail.tag = "register";
                    verifyEmail.token = "bearer " + user.getBearer();
                    ((MainActivity) getActivity()).addFragamentbyTAG(verifyEmail, "verify");
//                    getActivity().getSupportFragmentManager().popBackStack();
                } else if (response.code() == 400) {
                    progressBar.setVisibility(View.GONE);

                    // RESPONSE result = ResponseUtil.parseError(response);
                    APIError error = ErrorUtils.parseError(response);
                    Toast.makeText(getActivity(), error.getErrors().get(0).getMessage(), Toast.LENGTH_LONG).show();

                } else {
                    progressBar.setVisibility(View.GONE);
                    APIError error = ErrorUtils.parseError(response);
                    Toast.makeText(getActivity(), error.getErrors().get(0).getMessage(), Toast.LENGTH_LONG).show();

                }
            }

            @Override
            public void onFailure(Call<UserModel> call, Throwable throwable) {
                progressBar.setVisibility(View.GONE);

                Toast.makeText(getActivity(), getString(R.string.errorServer), Toast.LENGTH_LONG).show();
            }
        });


    }

    public void registerNewAccountWithoutAvatar() {

        HashMap<String, Object> body = new HashMap<>();
        body.put("name", edtName.getText().toString().trim());
        body.put("email", edtEmail.getText().toString().trim());
        body.put("password", password.getText().toString());
        body.put("confirmPassword", confirmPassword.getText().toString());
        body.put("mobileNumber", phone.getText().toString());
        body.put("accountType", accounttype);

        if (tag.equalsIgnoreCase("social")) {
            if (idType.equalsIgnoreCase("faceId"))
                body.put("faceId", socialId);
            else
                body.put("googleId", socialId);
        }


        body.put("interestedCategory", interestCategory);
        progressBar.setVisibility(View.VISIBLE);
        UserApi retrofitAPI = ServiceClient.createService(UserApi.class, getActivity());
        retrofitAPI.register(body).enqueue(new Callback<UserModel>() {
            @Override
            public void onResponse(Call<UserModel> call, Response<UserModel> response) {
                if (response.code() == 200) {
                    progressBar.setVisibility(View.GONE);


                    UserModel user = response.body();
                    Global.getInstance(getActivity()).saveInSharedPrefernces(Constants.SH_Email, user.getEmail());


                    Global.getInstance(getActivity()).saveInSharedPrefernces(Constants.SH_statusPhone, user.getPhoneActivation().toString());
                    Global.getInstance(getActivity()).saveInSharedPrefernces(Constants.SH_stausEmail, user.getEmailActivation().toString());


                    //   Global.getInstance(getActivity()).saveInSharedPrefernces(Constants.SH_Token, "bearer " + user.getBearer());
                    //Global.getInstance(getActivity()).saveInSharedPrefernces(Constants.Authorized, "true");

                    VerifyEmail verifyEmail = new VerifyEmail();
                    verifyEmail.tag = "register";
                    verifyEmail.token = "bearer " + user.getBearer();
                    ((MainActivity) getActivity()).addFragamentbyTAG(verifyEmail, "verify");

//                    getActivity().getSupportFragmentManager().popBackStack();
                } else if (response.code() == 400) {
                    progressBar.setVisibility(View.GONE);

                    //     errors = response.body().getErrors();

                    APIError error = ErrorUtils.parseError(response);
                    Toast.makeText(getActivity(), error.getErrors().get(0).getMessage(), Toast.LENGTH_LONG).show();


                } else {
                    APIError error = ErrorUtils.parseError(response);
                    Toast.makeText(getActivity(), error.getErrors().get(0).getMessage(), Toast.LENGTH_LONG).show();

                }
            }

            @Override
            public void onFailure(Call<UserModel> call, Throwable throwable) {
                progressBar.setVisibility(View.GONE);

                Toast.makeText(getActivity(), getString(R.string.errorServer), Toast.LENGTH_LONG).show();
            }
        });


    }

    public void getCategories() {
        //  progressBar.setVisibility(View.VISIBLE);
        //  HashMap<String, Object> body = new HashMap<>();
        //  String currentLanguage = Global.getInstance(getActivity()).getFromSharedPrefernec(Constants.currentLangauge);

        //   if (isFirstTimeAppear==true)
        //  pDialog = showLoadingDialog(getActivity());
        //  progressBar.setVisibility(View.VISIBLE);

        EventApi retrofitAPI = ServiceClient.createService(EventApi.class, getActivity());
        retrofitAPI.getCategories().enqueue(new Callback<CategoryData>() {
            @Override
            public void onResponse(Call<CategoryData> call, Response<CategoryData> response) {
                if (response.code() == 200) {

                    categories = response.body().getCategories();


                } else {

                }


            }

            @Override
            public void onFailure(Call<CategoryData> call, Throwable throwable) {
                //  hideLoadingDialog(pDialog);
                //   progressBar.setVisibility(View.GONE);
                Toast.makeText(getActivity(), getString(R.string.errorServer), Toast.LENGTH_LONG).show();


            }
        });

    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);


        if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

            if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                openGallery();
            }
        }
    }

    private void askpermissions() {


        if (Build.VERSION.SDK_INT < 23) {

            if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    Activity#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for Activity#requestPermissions for more details.
                ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, 2);

            } else {


                openGallery();

            }


        } else {

            if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {


                openGallery();


            } else {

                ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);

            }


        }
    }

}
