package com.interteleco.kuwaitcalendar.fragments;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.fragment.app.Fragment;

import com.interteleco.kuwaitcalendar.R;
import com.interteleco.kuwaitcalendar.activities.Home;
import com.interteleco.kuwaitcalendar.activities.Splash;
import com.interteleco.kuwaitcalendar.server.ServiceClient;
import com.interteleco.kuwaitcalendar.server.UserApi;
import com.interteleco.kuwaitcalendar.server.models.APIError;
import com.interteleco.kuwaitcalendar.server.models.RESPONSE;
import com.interteleco.kuwaitcalendar.utils.Constants;
import com.interteleco.kuwaitcalendar.utils.ErrorUtils;
import com.interteleco.kuwaitcalendar.utils.Fonts;
import com.interteleco.kuwaitcalendar.utils.Global;


import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class Setting extends Fragment {


    @BindView(R.id.menuBtn)
    ImageView backBtn;

    @BindView(R.id.toolbar_title)
    TextView toolbar_title;
    @BindView(R.id.changeLanguage)
    TextView changeLanguage;

    @BindView(R.id.language)
    TextView language;
    String authorized;

    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.settings, container, false);
        ButterKnife.bind(this, view);
        ((Home) getActivity()).customizeToolbar(backBtn, getResources().getDrawable(R.drawable.arrow_leftb), getResources().getDrawable(R.drawable.arrow_right));


        authorized = Global.getInstance(getContext()).getFromSharedPreferneces(Constants.Authorized);


        setFonts();
        // Fonts.set(new TextView[]{(TextView) toolbar.getChildAt(0)}, getActivity(), 0);


        return view;

    }

    private void setFonts() {

        Fonts.set(new TextView[]{toolbar_title, changeLanguage, language}, getActivity(), 1);
        //   Fonts.set(new TextView[]{toolbar_title}, getActivity(), 1);

    }

    @OnClick({R.id.menuBtn, R.id.changeLanguage})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.menuBtn: {
                getFragmentManager().popBackStack();

            }

            break;

            case R.id.changeLanguage: {
                String language = Global.getInstance(getContext()).getFromSharedPreferneces("mylang");

                if (language.equalsIgnoreCase("en")) {
                    Global.getInstance(getContext()).saveInSharedPrefernces("mylang", "ar");

                } else {
                    Global.getInstance(getContext()).saveInSharedPrefernces("mylang", "en");

                }
                restart(3);
            }

            break;

        }
    }


    public void restart(int delay) {
        Intent intent = new Intent(getContext(), Splash.class);

        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                | Intent.FLAG_ACTIVITY_CLEAR_TASK
                | Intent.FLAG_ACTIVITY_NEW_TASK);

        PendingIntent pendingIntent = PendingIntent.getActivity(
                getContext(), 0, intent, intent.getFlags());

        //Following code will restart your application after 2 seconds
        AlarmManager mgr = (AlarmManager) getContext()
                .getSystemService(Context.ALARM_SERVICE);
        mgr.set(AlarmManager.RTC, System.currentTimeMillis() + 100,
                pendingIntent);

        //This will finish your activity manually
        getActivity().finish();

        //This will stop your application and take out from it.
        System.exit(2);
//        PendingIntent intent = PendingIntent.getActivity(this.getBaseContext(), 0, new Intent(getIntent()), getIntent().getFlags());
//        AlarmManager manager = (AlarmManager) this.getSystemService(Context.ALARM_SERVICE);
//        manager.set(AlarmManager.RTC, System.currentTimeMillis() + delay, intent);
//        System.exit(2);
    }


}
