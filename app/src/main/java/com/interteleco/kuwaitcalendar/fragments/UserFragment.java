package com.interteleco.kuwaitcalendar.fragments;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.tabs.TabLayout;
import com.interteleco.kuwaitcalendar.R;
import com.interteleco.kuwaitcalendar.adapters.UserFragmentAdapter;
import com.interteleco.kuwaitcalendar.utils.Constants;
import com.interteleco.kuwaitcalendar.utils.Fonts;
import com.interteleco.kuwaitcalendar.utils.Global;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class UserFragment extends Fragment {

    private View view;

    AppCompatActivity context;

    @BindView(R.id.viewpager)
    ViewPager MviewPager;

    @BindView(R.id.tabs)
    TabLayout tabLayout;

    Login login;

    Register register;
    public String tag = "";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.user_fragment, container, false);
        ButterKnife.bind(this, view);


        // Toolbar toolbar = (Toolbar) view.findViewById(R.id.toolbar);


        // AppCompatActivity activity = (AppCompatActivity) getActivity();
        // activity.setSupportActionBar(toolbar);
        // activity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        // activity.getSupportActionBar().setHomeAsUpIndicator(R.drawable.arrow);
        //    ((HomeActivity) getActivity()).customizeToolbar(toolbar);
        //   setHasOptionsMenu(true);

        // Fonts.set(new TextView[]{(TextView) toolbar.getChildAt(0)}, getActivity(), 0);
        setupViewPager();
        if (tag.equalsIgnoreCase("signUp"))
            MviewPager.setCurrentItem(1);
        else
            MviewPager.setCurrentItem(0);
        return view;
    }

    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            updateAction(intent);


        }
    };

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_main, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();
        switch (id) {
            case android.R.id.home: {
                getFragmentManager().popBackStack();

            }
        }

        return super.onOptionsItemSelected(item);
    }


    private void setTab(Intent intent) {


    }


    private void updateAction(Intent intent) {
        String action = intent.getStringExtra("action");

        if (action.contains(Constants.SocialRegister)) {

            MviewPager.setCurrentItem(1);

        } else if (action.contains(Constants.Login)) {
            MviewPager.setCurrentItem(0);

        }
    }

    @Override
    public void onResume() {
        super.onResume();

        IntentFilter intentFilter = new IntentFilter(Global.getInstance(getActivity()).BROADCAST_ACTION);

        getContext().registerReceiver(broadcastReceiver, intentFilter);


    }

    @Override
    public void onDestroy() {

        super.onDestroy();
        getContext().unregisterReceiver(broadcastReceiver);
    }


    private void setupViewPager() {
        UserFragmentAdapter adapter = new UserFragmentAdapter(getChildFragmentManager());


        login = new Login();
        register = new Register();


        adapter.addFragment(login, getString(R.string.Signin));
        adapter.addFragment(register, getString(R.string.Signup));

        MviewPager.setAdapter(adapter);
        MviewPager.setOffscreenPageLimit(2);
        tabLayout.setupWithViewPager(MviewPager);

        for (int i = 0; i < tabLayout.getTabCount(); i++) {
            View view = LayoutInflater.from(getContext()).inflate(R.layout.tab, null);
            TextView tv = (TextView) view.findViewById(R.id.tab);
            tabLayout.getTabAt(i).setCustomView(tv);
            tv.setText(tabLayout.getTabAt(i).getText());
            if (i == 0) {
                tv.setTextColor(getResources().getColor(R.color.White));
                //tv.setTextColor(getResources().getColor(R.color.darkgreen));
                // tv.setBackground(getResources().getDrawable(R.drawable.tab_bg));
            } else {
                tv.setTextColor(getResources().getColor(R.color.lightred));
                //  tv.setBackground(getResources().getDrawable(R.drawable.green_tab));
            }
            Fonts.set(new TextView[]{tv}, getActivity(), 1);
        }
        //   changeTabsFont(adapter);
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {

                MviewPager.setCurrentItem(tab.getPosition());
                View view = tab.getCustomView();
                TextView textView = (TextView) view.findViewById(R.id.tab);
                textView.setTextColor(getResources().getColor(R.color.White));


            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                View view = tab.getCustomView();
                TextView textView = (TextView) view.findViewById(R.id.tab);
                textView.setTextColor(getResources().getColor(R.color.lightred));

            }


            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }

        });
    }


}
