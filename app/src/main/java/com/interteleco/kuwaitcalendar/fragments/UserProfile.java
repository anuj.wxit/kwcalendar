package com.interteleco.kuwaitcalendar.fragments;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.bumptech.glide.Glide;
import com.interteleco.kuwaitcalendar.R;
import com.interteleco.kuwaitcalendar.activities.Home;
import com.interteleco.kuwaitcalendar.activities.MainActivity;
import com.interteleco.kuwaitcalendar.adapters.ProfileExpandListAdapter;
import com.interteleco.kuwaitcalendar.server.EventApi;
import com.interteleco.kuwaitcalendar.server.ServiceClient;
import com.interteleco.kuwaitcalendar.server.UserApi;
import com.interteleco.kuwaitcalendar.server.models.EventData;
import com.interteleco.kuwaitcalendar.server.models.ExpandedItem;
import com.interteleco.kuwaitcalendar.server.models.ProfileEvents;
import com.interteleco.kuwaitcalendar.server.models.WeekEvents;
import com.interteleco.kuwaitcalendar.utils.Constants;
import com.interteleco.kuwaitcalendar.utils.Fonts;
import com.interteleco.kuwaitcalendar.utils.Global;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UserProfile extends Fragment {

    private static ExpandableListView expandableListView;
    private static ProfileExpandListAdapter adapter;
    @BindView(R.id.menuBtn)
    ImageView backBtn;
    @BindView(R.id.avatar)
    CircleImageView avatar;
    @BindView(R.id.toolbar_title)
    TextView toolbar_title;
    @BindView(R.id.user)
    TextView user;


    @BindView(R.id.swipe_refresh_layout)
    SwipeRefreshLayout swipeRefreshLayout;

    String avatarUrl, userName;
    Global objGlobal;
    ArrayList<EventData> calendar, myEvents, favourites;
    View view;
    ProgressBar progressBar;
    String tag = "";

    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.user_profile, container, false);
        ButterKnife.bind(this, view);
        progressBar = (ProgressBar) view.findViewById(R.id.progressBar);
        objGlobal = Global.getInstance(getActivity());
        calendar = new ArrayList<>();
        myEvents = new ArrayList<>();
        favourites = new ArrayList<>();
        expandableListView = (ExpandableListView) view.findViewById(R.id.expanded_menu);
        Fonts.set(new TextView[]{toolbar_title}, getActivity(), 1);
        // Setting group indicator null for custom indicator
        expandableListView.setGroupIndicator(null);
        ((Home) getActivity()).customizeToolbar(backBtn, getResources().getDrawable(R.drawable.arrow), getResources().getDrawable(R.drawable.arrow_r));

        getProfileEvents();
        //setItems();

        setListener();

        return view;
    }

    @OnClick({R.id.menuBtn, R.id.selectPic})
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.menuBtn: {
                if (!tag.equalsIgnoreCase("add")) {
                    getFragmentManager().popBackStack();
                } else {
                    getFragmentManager().popBackStack();

                //    getFragmentManager().popBackStack();
                }
            }
            break;

            case R.id.selectPic: {
                EditUserProfile editUserProfile = new EditUserProfile();
                editUserProfile.navigationTag = "home";
                ((Home) getActivity()).addFragament(editUserProfile);
            }
            break;

        }
    }

    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            updateAction(intent);


        }
    };

    private void updateAction(Intent intent) {
        String action = intent.getStringExtra("action");

        if (action.contains(Constants.Calendar) || action.contains(Constants.LikeToggle)) {
            calendar.clear();
            favourites.clear();
            myEvents.clear();
            adapter.notifyDataSetChanged();
            getProfileEvents();
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        IntentFilter intentFilter = new IntentFilter(Global.getInstance(getActivity()).BROADCAST_ACTION);

        getContext().registerReceiver(broadcastReceiver, intentFilter);


    }

    @Override
    public void onDestroy() {

        super.onDestroy();
        getContext().unregisterReceiver(broadcastReceiver);
    }


    void setItems() {

        // Array list for header
        ArrayList<ExpandedItem> header = new ArrayList<>();
        HashMap<ExpandedItem, ArrayList<EventData>> hashMap = new HashMap<ExpandedItem, ArrayList<EventData>>();

        ExpandedItem item = new ExpandedItem(R.drawable.calendar, getString(R.string.myEvents));
        ExpandedItem item2 = new ExpandedItem(R.drawable.calender, getString(R.string.mycalendar));
        ExpandedItem item3 = new ExpandedItem(R.drawable.fav, getString(R.string.Favourites));
        header.add(item);
        header.add(item2);
        header.add(item3);

        // Array list for child items

        // Hash map for both header and child
        //   HashMap<String, List<EventData>> hashMap = new HashMap<String, List<EventData>>();

        // Adding headers to list

        // Adding header and childs to hash map

        hashMap.put(header.get(0), myEvents);
        hashMap.put(header.get(1), calendar);
        hashMap.put(header.get(2), favourites);


        adapter = new ProfileExpandListAdapter(getContext(), header, hashMap);

        // Setting adpater over expandablelistview
        expandableListView.setAdapter(adapter);

        if (swipeRefreshLayout != null) {
            swipeRefreshLayout.setRefreshing(false);
            swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
                @Override
                public void onRefresh() {
                    swipeRefreshLayout.setRefreshing(false);
                    calendar.clear();
                    favourites.clear();
                    myEvents.clear();
                    adapter.notifyDataSetChanged();
                    getProfileEvents();
                }
            });


        }

        }

    // Setting different listeners to expandablelistview
    void setListener() {

        // This listener will show toast on group click
        expandableListView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {

            @Override
            public boolean onGroupClick(ExpandableListView listview, View view,
                                        int group_pos, long id) {

//                Toast.makeText(MainActivity.this,
//                        "You clicked : " + adapter.getGroup(group_pos),
//                        Toast.LENGTH_SHORT).show();
                return false;
            }
        });

        // This listener will expand one group at one time
        // You can remove this listener for expanding all groups
        expandableListView
                .setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {

                    // Default position
                    int previousGroup = -1;

                    @Override
                    public void onGroupExpand(int groupPosition) {
                        if (groupPosition != previousGroup)

                            // Collapse the expanded group
                            expandableListView.collapseGroup(previousGroup);
                        previousGroup = groupPosition;
                    }

                });

        // This listener will show toast on child click
        expandableListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {

            @Override
            public boolean onChildClick(ExpandableListView listview, View view,
                                        int groupPos, int childPos, long id) {
//                Toast.makeText(MainActivity.this,
//                        "You clicked : " + adapter.getChild(groupPos, childPos),
//                        Toast.LENGTH_SHORT).show();

                EventData eventData = (EventData) adapter.getChild(groupPos, childPos);
                EventDetails eventDetails = new EventDetails();
                eventDetails.id = eventData.get_id();
                eventDetails.tag="myevent";
                ((Home) getActivity()).addFragament(eventDetails);


                return false;
            }
        });
    }

    public void getProfileEvents() {

        progressBar.setVisibility(View.VISIBLE);
        UserApi retrofitAPI = ServiceClient.createService(UserApi.class, getActivity());
        retrofitAPI.getUSerEvents(objGlobal.getFromSharedPreferneces(Constants.SH_Token)).enqueue(new Callback<ProfileEvents>() {
            @Override
            public void onResponse(Call<ProfileEvents> call, Response<ProfileEvents> response) {
                if (response.code() == 200) {
                    if (swipeRefreshLayout != null)
                        swipeRefreshLayout.setRefreshing(false);


                    progressBar.setVisibility(View.GONE);
                    calendar = response.body().getMyCalendar();
                    myEvents = response.body().getMyEvents();
                    favourites = response.body().getMyFavourites();
                    avatarUrl = response.body().getAvatar();
                    userName = response.body().getName();
                    // events = response.body().getData();
                    // allevents.addAll(events);
                    // count = response.body().getTotal();
                    user.setText(userName);
                  if(getContext()!=null&&avatarUrl!=null)
                    Glide.with(getContext()).load(Constants.ImgURL + avatarUrl).into(avatar);
if(getContext()!=null)
                    setItems();

                } else {
                    if (swipeRefreshLayout != null)
                        swipeRefreshLayout.setRefreshing(false);

                    progressBar.setVisibility(View.GONE);
                    Toast.makeText(getActivity(), getString(R.string.serverError), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<ProfileEvents> call, Throwable throwable) {

                if (swipeRefreshLayout != null)
                    swipeRefreshLayout.setRefreshing(false);
                progressBar.setVisibility(View.GONE);
                Toast.makeText(getActivity(), getString(R.string.serverError), Toast.LENGTH_LONG).show();
            }
        });
    }


}
