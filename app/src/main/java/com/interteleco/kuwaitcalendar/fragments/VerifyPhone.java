package com.interteleco.kuwaitcalendar.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatButton;
import androidx.fragment.app.Fragment;

import com.goodiebag.pinview.Pinview;
import com.interteleco.kuwaitcalendar.R;
import com.interteleco.kuwaitcalendar.activities.Home;

import com.interteleco.kuwaitcalendar.activities.MainActivity;
import com.interteleco.kuwaitcalendar.server.ServiceClient;
import com.interteleco.kuwaitcalendar.server.UserApi;
import com.interteleco.kuwaitcalendar.server.models.APIError;
import com.interteleco.kuwaitcalendar.server.models.RESPONSE;
import com.interteleco.kuwaitcalendar.utils.Constants;
import com.interteleco.kuwaitcalendar.utils.ErrorUtils;
import com.interteleco.kuwaitcalendar.utils.Global;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class VerifyPhone extends Fragment {
//    @BindView(R.id.menuBtn)
//    ImageView backBtn;

    @BindView(R.id.toolbar_title)
    TextView toolbar_title;

    @BindView(R.id.resendCode)
    TextView resend;


    Pinview pincode;
    ProgressBar progressBar;
    public String tag = "";
    boolean emailStatus;
    String token;

    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.verify_phone, container, false);
        ButterKnife.bind(this, view);
        ((MainActivity) getActivity()).hideKeyBoard();
        pincode = (Pinview) view.findViewById(R.id.code);
        pincode.setTextColor(getResources().getColor(R.color.White));
        progressBar = (ProgressBar) view.findViewById(R.id.progressBar);
        AppCompatButton sendButton = (AppCompatButton) view.findViewById(R.id.verifyCode);

        if (!Global.getInstance(getContext()).getFromSharedPreferneces("mylang").equalsIgnoreCase("en")) {
            pincode.setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
            pincode.setTextDirection(View.TEXT_DIRECTION_LTR);
        }

        // setFonts();
        //  Fonts.set(new TextView[]{(TextView) toolbar.getChildAt(0)}, getActivity(), 0);
        sendButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                //  ((MainActivity) getActivity()).hideKeyBoard();
                //    Toast.makeText(getActivity(), "Button", Toast.LENGTH_SHORT).show();
                verifyPhone();
            }
        });


        return view;

    }

    //    private void setFonts() {
//        Fonts.set(new EditText[]{email}, getActivity(), 0);
//        Fonts.set(new TextView[]{errorMessage, txt1, txt2}, getActivity(), 0);
//
//        Fonts.set(new AppCompatButton[]{sendButton}, getActivity(), 0);
//    }
    @OnClick({R.id.resendCode})
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.resendCode: {

                resendCode();

            }
            break;
//            case R.id.menuBtn: {
//
//                getFragmentManager().popBackStack();
//
//            }
//            break;


        }


    }

    public void verifyPhone() {
        if (pincode.getValue() != "" && pincode.getValue() != null) {

            HashMap<String, Object> body = new HashMap<>();
            //    body.put("lang", currentLanguage.toLowerCase());
            body.put("code", pincode.getValue());

            verifyCode(body);


        } else {
            Toast.makeText(getActivity(), getString(R.string.enterCode), Toast.LENGTH_LONG).show();
        }


    }


    private void verifyCode(HashMap<String, Object> body) {
        ((MainActivity) getActivity()).hideKeyBoard();
        progressBar.setVisibility(View.VISIBLE);
        UserApi retrofitAPI = ServiceClient.createService(UserApi.class, getActivity());
        //  String token = Global.getInstance(getContext()).getFromSharedPreferneces(Constants.SH_Token);
        retrofitAPI.confirmPhone(token, body).enqueue(new Callback<RESPONSE>() {
            @Override
            public void onResponse(Call<RESPONSE> call, Response<RESPONSE> response) {
                if (response.code() == 200) {
                    progressBar.setVisibility(View.GONE);
                    RESPONSE result = response.body();

                    Intent intent = new Intent(Global.getInstance(getContext()).BROADCAST_ACTION);
                    intent.putExtra("action", Constants.verifyPhone);

                    getContext().sendBroadcast(intent);


                    if (tag.equalsIgnoreCase("register")) {
                        Global.getInstance(getActivity()).saveInSharedPrefernces(Constants.Authorized, "true");
                        Global.getInstance(getActivity()).saveInSharedPrefernces(Constants.SH_Token, token);
                        Intent objIntent = new Intent(getActivity(), Home.class);
                        startActivity(objIntent);
                        getActivity().finish();
                        //  getFragmentManager().popBackStack();

                    } else if (tag.equalsIgnoreCase("verification")) {
                        if (emailStatus == true) {
                            Global.getInstance(getActivity()).saveInSharedPrefernces(Constants.SH_Token, token);
                            Global.getInstance(getActivity()).saveInSharedPrefernces(Constants.Authorized, "true");
                            Intent objIntent = new Intent(getActivity(), Home.class);
                            startActivity(objIntent);
                            getActivity().finish();
                        } else {

                            getFragmentManager().popBackStack();

                        }

                    }
                } else {
                    APIError error = ErrorUtils.parseError(response);
                    Toast.makeText(getActivity(), error.getMessage(), Toast.LENGTH_LONG).show();
                    progressBar.setVisibility(View.GONE);

                }
            }

            @Override
            public void onFailure(Call<RESPONSE> call, Throwable throwable) {
                progressBar.setVisibility(View.GONE);
                Toast.makeText(getActivity(), getString(R.string.errorServer), Toast.LENGTH_LONG).show();
            }
        });
    }

    private void resendCode() {
        ((MainActivity) getActivity()).hideKeyBoard();
        progressBar.setVisibility(View.VISIBLE);
        UserApi retrofitAPI = ServiceClient.createService(UserApi.class, getActivity());
        //  String token = Global.getInstance(getContext()).getFromSharedPreferneces(Constants.SH_Token);
        retrofitAPI.resendPhoneActivationCode(token).enqueue(new Callback<RESPONSE>() {
            @Override
            public void onResponse(Call<RESPONSE> call, Response<RESPONSE> response) {
                if (response.code() == 200) {
                    progressBar.setVisibility(View.GONE);
                    RESPONSE result = response.body();


                    Toast.makeText(getActivity(), getString(R.string.coderesend), Toast.LENGTH_LONG).show();


                } else {
                    APIError error = ErrorUtils.parseError(response);
                    Toast.makeText(getActivity(), error.getMessage(), Toast.LENGTH_LONG).show();
                    progressBar.setVisibility(View.GONE);

                }
            }

            @Override
            public void onFailure(Call<RESPONSE> call, Throwable throwable) {
                progressBar.setVisibility(View.GONE);
                Toast.makeText(getActivity(), getString(R.string.errorServer), Toast.LENGTH_LONG).show();
            }
        });
    }


}
