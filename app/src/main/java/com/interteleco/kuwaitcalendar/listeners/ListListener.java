package com.interteleco.kuwaitcalendar.listeners;

import com.interteleco.kuwaitcalendar.server.models.CategoryData;
import com.interteleco.kuwaitcalendar.server.models.Target;
import com.interteleco.kuwaitcalendar.server.models.Venue;

public interface ListListener {

    public void selectTarget(Target target);
    public void selectVenue(Venue venue);
    public void selectCategory(CategoryData.Category category);
}
