package com.interteleco.kuwaitcalendar.server;


import com.interteleco.kuwaitcalendar.server.models.CategoryData;
import com.interteleco.kuwaitcalendar.server.models.EventData;
import com.interteleco.kuwaitcalendar.server.models.EventModel;
import com.interteleco.kuwaitcalendar.server.models.EventsResponse;
import com.interteleco.kuwaitcalendar.server.models.RESPONSE;
import com.interteleco.kuwaitcalendar.server.models.VenueResponse;
import com.interteleco.kuwaitcalendar.server.models.WeekEvents;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;

import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;

import retrofit2.http.HTTP;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Part;
import retrofit2.http.PartMap;
import retrofit2.http.Path;
import retrofit2.http.Query;


public interface EventApi {

    @GET("events/{id}")
    Call<EventModel> getEventdetails(@Path("id") String id,@Header("Authorization") String token);

    @GET("events/categories")
    Call<CategoryData> getCategories();

    @GET("events/page")
    Call<EventsResponse> getEvents(@Header("Authorization") String token, @Query("category") String category, @Query("size") int size, @Query("page") int page);


    @GET("events")
    Call<WeekEvents> getAllEvents(@Header("Authorization") String token, @Query("size") int size, @Query("page") int page);

    @GET("events/thisWeek")
    Call<WeekEvents> getWeekEvents(@Header("Authorization") String token, @Query("category") String category, @Query("size") int size, @Query("page") int page);

    @GET("events/recommended")
    Call<WeekEvents> getRecomEvents(@Header("Authorization") String token, @Query("category") String category, @Query("size") int size, @Query("page") int page);


    @GET("events/map")
    Call<List<EventData>> getMapEvents(@Query("title") String title, @Query("category") String category, @Query("duration") int duration);

    @POST("events")
    Call<RESPONSE> addEvent(@Body HashMap<String, Object> body, @Header("Authorization") String token);

    @GET("events/venues")
    Call<VenueResponse> getVenues();

    @Multipart
    @POST("uploadImage/{folder}")
    Call<RESPONSE> upload(@Path("folder") String events, @Header("Authorization") String token, @Part MultipartBody.Part image
    );

    @Multipart
    @POST("uploadImage/{folder}")
    Call<ResponseBody> uploadImage(@Path("folder") String events,
                                   @Header("Authorization") String authorization,
                                   @PartMap Map<String, RequestBody> map
    );




    @PUT("events/like")
    Call<RESPONSE> likeEvent(@Header("Authorization") String token, @Body HashMap<String, Object> body);

    @PUT("events/share")
    Call<RESPONSE> shareEvent(@Header("Authorization") String token, @Body HashMap<String, Object> body);

    @PUT("events/addCalendar")
    Call<RESPONSE> addCalendar(@Header("Authorization") String token, @Body HashMap<String, Object> body);


    @PUT("events/removeCalendar")
    Call<RESPONSE> removeCalendar(@Header("Authorization") String token, @Body HashMap<String, Object> body);

    @HTTP(method = "DELETE", path = "events", hasBody = true)
    Call<RESPONSE> deleteEvent(@Header("Authorization") String token, @Body HashMap<String, Object> body);


}