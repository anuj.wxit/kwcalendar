package com.interteleco.kuwaitcalendar.server;

import android.content.Context;

import com.interteleco.kuwaitcalendar.utils.Constants;
import com.interteleco.kuwaitcalendar.utils.Global;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ServiceClient {

    private static Retrofit.Builder builder =
            new Retrofit.Builder()
                    .baseUrl(Constants.MAINURL)
                    .addConverterFactory(GsonConverterFactory.create());

    public static Retrofit retrofit = builder.build();

    private static HttpLoggingInterceptor logging =
            new HttpLoggingInterceptor()
                    .setLevel(HttpLoggingInterceptor.Level.BODY);

    private static OkHttpClient.Builder httpClient =
            new OkHttpClient.Builder();
    public static OkHttpClient client = httpClient.connectTimeout(30, TimeUnit.SECONDS).readTimeout(30, TimeUnit.SECONDS).build();

    public static <S> S createService(
            Class<S> serviceClass, final Context mActivity) {

        httpClient.addInterceptor(new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {

                Request original = chain.request();
                Request request = original.newBuilder()
                        .header("Content-Type", "application/json").header("Accept-Language", Global.getInstance(mActivity).getFromSharedPreferneces("mylang"))
                        //   .header("Authorization", "auth-token")
                        //  .method(original.method(), original.body())
                        .build();
                Response response = chain.proceed(request);

                // Customize or return the response
                return response;
            }
        });


        if (!httpClient.interceptors().contains(logging)) {
            httpClient.addInterceptor(logging);
            client = httpClient.build();
            builder.client(client);
            retrofit = builder.build();
        }

        return retrofit.create(serviceClass);
    }


}
