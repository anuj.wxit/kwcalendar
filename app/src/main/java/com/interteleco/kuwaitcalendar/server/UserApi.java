package com.interteleco.kuwaitcalendar.server;

import com.interteleco.kuwaitcalendar.server.models.CategoryData;
import com.interteleco.kuwaitcalendar.server.models.ProfileEvents;
import com.interteleco.kuwaitcalendar.server.models.RESPONSE;
import com.interteleco.kuwaitcalendar.server.models.User;
import com.interteleco.kuwaitcalendar.server.models.UserModel;

import java.util.HashMap;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Part;
import retrofit2.http.Path;

public interface UserApi {

    @POST("auth/register")
    Call<UserModel> register(@Body HashMap<String, Object> body);

    @POST("auth/login")
    Call<UserModel> login(@Body HashMap<String, Object> body);

    @POST("auth/loginSocial")
    Call<UserModel> loginSocial(@Body HashMap<String, Object> body);


    @POST("auth/forgetPassword")
    Call<RESPONSE> forgetPassword(@Body HashMap<String, Object> body);


    @PUT("auth/resetPassword")
    Call<RESPONSE> resetPassword(@Body HashMap<String, Object> body);


    @PUT("profile/changePassword")
    Call<RESPONSE> changePassword(@Header("Authorization") String token, @Body HashMap<String, Object> body);


    @POST("auth/confirmEmail")
    Call<RESPONSE> confirmEmail(@Header("Authorization") String token, @Body HashMap<String, Object> body);


    @POST("auth/confirmPhone")
    Call<RESPONSE> confirmPhone(@Header("Authorization") String token, @Body HashMap<String, Object> body);

    @Multipart
    @POST("uploadImage/{folder}")
    Call<RESPONSE> uploadProfile(@Path("folder") String profile, @Part MultipartBody.Part image
    );

    @GET("profile/info")
    Call<User> getProfileInfo(@Header("Authorization") String token);

    @GET("profile")
    Call<ProfileEvents> getUSerEvents(@Header("Authorization") String token);


    @PUT("profile/editProfile")
    Call<RESPONSE> editProfile(@Header("Authorization") String token, @Body HashMap<String, Object> body);

    @PUT("profile/changePhone")
    Call<RESPONSE> changePhone(@Header("Authorization") String token, @Body HashMap<String, Object> body);


    @PUT("profile/changeEmail")
    Call<RESPONSE> changeEmail(@Header("Authorization") String token, @Body HashMap<String, Object> body);

    @POST("profile/resend-email-activationCode")
    Call<RESPONSE> resendEmailActivationCode(@Header("Authorization") String token);

    @POST("profile/resend-phone-activationCode")
    Call<RESPONSE> resendPhoneActivationCode(@Header("Authorization") String token);

    @POST("events/feedback")
    Call<RESPONSE> sendFeedback(@Header("Authorization") String token, @Body HashMap<String, Object> body);
    @POST("profile/add-token")
    Call<RESPONSE> addToken(@Header("Authorization") String token, @Body HashMap<String, Object> body);



    @PUT("profile/update-token")
    Call<RESPONSE> updateToken(@Header("Authorization") String token, @Body HashMap<String, Object> body);

}
