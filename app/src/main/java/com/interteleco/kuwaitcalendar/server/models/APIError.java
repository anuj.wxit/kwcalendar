package com.interteleco.kuwaitcalendar.server.models;

import java.util.ArrayList;

public class APIError {
    public ArrayList<ErrorMessage> getErrors() {
        return errors;
    }

    public void setErrors(ArrayList<ErrorMessage> errors) {
        this.errors = errors;
    }

    private ArrayList<ErrorMessage> errors = new ArrayList<>();

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    private String message;
}
