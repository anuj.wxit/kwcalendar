package com.interteleco.kuwaitcalendar.server.models;

import com.google.gson.annotations.SerializedName;

public class EventData {
        private Boolean  published;
        private String _id;
        private String title_en;

    public boolean isLikes() {
        return likes;
    }

    public void setLikes(boolean likes) {
        this.likes = likes;
    }

    private boolean likes;

    public boolean isCalender() {
        return isCalender;
    }

    public void setCalender(boolean calender) {
        isCalender = calender;
    }

    private boolean isCalender;

        public String getTitle_ar() {
        return title_ar;
    }

       public void setTitle_ar(String title_ar) {
        this.title_ar = title_ar;
    }

        private String title_ar;
        public boolean isPublished() {
            return published;
        }

        public void setPublished(boolean published) {
            this.published = published;
        }

        public String get_id() {
            return _id;
        }

        public void set_id(String _id) {
            this._id = _id;
        }

        public String getTitle_en() {
            return title_en;
        }

        public void setTitle_en(String title_en) {
            this.title_en = title_en;
        }

        public Double getPrice() {
            return price;
        }

        public void setPrice(Double price) {
            this.price = price;
        }

        public String getLink() {
            return link;
        }

        public void setLink(String link) {
            this.link = link;
        }

        public String getStartDate() {
            return startDate;
        }

        public void setStartDate(String startDate) {
            this.startDate = startDate;
        }

        public String getEndDate() {
            return endDate;
        }

        public void setEndDate(String endDate) {
            this.endDate = endDate;
        }

        public Double getLongt() {
            return longt;
        }

        public void setLongt(Double longt) {
            this.longt = longt;
        }

        public Double getLat() {
            return lat;
        }

        public void setLat(Double lat) {
            this.lat = lat;
        }

        public String getOpenTime() {
            return openTime;
        }

        public void setOpenTime(String openTime) {
            this.openTime = openTime;
        }

        public String getCloseTime() {
            return closeTime;
        }

        public void setCloseTime(String closeTime) {
            this.closeTime = closeTime;
        }

        public String getImageURL() {
            return imageURL;
        }

        public void setImageURL(String imageURL) {
            this.imageURL = imageURL;
        }

        public String getDesc_en() {
            return desc_en;
        }

        public void setDesc_en(String desc_en) {
            this.desc_en = desc_en;
        }

        private Double price;
        private String link;
        private String startDate;
        private String endDate;

        public String getType() {
            return type;
        }
        public void setType(String type) {
            this.type = type;
        }

        private String type;

        @SerializedName("long")
        private Double longt;

        private Double lat;

        private String openTime;
        private String closeTime;
        private String imageURL;
        private String desc_en;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    private  int status;
    public String getDesc_ar() {
        return desc_ar;
    }

    public void setDesc_ar(String desc_ar) {
        this.desc_ar = desc_ar;
    }

    private String desc_ar;
    public Venue getVenueId() {
        return venueId;
    }

    public void setVenueId(Venue venueId) {
        this.venueId = venueId;
    }

    private Venue venueId;


}
