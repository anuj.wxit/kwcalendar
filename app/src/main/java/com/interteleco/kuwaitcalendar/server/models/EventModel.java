package com.interteleco.kuwaitcalendar.server.models;


public class EventModel {
    public EventData getEvent() {
        return event;
    }

    public void setEvent(EventData event) {
        this.event = event;
    }

    private EventData event;

    private String open;

    public String getOpen() {
        return open;
    }

    public void setOpen(String open) {
        this.open = open;
    }

    public String getClose() {
        return close;
    }

    public void setClose(String close) {
        this.close = close;
    }

    public boolean isCalender() {
        return isCalender;
    }

    public void setCalender(boolean calender) {
        isCalender = calender;
    }

    private String close;
    private boolean isCalender;
}
