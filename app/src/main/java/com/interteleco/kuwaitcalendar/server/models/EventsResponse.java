package com.interteleco.kuwaitcalendar.server.models;

public class EventsResponse {
    public ResponseData getData() {
        return data;
    }

    public void setData(ResponseData data) {
        this.data = data;
    }

    private ResponseData data;

}
