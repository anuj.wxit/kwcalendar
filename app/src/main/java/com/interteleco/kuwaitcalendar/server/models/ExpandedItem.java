package com.interteleco.kuwaitcalendar.server.models;

public class ExpandedItem {
    private int icon;

    public int getIcon() {
        return icon;
    }

    public void setIcon(int icon) {
        this.icon = icon;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    private String title;

    public ExpandedItem(int icon, String title) {
        this.icon = icon;
        this.title = title;
    }

}
