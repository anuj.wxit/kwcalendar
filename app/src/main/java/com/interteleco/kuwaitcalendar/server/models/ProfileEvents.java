package com.interteleco.kuwaitcalendar.server.models;

import java.util.ArrayList;

public class ProfileEvents {
private String name;
private String avatar;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public ArrayList<EventData> getMyEvents() {
        return myEvents;
    }

    public void setMyEvents(ArrayList<EventData> myEvents) {
        this.myEvents = myEvents;
    }

    public ArrayList<EventData> getMyFavourites() {
        return myFavourites;
    }

    public void setMyFavourites(ArrayList<EventData> myFavourites) {
        this.myFavourites = myFavourites;
    }

    public ArrayList<EventData> getMyCalendar() {
        return myCalendar;
    }

    public void setMyCalendar(ArrayList<EventData> myCalendar) {
        this.myCalendar = myCalendar;
    }

    private ArrayList<EventData> myEvents = new ArrayList<>();
    private ArrayList<EventData> myFavourites = new ArrayList<>();
    private ArrayList<EventData> myCalendar = new ArrayList<>();

}
