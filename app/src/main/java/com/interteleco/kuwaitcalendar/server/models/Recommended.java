package com.interteleco.kuwaitcalendar.server.models;

import java.util.ArrayList;

public class Recommended {

     private int total;

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public int getPagesCount() {
        return pagesCount;
    }

    public void setPagesCount(int pagesCount) {
        this.pagesCount = pagesCount;
    }

    public ArrayList<EventData> getData() {
        return data;
    }

    public void setData(ArrayList<EventData> data) {
        this.data = data;
    }

    private int pagesCount;
     public ArrayList<EventData> data = new ArrayList<>();
}
