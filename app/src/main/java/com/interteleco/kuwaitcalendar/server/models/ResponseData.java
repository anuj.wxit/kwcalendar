package com.interteleco.kuwaitcalendar.server.models;

import java.util.ArrayList;

public class ResponseData {

    public ArrayList<EventData> slider = new ArrayList<>();
      WeekEvents thisWeek;

    public ArrayList<EventData> getSlider() {
        return slider;
    }

    public void setSlider(ArrayList<EventData> slider) {
        this.slider = slider;
    }

    public WeekEvents getThisWeek() {
        return thisWeek;
    }

    public void setThisWeek(WeekEvents thisWeek) {
        this.thisWeek = thisWeek;
    }

    public Recommended getRecommended() {
        return recommended;
    }

    public void setRecommended(Recommended recommended) {
        this.recommended = recommended;
    }

    Recommended recommended;

}
