package com.interteleco.kuwaitcalendar.server.models;

public class Target {
    public Target(int id, String title) {
        this.id = id;
        this.title = title;
    }

    private int id;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    private String title;


}
