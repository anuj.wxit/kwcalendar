package com.interteleco.kuwaitcalendar.server.models;

import com.google.gson.annotations.SerializedName;

public class UserModel {

    @SerializedName("email")
    private String email;
    @SerializedName("name")
    private String name;

    public int getAccountType() {
        return accountType;
    }

    public void setAccountType(int accountType) {
        this.accountType = accountType;
    }

    private int accountType;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBearer() {
        return bearer;
    }

    public void setBearer(String bearer) {
        this.bearer = bearer;
    }

    public Boolean getEmailActivation() {
        return emailActivation;
    }

    public void setEmailActivation(Boolean emailActivation) {
        this.emailActivation = emailActivation;
    }

    public Boolean getPhoneActivation() {
        return phoneActivation;
    }

    public void setPhoneActivation(Boolean phoneActivation) {
        this.phoneActivation = phoneActivation;
    }

    @SerializedName("bearer")
    private String bearer;

    @SerializedName("emailActivation")
    private Boolean emailActivation;
    @SerializedName("phoneActivation")
    private Boolean phoneActivation;
    private String _id;
    private String mobileNumber;

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    private String avatar;
    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public CategoryData.Category getInterestedCategory() {
        return interestedCategory;
    }

    public void setInterestedCategory(CategoryData.Category interestedCategory) {
        this.interestedCategory = interestedCategory;
    }

    private CategoryData.Category interestedCategory;
}
