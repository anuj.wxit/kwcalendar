package com.interteleco.kuwaitcalendar.server.models;

import java.util.ArrayList;

public class VenueResponse {

    public ArrayList<Venue> getVenues() {
        return venues;
    }

    public void setVenues(ArrayList<Venue> venues) {
        this.venues = venues;
    }

    public ArrayList<Venue> venues = new ArrayList<>();

}
