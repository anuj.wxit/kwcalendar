package com.interteleco.kuwaitcalendar.utils;

import android.content.Context;
import android.graphics.Typeface;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatEditText;

public class Fonts {


    public static void set(TextView[] textViews, Context context, int fontIndex) {
        for (TextView textView : textViews) {

            Typeface font = null;
            if (Global.getInstance(context).getFromSharedPreferneces("mylang").equalsIgnoreCase("en")) {
                switch (fontIndex) {
                    case 0:
                        font = Typeface.createFromAsset(context.getResources().getAssets(), "Roboto-Regular.ttf");
                        break;

                    case 1:
                        font = Typeface.createFromAsset(context.getResources().getAssets(), "Roboto-Bold.ttf");
                        break;
                }
            } else {
                switch (fontIndex) {
                    case 0:
                        font = Typeface.createFromAsset(context.getResources().getAssets(), "DroidKufi-Regular.ttf");
                        break;

                    case 1:
                        font = Typeface.createFromAsset(context.getResources().getAssets(), "DroidKufi-Bold.ttf");
                        break;
                }


            }
            textView.setTypeface(font);


        }

    }

    public static void set(EditText[] editTexts, Context context, int fontIndex) {
        for (EditText editText : editTexts) {
            Typeface font = null;
            if (Global.getInstance(context).getFromSharedPreferneces("mylang").equalsIgnoreCase("en")) {

                switch (fontIndex) {
                    case 0:
                        font = Typeface.createFromAsset(context.getResources().getAssets(), "Roboto-Regular.ttf");
                        break;

                    case 1:
                        font = Typeface.createFromAsset(context.getResources().getAssets(), "Roboto-Bold.ttf");
                        break;
                }
            } else {
                switch (fontIndex) {
                    case 0:
                        font = Typeface.createFromAsset(context.getResources().getAssets(), "DroidKufi-Regular.ttf");
                        break;

                    case 1:
                        font = Typeface.createFromAsset(context.getResources().getAssets(), "DroidKufi-Bold.ttf");
                        break;
                }

            }
            editText.setTypeface(font);
        }
    }

    public static void set(AppCompatEditText[] editTexts, Context context, int fontIndex) {
        for (EditText editText : editTexts) {
            Typeface font = null;
            if (Global.getInstance(context).getFromSharedPreferneces("mylang").equalsIgnoreCase("en")) {
                switch (fontIndex) {
                    case 0:
                        font = Typeface.createFromAsset(context.getResources().getAssets(), "Roboto-Regular.ttf");
                        break;

                    case 1:
                        font = Typeface.createFromAsset(context.getResources().getAssets(), "Roboto-Bold.ttf");
                        break;
                }
            } else {
                switch (fontIndex) {
                    case 0:
                        font = Typeface.createFromAsset(context.getResources().getAssets(), "DroidKufi-Regular.ttf");
                        break;

                    case 1:
                        font = Typeface.createFromAsset(context.getResources().getAssets(), "DroidKufi-Bold.ttf");
                        break;


                }
            }
            editText.setTypeface(font);
        }
    }

    public static void set(AppCompatButton[] buttons, Context context, int fontIndex) {
        for (Button button : buttons) {
            Typeface font = null;

            if (Global.getInstance(context).getFromSharedPreferneces("mylang").equalsIgnoreCase("en")) {
                switch (fontIndex) {
                    case 0:
                        font = Typeface.createFromAsset(context.getResources().getAssets(), "Roboto-Regular.ttf");
                        break;

                    case 1:
                        font = Typeface.createFromAsset(context.getResources().getAssets(), "Roboto-Bold.ttf");
                        break;
                }
            } else {
                switch (fontIndex) {
                    case 0:
                        font = Typeface.createFromAsset(context.getResources().getAssets(), "DroidKufi-Regular.ttf");
                        break;

                    case 1:
                        font = Typeface.createFromAsset(context.getResources().getAssets(), "DroidKufi-Bold.ttf");
                        break;


                }
            }
            button.setTypeface(font);
        }
    }

}

