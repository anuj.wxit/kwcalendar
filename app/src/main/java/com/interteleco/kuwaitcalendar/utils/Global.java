package com.interteleco.kuwaitcalendar.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class Global {
    static Global objGlobal = null;
    static Context mContext;
    public static String BROADCAST_ACTION = "com.interteleco.event.action";
    private static final String PREFS_NAME = "UserData";

    private Global() {
    }

    public static Global getInstance(AppCompatActivity objContext) {
        if (objGlobal == null) {
            objGlobal = new Global();
            objGlobal.mContext = objContext;
        }

        return objGlobal;
    }

    public static Global getInstance(Context objContext) {
        if (objGlobal == null) {
            objGlobal = new Global();
            objGlobal.mContext = objContext;
        }

        return objGlobal;
    }

    public void clearSharedPrefernce() {
//        SharedPreferences objSharedPreferences = mContext.getSharedPreferences(
//                Constants.USERINFO, Context.MODE_PRIVATE);
//        SharedPreferences.Editor objEditor = objSharedPreferences.edit();
//        objEditor.putBoolean(Constants.ISREGISTERED, false);
//        objEditor.putBoolean(Constants.ISLOOGEDIN, false);
//        objEditor.putString(Constants.NAME, "");
//
//        objEditor.putString(Constants.PASSWORD, "");
//        objEditor.putString(Constants.EMAIL, "");
//        objEditor.commit();

    }

    public void Logout() {
        SharedPreferences sharedPref = mContext.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(Constants.SH_Email, "");
        editor.putString(Constants.SH_Token, "");
        editor.putString(Constants.SH_stausEmail, "");
        editor.putString(Constants.SH_statusPhone, "");
        editor.putString(Constants.Authorized, "");


        editor.commit();
    }

    public boolean validateEmail(String email) {

        String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
        if (email.length() > 0 && email.matches(emailPattern))
            return true;
        else
            return false;

    }


    public void hideKeyboard(View objView) {
        InputMethodManager imm = (InputMethodManager) mContext
                .getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(objView.getWindowToken(), 0);
    }

    public boolean chechInternet() {
        ConnectivityManager cm =
                (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }


    public void setTextViewdefaultFont(TextView objTextView) {

        Typeface font = Typeface.createFromAsset(objGlobal.mContext.getAssets(), "font/droidkufi_bold.ttf");
        objTextView.setTypeface(font);

    }

    public void setButtondefaultFont(Button objButton) {

        Typeface font = Typeface.createFromAsset(objGlobal.mContext.getAssets(), "font/droidkufi_bold.ttf");
        objButton.setTypeface(font);

    }


    public void saveInSharedPrefernces(String key, String value) {

        SharedPreferences sharedPref = mContext.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        if (value == null)
            value = "";
        editor.putString(key, value);

        editor.commit();
    }

    public String getFromSharedPreferneces(String key) {
        String value = null;
        SharedPreferences sharedPref = mContext.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        value = sharedPref.getString(key, null);
//        if (value!=null&&!value.equalsIgnoreCase(""))
//            value=value.toLowerCase();
        return value;
    }


    public static String[] SectionsType = {"PRODUCT", "SERVICE", "EXHIBITION"};
    public static final boolean DebugMode = true;
    public static int Section_0 = -1;
    public static int Section_1 = -1;

    public static String Filter_section = null;
    public static String Filter_category_ids = null;

    public static String Filter_category_text = null;
    public static String Filter_price_from = null;
    public static String Filter_price_to = null;
    public static String Filter_date_from = null;
    public static String Filter_date_to = null;
    public static String Filter_SortBy = null;


    public static String Filter_section_just = null;
    public static String Filter_category_text_just = null;
    public static String Filter_category_text_ids = null;

    public static String Filter_price_from_just = null;
    public static String Filter_price_to_just = null;
    public static String Filter_date_from_just = null;
    public static String Filter_date_to_just = null;
    public static String Filter_SortBy_just = null;


    public static String Filter_section_new = null;
    public static String Filter_category_text_new = null;
    public static String Filter_category_text_new_id = null;
    public static String Filter_price_from_new = null;
    public static String Filter_price_to_new = null;
    public static String Filter_date_from_new = null;
    public static String Filter_date_to_new = null;
    public static String Filter_SortBy_new = null;


    public static Integer[] whichIds_just = null;

    public static Integer[] whichIds_new = null;

    public static void ResetVar() {
        Section_0 = -1;
        Section_1 = -1;


        Filter_section = null;
        Filter_category_ids = null;

        Filter_category_text = null;
        Filter_price_from = null;
        Filter_price_to = null;
        Filter_date_from = null;
        Filter_date_to = null;
        Filter_SortBy = null;


        Filter_section_just = null;
        Filter_category_text_just = null;
        Filter_category_text_ids = null;

        Filter_price_from_just = null;
        Filter_price_to_just = null;
        Filter_date_from_just = null;
        Filter_date_to_just = null;
        Filter_SortBy_just = null;


        Filter_section_new = null;
        Filter_category_text_new = null;
        Filter_category_text_new_id = null;

        Filter_price_from_new = null;
        Filter_price_to_new = null;
        Filter_date_from_new = null;
        Filter_date_to_new = null;
        Filter_SortBy_new = null;


        whichIds_just = null;

        whichIds_new = null;

    }


}
