package com.interteleco.kuwaitcalendar.utils;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import androidx.localbroadcastmanager.content.LocalBroadcastManager;

public class NetworkStatusReciever   extends BroadcastReceiver {
    public static final String NETWORK_AVAILABLE_ACTION = "com.interteleco.kuwaitcalender.NetworkAvailable";
    public static final String IS_NETWORK_AVAILABLE = "isNetworkAvailable";

    @Override
    public void onReceive(Context context, Intent intent) {
        Intent networkStatusIntent = new Intent(NETWORK_AVAILABLE_ACTION);
        networkStatusIntent.putExtra(IS_NETWORK_AVAILABLE,  isConnected(context));
        LocalBroadcastManager.getInstance(context).sendBroadcast(networkStatusIntent);
    }


    public  static boolean isConnected(Context context) {

        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netinfo = cm.getActiveNetworkInfo();

        if (netinfo != null && netinfo.isConnectedOrConnecting()) {
            android.net.NetworkInfo wifi = cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
            android.net.NetworkInfo mobile = cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

            if ((mobile != null && mobile.isConnectedOrConnecting()) || (wifi != null && wifi.isConnectedOrConnecting()))
                return true;
            else
                return false;
        } else
            return false;


    }


}
