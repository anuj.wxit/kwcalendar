package com.interteleco.kuwaitcalendar.utils;

import com.interteleco.kuwaitcalendar.server.ServiceClient;
import com.interteleco.kuwaitcalendar.server.models.RESPONSE;

import java.io.IOException;
import java.lang.annotation.Annotation;

import okhttp3.ResponseBody;
import retrofit2.Converter;
import retrofit2.Response;

public class ResponseUtil {

    public static RESPONSE parseResponse(Response<?> response) {
        Converter<ResponseBody, RESPONSE> converter =
                ServiceClient.retrofit
                        .responseBodyConverter(RESPONSE.class, new Annotation[0]);

        RESPONSE result;

        try {
            result = converter.convert((ResponseBody) response.body());
        } catch (IOException e) {
            return new RESPONSE();
        }

        return result;
    }

    public static RESPONSE parseError(Response<?> response) {
        Converter<ResponseBody, RESPONSE> converter =
                ServiceClient.retrofit
                        .responseBodyConverter(RESPONSE.class, new Annotation[0]);

        RESPONSE result;

        try {
            result = converter.convert( response.errorBody());
        } catch (IOException e) {
            return new RESPONSE();
        }

        return result;
    }


}
