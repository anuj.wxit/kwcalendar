package com.interteleco.kuwaitcalendar.utils;

import android.content.Intent;

import androidx.fragment.app.FragmentActivity;

public class ShareUtils {

    public static void share(FragmentActivity fragmentActivity, String Url) {
        Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");

        sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Subject Here");
        sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, Url);
        fragmentActivity.startActivity(Intent.createChooser(sharingIntent, "Share via"));


    }

}
