package com.interteleco.kuwaitcalendar.utils;

public interface TaskLoadedCallback {
    void onTaskDone(Object... values);
}
