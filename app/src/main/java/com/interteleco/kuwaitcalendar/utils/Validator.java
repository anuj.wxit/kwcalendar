package com.interteleco.kuwaitcalendar.utils;

import android.widget.EditText;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Validator {

    public static boolean validateEmptyField(EditText objEditText) {
        if (objEditText.getText().toString().trim() != null && !objEditText.getText().toString().trim().equalsIgnoreCase(""))
            return true;
        else

            return false;

    }

    public static boolean isValidEMail(String email2) {
        boolean check = false;
        Pattern p;
        Matcher m;

        String EMAIL_STRING = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

        p = Pattern.compile(EMAIL_STRING);

        m = p.matcher(email2);
        check = m.matches();
        return check;
    }

    public static boolean ValidateUserName(String username) {
        boolean isvalide = false;
        if (username.length() > 3 && username.length() < 22)
            isvalide = true;

        return isvalide;
    }

    public static boolean ValidatePassword(String password) {
        boolean isvalide = false;
        if (password.length() > 7 && password.length() < 25)
            isvalide = true;

        return isvalide;
    }

    public static boolean ValidateQuantity(String quantity) {
        boolean isvalide = false;
        if (quantity.length() <= 8)
            isvalide = true;

        return isvalide;
    }

    public static boolean ValidatePasswordMatch(String password, String ConfirmPass) {
        boolean isvalide = false;
        if (password.trim().equalsIgnoreCase(ConfirmPass.trim()))
            isvalide = true;

        return isvalide;
    }

    public static boolean isNumber(String strnumber) {
        boolean check = false;
        try {

            int number = Integer.parseInt(strnumber);
            if (number > 1 && number < 99)
                check = true;

            return check;

        } catch (Exception e) {
            check = false;
            return check;
        }

    }

    public static boolean isMobile(String strMobile) {
        boolean check = false;
        try {

            if (strMobile.substring(0, 1).equalsIgnoreCase("6") || strMobile.substring(0, 1).equalsIgnoreCase("5") || strMobile.substring(0, 1).equalsIgnoreCase("9"))
                check = true;
            else
                check = false;


        } catch (Exception e) {
            check = false;
            return check;
        }


        return check;

    }

    public static boolean iskwMobile(String mobile, String code) {
        boolean check = false;
        if (code.equalsIgnoreCase("(965)") && mobile.substring(0, 1).equalsIgnoreCase("1") || code.equalsIgnoreCase("(965)") && mobile.substring(0, 1).equalsIgnoreCase("١"))
            check = true;
        else
            check = false;
        return check;
    }

    public static boolean validateDate(String from, String To) {
        boolean check = false;


        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

            Date Datefrom = sdf.parse(from.trim());
            Date DateTo = sdf.parse(To.trim());
            if (Datefrom.equals(DateTo) || DateTo.after(Datefrom)) {
                check = true;
            }

        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }


        return check;
    }

    public static boolean validateCurrentDate(String targetDay) {

        boolean check = false;


        try {

            Calendar c = Calendar.getInstance();


            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            String TodayDate = sdf.format(c.getTime());
            Date Datetoday = sdf.parse(TodayDate);
            Date DateTarget = sdf.parse(targetDay);
            if (Datetoday.equals(DateTarget) || DateTarget.after(Datetoday)) {
                check = true;
            }

        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }


        return check;
    }


    public static boolean isEmptyString(String data) {
        boolean check = false;
        if (data != null && !data.equalsIgnoreCase(""))

            check = true;
        else
            check= false;

        return check;
    }
}